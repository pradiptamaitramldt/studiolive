//
//  MyFollowersView.swift
//  StudioLive
//
//  Created by Bit Mini on 13/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class MyFollowersView: UIViewController {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet weak var tableViewFollowers: UITableView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var bottom_view: UIView!
    @IBOutlet var searchView: UIView!
    
    var bottom_menu = BottomMenu()
    var userType = ""
    var userData : FetchAllUserModel?
    var utility = Utility()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = userType
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.bottom_view.bounds.size.width,height:self.bottom_view.bounds.size.height)
        self.bottom_view.addSubview(bottom_menu)
        bottom_menu.delegate = self
        
        //self.setupView()
        // Do any additional setup after loading the view.
         self.tableViewFollowers.register(UINib(nibName: "MyFollowersView", bundle: .main), forCellReuseIdentifier: "MyFollowFollowersCell")
        self.tableViewFollowers.dataSource = self
        self.tableViewFollowers.delegate = self
        searchView.setCornerMolded(radiousVlaue : 10.0)
        searchView.setShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor)
        
        if userType == "My Followers" {
                  fetchAllConnectedUser()
               }
               else {
                  fetchAllUser()
               }
        
    }
    
    /*private func setupView() {
        backView.layer.shadowOpacity = 0.3
        backView.layer.shadowOffset = CGSize(width: 0, height: 0)
        backView.layer.shadowRadius = 4.0
        backView.layer.cornerRadius = 6.0
        //        textFieldCountry.delegate = self
        //        textFieldCity.delegate = self
        //        textViewMessage.delegate = self
    }*/
    func fetchAllUser() {
        let fetchAllUserRepository = FetchAllUserRepository()
        fetchAllUserRepository.fetchAllUser(vc:  self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                   // print("romio:\(response.statuscode)")
                    self.userData = response
                   // print("getData : \(self.userData)")
                    self.tableViewFollowers.reloadData()
                }else if response.statuscode == 422 {
                    
                }else {
                    
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    func fetchAllConnectedUser() {
        let fetchAllUserRepository = FetchAllUserRepository()
        fetchAllUserRepository.fetchAllConnectedUser(vc:  self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                   // print("romio:\(response.statuscode)")
                    self.userData = response
                   // print("getData : \(self.userData)")
                    self.tableViewFollowers.reloadData()
                }else if response.statuscode == 422 {
                    
                }else {
                    
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    @IBAction func buttonMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    
    func stopLoading() {
        let load = Loader()
       load.hide(delegate: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MyFollowersView : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userData?.responseData.users.count ?? 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyFollowFollowersCell") as! MyFollowFollowersCell
        if userType == "My Followers" {
            cell.followUnfollowButton.setTitle("Unfollow", for: .normal)
        }
        else {
            if (userData?.responseData.users[indexPath.row].sendRequest ?? "" == "REQUEST_SEND") || (userData?.responseData.users[indexPath.row].sendRequest ?? "" == "REQUEST_RECEIVE"){
                cell.followUnfollowButton.setTitle("Cancel", for: .normal)
            }
            else {
                cell.followUnfollowButton.setTitle("Follow", for: .normal)
            }
        }
        cell.labelName.text = userData?.responseData.users[indexPath.row].name
        cell.labelEmailID.text = userData?.responseData.users[indexPath.row].email
        cell.profileImage.af_setImage(
            withURL: URL(string: userData?.responseData.users[indexPath.row].profileImage ?? "")!,
                       placeholderImage: UIImage(named: "userprofileDummy"),
                       filter: nil,
                       imageTransition: .crossDissolve(0.2)
                   )
        cell.followUnfollowButton.tag = indexPath.row
        cell.followUnfollowButton.addTarget(self, action: #selector(followUnfollowButtonAction(sender:)), for: .touchUpInside)
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        return cell
    }
    @objc func followUnfollowButtonAction(sender: UIButton){
        if userType == "My Followers" {
            
        }
        else {
            if (userData?.responseData.users[sender.tag].sendRequest ?? "" == "REQUEST_SEND") || (userData?.responseData.users[sender.tag].sendRequest ?? "" == "REQUEST_RECEIVE"){
                requestResponse(tag: sender.tag, response: "NO")
            }
            else {
                sendFriendRequest(tag : sender.tag)
            }
            
        }
    }
    func requestResponse(tag : Int, response : String) {
        let fetchAllUserRepository = AcceptFriendRequestRepository()
         let userID = self.utility.Retrive(Constants.Strings.UserID)
        //5f2d53624db503788439412b
         let parameter = [ "customerId":userID as! String,
                           "toCustomerId" : self.userData?.responseData.users[tag].customerID ?? "",
                           "isAccept":response]
         print("param\(parameter)")
         fetchAllUserRepository.acceptFriendRequest(vc:  self,parameter :parameter ) { (response, success, error) in
             self.stopLoading()
             if (error != nil) {
                 Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
             }
             if (success) {
                 if response.statuscode == 200 {
                    // print("romio:\(response.statuscode)")
                    // self.userData = response
                    // print("getData : \(self.userData)")
                   //  self.tableViewFollowers.reloadData()
                 //   Utility.showAlert(withMessage: (response.message), onController: self)
                    self.fetchAllUser()
                 }else if response.statuscode == 422 {
                     
                 }else {
                     
                 }
             }else{
                 Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
             }
         }
    }
    
    func sendFriendRequest(tag : Int) {
        let fetchAllUserRepository = SendFriendRequestRepository()
        let userID = self.utility.Retrive(Constants.Strings.UserID)
       
        let parameter = [ "customerId":userID as! String,
                          "toCustomerId" : userData?.responseData.users[tag].customerID ?? ""]
        print("param\(parameter)")
        fetchAllUserRepository.sendFriendRequest(vc:  self,parameter :parameter ) { (response, success, error) in
            self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                   // print("romio:\(response.statuscode)")
                   // self.userData = response
                   // print("getData : \(self.userData)")
                 //    Utility.showAlert(withMessage: (response.message), onController: self)
                    self.tableViewFollowers.reloadData()
                    self.fetchAllUser()
                }else if response.statuscode == 422 {
                    
                }else {
                    
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
}
extension MyFollowersView: bottomMenuDelegate{
    func selectedButton(selectedItem: NSInteger) {
        if selectedItem == 1 {
            //home
            let st = UIStoryboard.init(name: "HomeView", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"HomeView") as! HomeView
            self.navigationController?.pushViewController(controller, animated: true)
        }else if selectedItem == 2{
            //Feed
            let st = UIStoryboard.init(name: "Feed", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FeedView") as! FeedView
            self.navigationController?.pushViewController(controller, animated: true)
        }
       else if selectedItem == 3 {
            //Post
            let st = UIStoryboard.init(name: "Post", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"PostView") as! PostView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 4 {
            let st = UIStoryboard.init(name: "Favourite", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FavouriteView") as! FavouriteView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 5 {
            let st = UIStoryboard.init(name: "Profile", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"ProfileView") as! ProfileView
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
