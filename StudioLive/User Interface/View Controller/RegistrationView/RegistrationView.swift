//
//  RegistrationView.swift
//  StudioLive
//
//  Created by Pradipta on 09/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class RegistrationView: UIViewController {

    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var buttonFB: UIButton!
    @IBOutlet weak var buttonGoogle: UIButton!
    @IBOutlet weak var checkIcon: UIImageView!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldDOB: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldConfirmPassword: UITextField!
    
    var registrationCommand: RegistartionCommand?
    var utility = Utility()
    var isChecked : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.textFieldName.delegate=self
        self.textFieldEmail.delegate=self
        self.textFieldDOB.delegate=self
        self.textFieldPassword.delegate=self
        self.textFieldConfirmPassword.delegate=self
        self.setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.buttonSubmit.layer.cornerRadius = self.buttonSubmit.frame.size.height/2
        self.buttonFB.layer.cornerRadius = self.buttonFB.frame.size.height/2
        self.buttonGoogle.layer.cornerRadius = self.buttonGoogle.frame.size.height/2
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Private
    private func setup() {
        self.registrationCommand = RegistartionCommand.init(name: nil, email: nil, password: nil, dob: nil, confirmPassword: nil)
    }
    
    private func updateCommand() {
        self.registrationCommand?.name = self.textFieldName.text
        self.registrationCommand?.email = self.textFieldEmail.text?.trimmingCharacters(in: .whitespaces)
        self.registrationCommand?.dob = self.textFieldDOB.text
        self.registrationCommand?.password = self.textFieldPassword.text
        self.registrationCommand?.confirmPassword = self.textFieldConfirmPassword.text
    }
    
    func performRegistration(registrationCommand: RegistartionCommand?) {
        
        let registrationRepository = RegistrationRepository()
        registrationRepository.registrationUser(registrationCommand: registrationCommand!, vc: self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.responseCode == 2000 {
                    if (response.responseData?.id) != nil {
                        print("email...\(response.responseData?.email ?? "NA")")
                        print("id...\(response.responseData?.id ?? "NA")")
                        self.utility.Save(response.responseData?.id ?? "NA", keyname: Constants.Strings.UserID)
                        self.utility.Save(response.responseData?.email ?? "NA", keyname: Constants.Strings.UserEmail)
                        let alert = UIAlertController(title: Constants.Strings.AppName, message: response.responseMessage, preferredStyle: UIAlertController.Style.alert)
                        
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                            //Cancel Action
                            let st = UIStoryboard.init(name: "Main", bundle: nil)
                            let controller = st.instantiateViewController(withIdentifier:"OTPView") as! OTPView
                            controller.otp = response.responseData?.otp ?? "0"
                            controller.fromClass = "normal"
                            self.navigationController?.pushViewController(controller, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else if response.responseCode == 2008 {
                    let alert = UIAlertController(title: Constants.Strings.AppName, message: response.responseMessage, preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                        //Cancel Action
                        let st = UIStoryboard.init(name: "Main", bundle: nil)
                        let controller = st.instantiateViewController(withIdentifier:"OTPView") as! OTPView
                        controller.otp = response.responseData?.otp ?? "0"
                        self.navigationController?.pushViewController(controller, animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    @IBAction func buttonBackToLoginAction(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSubmitAction(_ sender: Any) {
        self.updateCommand()
        self.registrationCommand?.validate(completion: { (isValid, message) in
            
            // Start activity
            //self.startLoading()
            if(isValid){
                if self.isChecked==true {
                    guard let registrationCommand = self.registrationCommand else{
                        return
                    }
                    self.performRegistration(registrationCommand: registrationCommand)
                }else{
                    Utility.showAlert(withMessage: "Please agree to the terms & conditions and privacy policy", onController: self)
                }
                
            }else{
                Utility.showAlert(withMessage: message!, onController: self)
            }
        })
    }
    @IBAction func buttonCheckAction(_ sender: Any) {
        if isChecked==false {
            isChecked=true
            self.checkIcon.image=UIImage(named: "select-checkbox")
        }else{
            isChecked=false
            self.checkIcon.image=UIImage(named: "checkbox")
        }
    }
    @IBAction func buttonDOBAction(_ sender: Any) {
        self.view.endEditing(true)
        let datePicker = ActionSheetDatePicker(title: "Select Date", datePickerMode: UIDatePicker.Mode.date, selectedDate: NSDate() as Date, doneBlock: {
            picker,index,value in
            
            //Convert in date format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = index as! Date
            
            /*let dateOfBirth = date
            let today = NSDate()
            let gregorian = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let age = gregorian.components([.year], from: dateOfBirth, to: today as Date, options: [])*/
            self.textFieldDOB.text = dateFormatter.string(from: date)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!?.superview)
        //        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
        //        datePicker?.minimumDate = Date(timeInterval: -secondsInWeek, since: Date())
        //        datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
        
        datePicker?.maximumDate = Date()
        
        datePicker?.show()
    }
}
extension RegistrationView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //Format Date of Birth dd-MM-yyyy
        
        //initially identify your textfield
        
        if textField == self.textFieldDOB {
            
            // check the chars length yyyy -->4 at the same time calculate the MM-dd --> 7
            if (self.textFieldDOB?.text?.count == 4) || (self.textFieldDOB?.text?.count == 7) {
                //Handle backspace being pressed
                if !(string == "") {
                    // append the text
                    self.textFieldDOB?.text = (self.textFieldDOB?.text)! + "-"
                }
            }
            // check the condition not exceed 9 chars
            return !(textField.text!.count > 9 && (string.count ) > range.length)
        }
        else {
            return true
        }
    }
}
