//
//  ChangeEmailAddressView.swift
//  StudioLive
//
//  Created by Subhajit on 10/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ChangeEmailAddressView: UIViewController {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var confirmEmailTextField: UITextField!
    @IBOutlet weak var newEmailTextField: UITextField!
    
    var changeEmailCommand: ChangeEmailCommand?
    var utility = Utility()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.setup()
    }
    private func setupView() {
        backView.layer.shadowOpacity = 0.3
        backView.layer.shadowOffset = CGSize(width: 0, height: 0)
        backView.layer.shadowRadius = 4.0
        backView.layer.cornerRadius = 6.0
        
        newEmailTextField.delegate = self
        confirmEmailTextField.delegate = self
        
    }
    
    // MARK: - Private
    private func setup() {
        self.changeEmailCommand = ChangeEmailCommand.init(email: nil, confirmEmail: nil)
    }
    
    private func updateCommand() {
        self.changeEmailCommand?.email = self.newEmailTextField.text?.trimmingCharacters(in: .whitespaces)
        self.changeEmailCommand?.confirmEmail = self.confirmEmailTextField.text
    }
    
    func changeEmail(changeEmailCommand: ChangeEmailCommand?) {
        
        let changeEmailRepository = ChangeEmailRepository()
        changeEmailRepository.changeEmail(changeEmailCommand: changeEmailCommand!, vc: self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode==200 {
                    let otp = response.responseData?.otp
                    let oldEmail = response.responseData?.email
                    let newEmail = response.responseData?.newEmail
                    self.utility.Save(otp ?? "NA", keyname: Constants.Strings.ChangeEmailOTP)
                    self.utility.Save(oldEmail ?? "NA", keyname: Constants.Strings.OldEmail)
                    self.utility.Save(newEmail ?? "NA", keyname: Constants.Strings.NewEmail)
                    
                    let optionMenu = UIAlertController(title: "", message: response.message, preferredStyle: .actionSheet)
                    let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: {
                        (alert: UIAlertAction!) -> Void in
                        print("Cancelled")
                        let st = UIStoryboard.init(name: "ChangeEmailAddress", bundle: nil)
                        let controller = st.instantiateViewController(withIdentifier:"EmailOTPView") as! EmailOTPView
                        self.navigationController?.pushViewController(controller, animated: true)
                    })
                    optionMenu.addAction(cancelAction)
                    self.present(optionMenu, animated: true, completion: nil)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        self.updateCommand()
        self.changeEmailCommand?.validate(completion: { (isValid, message) in
            if(isValid){
                guard let changeEmailCommand = self.changeEmailCommand else{
                    return
                }
                self.changeEmail(changeEmailCommand: changeEmailCommand)
            }else{
                Utility.showAlert(withMessage: message!, onController: self)
            }
        })
    }
}


private typealias TextFielDelegate = ChangeEmailAddressView
extension TextFielDelegate : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
