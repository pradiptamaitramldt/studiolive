//
//  EmailOTPView.swift
//  StudioLive
//
//  Created by Pradipta on 16/03/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class EmailOTPView: UIViewController {

    @IBOutlet weak var otp1: UITextField!
    @IBOutlet weak var otp2: UITextField!
    @IBOutlet weak var otp3: UITextField!
    @IBOutlet weak var otp4: UITextField!
    @IBOutlet weak var buttonSubmit: UIButton!
    
    var createOtp : String = ""
    var accountVerifyCommand: AccountVerifyCommand?
    var utility = Utility()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.buttonSubmit.layer.cornerRadius=self.buttonSubmit.frame.size.height/2
        self.otp1.attributedPlaceholder = NSAttributedString(string: "0",attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 146.0/255, green: 156.0/255, blue: 179.0/255, alpha: 1.0)])
        self.otp2.attributedPlaceholder = NSAttributedString(string: "0",attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 146.0/255, green: 156.0/255, blue: 179.0/255, alpha: 1.0)])
        self.otp3.attributedPlaceholder = NSAttributedString(string: "0",attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 146.0/255, green: 156.0/255, blue: 179.0/255, alpha: 1.0)])
        self.otp4.attributedPlaceholder = NSAttributedString(string: "0",attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 146.0/255, green: 156.0/255, blue: 179.0/255, alpha: 1.0)])
        self.otp1.delegate=self
        self.otp2.delegate=self
        self.otp3.delegate=self
        self.otp4.delegate=self
        let text1 = self.otp1.text ?? ""
        let text2 = self.otp2.text ?? ""
        let text3 = self.otp3.text ?? ""
        let text4 = self.otp4.text ?? ""
        self.createOtp = text1 + text2 + text3 + text4
        self.setup()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Private
    private func setup() {
        self.accountVerifyCommand = AccountVerifyCommand.init(otp: nil)
    }
    
    private func updateCommand() {
        self.accountVerifyCommand?.otp = self.createOtp
    }
    
    func verifyAccount(accountVerifyCommand: AccountVerifyCommand?) {
        let otp = utility.Retrive(Constants.Strings.ChangeEmailOTP)
        if self.createOtp==otp as! String {
            let userID = self.utility.Retrive(Constants.Strings.UserID)
            let oldEmail = self.utility.Retrive(Constants.Strings.OldEmail)
            let newEmail = self.utility.Retrive(Constants.Strings.NewEmail)
            let paramDict = ["customerId":userID, "newEmail": newEmail, "email": oldEmail] as [String : Any]
            let verifyEmailRequest = VerifiyEmailRequest()
            verifyEmailRequest.verifyEmail(parameter: paramDict as! [String : String], vc: self, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
                if success{
                    do {
                        let objResponse = try JSONDecoder().decode(EditProfile.self, from: response! as! Data)
                        let message = objResponse.message
                        print("message...\(message ?? "NA")")
                        self.utility.Save(newEmail as! String, keyname: Constants.Strings.UserEmail)
                        Utility.showAlert(withMessage: message ?? "Successfully Done", onController: self)
                    } catch let error {
                        print("JSON Parse Error: \(error.localizedDescription)")
                    }
                }
            }
        }
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSubmitOtpAction(_ sender: Any) {
        let text1 = self.otp1.text ?? ""
        let text2 = self.otp2.text ?? ""
        let text3 = self.otp3.text ?? ""
        let text4 = self.otp4.text ?? ""
        self.createOtp = text1 + text2 + text3 + text4
        
        self.updateCommand()
        self.accountVerifyCommand?.validate(completion: { (isValid, message) in
            
            // Start activity
            //self.startLoading()
            if(isValid){
                guard let accountVerifyCommand = self.accountVerifyCommand else{
                    return
                }
                self.verifyAccount(accountVerifyCommand: accountVerifyCommand)
            }else{
                Utility.showAlert(withMessage: message!, onController: self)
            }
        })
    }
}
extension EmailOTPView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
