//
//  NeedHelpViewController.swift
//  StudioLive
//
//  Created by Bit Mini on 13/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class NeedHelpViewController: UIViewController {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var textFieldCountry: UITextField!
    @IBOutlet weak var textFieldCity: UITextField!
    @IBOutlet weak var textViewMessage: UITextView!
    @IBOutlet weak var button_Submit: UIButton!
    @IBOutlet weak var bottom_view: UIView!
    
    var bottom_menu = BottomMenu()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        // Do any additional setup after loading the view.
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.bottom_view.bounds.size.width,height:self.bottom_view.bounds.size.height)
        self.bottom_view.addSubview(bottom_menu)
        bottom_menu.delegate = self
    }
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }


    private func setupView() {
        backView.layer.shadowOpacity = 0.3
        backView.layer.shadowOffset = CGSize(width: 0, height: 0)
        backView.layer.shadowRadius = 4.0
        backView.layer.cornerRadius = 6.0
        button_Submit.layer.cornerRadius = 25
//        textFieldCountry.delegate = self
//        textFieldCity.delegate = self
//        textViewMessage.delegate = self
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NeedHelpViewController : bottomMenuDelegate{
    func selectedButton(selectedItem: NSInteger) {
        if selectedItem == 1 {
            //home
            let st = UIStoryboard.init(name: "HomeView", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"HomeView") as! HomeView
            self.navigationController?.pushViewController(controller, animated: true)
        }else if selectedItem == 2{
            //Feed
            let st = UIStoryboard.init(name: "Feed", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FeedView") as! FeedView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 3 {
            //Post
            let st = UIStoryboard.init(name: "Post", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"PostView") as! PostView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 4 {
            let st = UIStoryboard.init(name: "Favourite", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FavouriteView") as! FavouriteView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 5 {
        }
    }
}
