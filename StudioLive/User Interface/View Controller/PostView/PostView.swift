//
//  PostView.swift
//  StudioLive
//
//  Created by Pradipta on 15/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit
import MobileCoreServices
import IQAudioRecorderController

class PostView: UIViewController {
    
    @IBOutlet weak var bottom_view: UIView!
    @IBOutlet weak var viewCategoryBG: UIView!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var textFieldTitle: UITextField!
    @IBOutlet weak var textFieldCategory: UITextField!
    @IBOutlet weak var textFieldLocation: UITextField!
    @IBOutlet weak var imageCheck1: UIImageView!
    @IBOutlet weak var imageCheck2: UIImageView!
    @IBOutlet weak var imageCheck3: UIImageView!
    @IBOutlet weak var progress_view: UIProgressView!
    
    var bottom_menu = BottomMenu()
    var isCategoryViewOpened: Bool = false
    var audioFile = Data()
    var postCommand: PostCommand?
    var fileType: String = ""
    var privacyType: String = ""
    var categoryID: String = ""
    var videoPicker = UIImagePickerController()
    var videoFile = Data()
    var categoryListArray: [AudioCategoryResponseDatum] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.viewCategoryBG.isHidden = true
        
        self.table_view.delegate = self
        self.table_view.dataSource = self
        
        self.imageCheck1.image = UIImage(named: "checkbox")
        self.imageCheck2.image = UIImage(named: "checkbox")
        self.imageCheck3.image = UIImage(named: "checkbox")
        
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.bottom_view.bounds.size.width,height:self.bottom_view.bounds.size.height)
        self.bottom_view.addSubview(bottom_menu)
        bottom_menu.delegate = self
        bottom_menu.postSeleted()
        self.fetchAudioCategory()
        self.setup()
        NotificationCenter.default.addObserver(self, selector: #selector(self.readyToPlayAudio(notification:)), name: Notification.Name("ReadyToPlayAudio2"), object: nil)
    }
    
    @objc func readyToPlayAudio(notification: Notification) {
           let value = (notification.object as! Float)
           self.progress_view.progress = value
           
       }
    // MARK: - Private
    private func setup() {
        self.postCommand = PostCommand.init(title: nil, categoryID: nil, privacyType: nil, location: nil, fileType: nil)
    }
    
    private func updateCommand() {
        self.postCommand?.title = self.textFieldTitle.text
        self.postCommand?.categoryID = self.categoryID
        self.postCommand?.privacyType = self.privacyType
        self.postCommand?.location = self.textFieldLocation.text
        self.postCommand?.fileType = self.fileType
    }
    
    func postAudio(postCommand: PostCommand?) {
        
        let postRepository = PostRepository()
        if self.fileType == "AUDIO" {
            postRepository.uploadAudio(myAudio: self.audioFile, postCommand: postCommand!, vc: self) { (response, success, error) in
                //self.stopLoading()
                if (error != nil) {
                    Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
                }
                if (success) {
                    let statusCode = response["STATUSCODE"] as! String
                    if statusCode == "200" {
                        let message = response["message"]
                        Utility.showAlert(withMessage: message as! String, onController: self)
                    }
                }
            }
        }else if self.fileType == "VIDEO" {
            postRepository.uploadAudio(myAudio: self.videoFile, postCommand: postCommand!, vc: self) { (response, success, error) in
                //self.stopLoading()
                if (error != nil) {
                    Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
                }
                if (success) {
                    let statusCode = response["STATUSCODE"] as! String
                    if statusCode == "200" {
                        let message = response["message"]
                        Utility.showAlert(withMessage: message as! String, onController: self)
                    }
                }
            }
        }
        
    }
    
    func fetchAudioCategory() {
        let postRepository = PostRepository()
        postRepository.fetchAudioCategory(vc:  self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    self.categoryListArray = response.responseData ?? []
                    if self.categoryListArray.count > 0 {
                        DispatchQueue.main.async {
                            self.table_view.reloadData()
                        }
                    }
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    @IBAction func buttonDropDownAction(_ sender: Any) {
        if self.isCategoryViewOpened == false {
            self.isCategoryViewOpened = true
            self.viewCategoryBG.isHidden = false
        }else{
            self.isCategoryViewOpened = false
            self.viewCategoryBG.isHidden = true
        }
    }
    @IBAction func buttonUploadFileAction(_ sender: Any) {
        let successAlert = UIAlertController(title: "", message: "Please select or record your video or audio before upload it.", preferredStyle: UIAlertController.Style.alert)
        
        successAlert.addAction(UIAlertAction(title: "AUDIO", style: .default) { (action:UIAlertAction!) in
            
            self.fileType = "AUDIO"
            let controller = IQAudioRecorderViewController()
            controller.delegate = self
            controller.title = "Audio Recorder"
            controller.maximumRecordDuration = 30
            controller.allowCropping = true
            self.presentBlurredAudioRecorderViewControllerAnimated(controller)
        })
        
        successAlert.addAction(UIAlertAction(title: "VIDEO", style: .default) { (action:UIAlertAction!) in
            
            self.fileType = "VIDEO"
            self.videoPicker.delegate = self
            self.videoPicker.modalPresentationStyle = .currentContext
            self.videoPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.videoPicker.mediaTypes = [kUTTypeMovie as String, kUTTypeAVIMovie as String, kUTTypeVideo as String, kUTTypeMPEG4 as String]
            self.videoPicker.videoQuality = .typeHigh
            self.present(self.videoPicker, animated: true, completion: nil)
        })
        
        successAlert.addAction(UIAlertAction(title: "CANCEL", style: .default) { (action:UIAlertAction!) in
            
            
        })
        
        self.present(successAlert, animated: true)
    }
    @IBAction func buttonStartStreamingAction(_ sender: Any) {
        let st = UIStoryboard.init(name: "Post", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"LiveStreamingView") as! LiveStreamingView
        self.present(controller, animated: true, completion: nil)
    }
    @IBAction func buttonCheck1Action(_ sender: Any) {
        self.privacyType = "PUBLIC"
        self.imageCheck1.image = UIImage(named: "select-checkbox")
        self.imageCheck2.image = UIImage(named: "checkbox")
        self.imageCheck3.image = UIImage(named: "checkbox")
    }
    @IBAction func buttonCheck2Action(_ sender: Any) {
        self.privacyType = "PRIVATE"
        self.imageCheck1.image = UIImage(named: "checkbox")
        self.imageCheck2.image = UIImage(named: "select-checkbox")
        self.imageCheck3.image = UIImage(named: "checkbox")
    }
    @IBAction func buttonCheck3Action(_ sender: Any) {
        self.privacyType = "OPEN MIC"
        self.imageCheck1.image = UIImage(named: "checkbox")
        self.imageCheck2.image = UIImage(named: "checkbox")
        self.imageCheck3.image = UIImage(named: "select-checkbox")
    }
    @IBAction func buttonSubmitAction(_ sender: Any) {
        self.updateCommand()
        self.postCommand?.validate(completion: { (isValid, message) in
            
            // Start activity
            //self.startLoading()
            if(isValid){
                guard let postCommand = self.postCommand else{
                    return
                }
                self.postAudio(postCommand: postCommand)
            }else{
                Utility.showAlert(withMessage: message!, onController: self)
            }
        })
    }
}
extension PostView: bottomMenuDelegate{
    func selectedButton(selectedItem: NSInteger) {
        if selectedItem == 1 {
            //home
            let st = UIStoryboard.init(name: "HomeView", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"HomeView") as! HomeView
            self.navigationController?.pushViewController(controller, animated: true)
        }else if selectedItem == 2{
            //Feed
            let st = UIStoryboard.init(name: "Feed", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FeedView") as! FeedView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 3 {
        } else if selectedItem == 4 {
            let st = UIStoryboard.init(name: "Favourite", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FavouriteView") as! FavouriteView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 5 {
            let st = UIStoryboard.init(name: "Profile", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"ProfileView") as! ProfileView
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
extension PostView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoryListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PostCategoryTableCell = tableView.dequeueReusableCell(withIdentifier: "postCategoryTableCell") as! PostCategoryTableCell
        cell.selectionStyle = .none
        
        cell.labelCategoryTitle.text = self.categoryListArray[indexPath.row].categoryName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.isCategoryViewOpened = false
        self.viewCategoryBG.isHidden = true
        self.categoryID = self.categoryListArray[indexPath.row].id ?? ""
        self.textFieldCategory.text = self.categoryListArray[indexPath.row].categoryName
    }
}
extension PostView: IQAudioRecorderViewControllerDelegate {
    
    func audioRecorderController(_ controller: IQAudioRecorderViewController, didFinishWithAudioAtPath filePath: String) {
        controller.dismiss(animated: true, completion: nil)
        
        let filePathUrl = URL(fileURLWithPath: filePath)
        print(filePathUrl)
        
        if FileManager.default.fileExists(atPath: filePath) {
            do {
                let file = try Data(contentsOf: filePathUrl)
                self.audioFile = file
                
            } catch {
                print("Unable to load data: \(error)")
            }
        }
    }
    
    func audioRecorderControllerDidCancel(_ controller: IQAudioRecorderViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
extension PostView: IQAudioCropperViewControllerDelegate {
    func audioCropperController(_ controller: IQAudioCropperViewController, didFinishWithAudioAtPath filePath: String) {
        controller.dismiss(animated: true, completion: nil)
        
        let filePathUrl = URL(fileURLWithPath: filePath)
        print(filePathUrl)
    }
    
    func audioCropperControllerDidCancel(_ controller: IQAudioCropperViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension PostView : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let url = info[.mediaURL] as? URL {
            do {
                let file = try Data(contentsOf: url)
                self.videoFile = file
                
            } catch {
                print("Unable to load data: \(error)")
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
