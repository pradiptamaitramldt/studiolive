//
//  PostCategoryTableCell.swift
//  StudioLive
//
//  Created by Pradipta Maitra on 26/09/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class PostCategoryTableCell: UITableViewCell {

    @IBOutlet weak var labelCategoryTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
