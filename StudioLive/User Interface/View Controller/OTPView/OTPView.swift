//
//  OTPView.swift
//  StudioLive
//
//  Created by Pradipta on 10/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class OTPView: UIViewController {
    
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var otpText1: UITextField!
    @IBOutlet weak var otpText2: UITextField!
    @IBOutlet weak var otpText3: UITextField!
    @IBOutlet weak var otpText4: UITextField!
    
    var otp : String = ""
    var createOtp : String = ""
    var fromClass : String = ""
    var accountVerifyCommand: AccountVerifyCommand?
    var utility = Utility()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("otp...\(self.otp)")
        self.buttonSubmit.layer.cornerRadius=self.buttonSubmit.frame.size.height/2
        self.otpText1.attributedPlaceholder = NSAttributedString(string: "0",attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 146.0/255, green: 156.0/255, blue: 179.0/255, alpha: 1.0)])
        self.otpText2.attributedPlaceholder = NSAttributedString(string: "0",attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 146.0/255, green: 156.0/255, blue: 179.0/255, alpha: 1.0)])
        self.otpText3.attributedPlaceholder = NSAttributedString(string: "0",attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 146.0/255, green: 156.0/255, blue: 179.0/255, alpha: 1.0)])
        self.otpText4.attributedPlaceholder = NSAttributedString(string: "0",attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 146.0/255, green: 156.0/255, blue: 179.0/255, alpha: 1.0)])
        self.otpText1.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.otpText2.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.otpText3.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.otpText4.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.otpText1.delegate=self
        self.otpText2.delegate=self
        self.otpText3.delegate=self
        self.otpText4.delegate=self
        self.otpText1.keyboardType = .numberPad
        self.otpText2.keyboardType = .numberPad
        self.otpText3.keyboardType = .numberPad
        self.otpText4.keyboardType = .numberPad
        let text1 = self.otpText1.text ?? ""
        let text2 = self.otpText2.text ?? ""
        let text3 = self.otpText3.text ?? ""
        let text4 = self.otpText4.text ?? ""
        self.createOtp = text1 + text2 + text3 + text4
        self.setup()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let text = textField.text
        
        if text!.count >= 1{
            switch textField{
            case otpText1:
                otpText2.becomeFirstResponder()
            case otpText2:
                otpText3.becomeFirstResponder()
            case otpText3:
                otpText4.becomeFirstResponder()
            case otpText4:
                otpText4.resignFirstResponder()
            default:
                break
            }
        }
    }
    
    // MARK: - Private
    private func setup() {
        self.accountVerifyCommand = AccountVerifyCommand.init(otp: nil)
    }
    
    private func updateCommand() {
        self.accountVerifyCommand?.otp = self.createOtp
    }
    
    func verifyAccount(accountVerifyCommand: AccountVerifyCommand?) {
        
        let accountVerifyRepository = AccountVerifyRepository()
        accountVerifyRepository.verifyAccount(accountVerifyCommand: accountVerifyCommand!, vc: self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.responseCode == 2000 {
                    if (response.responseData?.userDetails?.id) != nil {
                        print("email...\(response.responseData?.userDetails?.email ?? "NA")")
                        print("id...\(response.responseData?.userDetails?.id ?? "NA")")
                        self.utility.Save(response.responseData?.authToken ?? "NA", keyname: Constants.Strings.Token)
                        self.utility.Save(response.responseData?.userDetails?.id ?? "NA", keyname: Constants.Strings.UserID)
                        self.utility.Save(response.responseData?.userDetails?.email ?? "NA", keyname: Constants.Strings.UserEmail)
                        self.utility.Save(response.responseData?.authToken ?? "NA", keyname: Constants.Strings.Token)
                        print("image : ",response.responseData?.imageUrl ?? "NA")
                        self.utility.Save(response.responseData?.imageUrl ?? "NA", keyname: Constants.Strings.profileImage)
                        self.utility.Save((response.responseData?.userDetails?.name ?? "NA"), keyname: Constants.Strings.userFullName)
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }else if response.responseCode == 2008 {
                    Utility.showAlert(withMessage: response.responseMessage ?? "Error", onController: self)
                }else if response.responseCode == 5002 {
                    Utility.showAlert(withMessage: response.responseMessage ?? "Error", onController: self)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func resendOTP() {
        
        let accountVerifyRepository = AccountVerifyRepository()
        accountVerifyRepository.resendOTP(vc: self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.responseCode == 2000 {
                    if (response.responseData?.email) != nil {
                        print("email...\(response.responseData?.email ?? "NA")")
                        print("otp...\(response.responseData?.otp ?? "NA")")
                        self.otp = (response.responseData?.otp)!
                    }
                }else if response.responseCode == 2008 {
                    Utility.showAlert(withMessage: response.responseMessage ?? "Error", onController: self)
                }else if response.responseCode == 5002 {
                    Utility.showAlert(withMessage: response.responseMessage ?? "Error", onController: self)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonResendOTPAction(_ sender: Any) {
        self.resendOTP()
    }
    @IBAction func buttonSubmitAction(_ sender: Any) {
        let text1 = self.otpText1.text ?? ""
        let text2 = self.otpText2.text ?? ""
        let text3 = self.otpText3.text ?? ""
        let text4 = self.otpText4.text ?? ""
        self.createOtp = text1 + text2 + text3 + text4
        
        self.updateCommand()
        self.accountVerifyCommand?.validate(completion: { (isValid, message) in
            
            // Start activity
            //self.startLoading()
            if(isValid){
                guard let accountVerifyCommand = self.accountVerifyCommand else{
                    return
                }
                if self.fromClass == "normal" {
                    self.verifyAccount(accountVerifyCommand: accountVerifyCommand)
                }else{
                    if self.createOtp == self.otp {
                        let st = UIStoryboard.init(name: "ChangePassword", bundle: nil)
                        let controller = st.instantiateViewController(withIdentifier:"CreatePasswordView") as! CreatePasswordView
                        self.navigationController?.pushViewController(controller, animated: true)
                    }else{
                        Utility.showAlert(withMessage: "OTP is incorrent", onController: self)
                    }
                }
                
            }else{
                Utility.showAlert(withMessage: message!, onController: self)
            }
        })
    }
    
}
extension OTPView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
