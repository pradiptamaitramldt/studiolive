//
//  ChangePasswordView.swift
//  StudioLive
//
//  Created by Subhajit on 10/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ChangePasswordView: UIViewController {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var submitBtnOutlet: UIButton!
    @IBOutlet weak var reEnterNewPassword: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var currentPasswordTextField: UITextField!
    
    var changePasswordCommand: ChangePasswordCommand?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.setup()
    }
    
    // MARK: - Private
    private func setup() {
        self.changePasswordCommand = ChangePasswordCommand.init(oldPassword: nil, newPassword: nil, confirmPassword: nil)
    }
    
    private func updateCommand() {
        self.changePasswordCommand?.oldPassword = self.currentPasswordTextField.text
        self.changePasswordCommand?.newPassword = self.newPasswordTextField.text
        self.changePasswordCommand?.confirmPassword = self.reEnterNewPassword.text
    }
    
    func changePassword(changePasswordCommand: ChangePasswordCommand?) {
        
        let changePasswordRepository = ChangePasswordRepository()
        changePasswordRepository.changePassword(changePasswordCommand: changePasswordCommand!, vc: self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode==200 {
                    Utility.showAlert(withMessage: response.message!, onController: self)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    private func setupView() {
        backView.layer.shadowOpacity = 0.3
        backView.layer.shadowOffset = CGSize(width: 0, height: 0)
        backView.layer.shadowRadius = 4.0
        backView.layer.cornerRadius = 6.0
        
        currentPasswordTextField.delegate = self
        newPasswordTextField.delegate = self
        reEnterNewPassword.delegate = self
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submitAction(_ sender: Any) {
        self.updateCommand()
        self.changePasswordCommand?.validate(completion: { (isValid, message) in
            if(isValid){
                guard let changePasswordCommand = self.changePasswordCommand else{
                    return
                }
                self.changePassword(changePasswordCommand: changePasswordCommand)
            }else{
                Utility.showAlert(withMessage: message!, onController: self)
            }
        })
    }
    
}


private typealias TextFielDelegate = ChangePasswordView
extension TextFielDelegate : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
