//
//  NotificationView.swift
//  StudioLive
//
//  Created by BrainiumSSD on 04/11/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class NotificationView: UIViewController {

    @IBOutlet var bottomView: UIView!
    @IBOutlet var notificationTableView: UITableView!
    
    var bottom_menu = BottomMenu()
    var notificationType = ""
    var utility = Utility()
    var userData : FetchNotificationRequestModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.bottomView.bounds.size.width,height:self.bottomView.bounds.size.height)
        self.bottomView.addSubview(bottom_menu)
        bottom_menu.delegate = self
        
        self.notificationTableView.register(UINib(nibName: "FriendRequestView", bundle: .main), forCellReuseIdentifier: "FriendRequestCell")
        self.notificationTableView.register(UINib(nibName: "NotificationCellView", bundle: .main), forCellReuseIdentifier: "NotificationCell")
               self.notificationTableView.dataSource = self
               self.notificationTableView.delegate = self
        notificationType = "FriendRequest"
        fetchAllNotification()
    }
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    func fetchAllNotification() {
        let fetchAllUserRepository = FetchNotificationRequestRepository()
        fetchAllUserRepository.fetchNotificationRequest(vc:  self) { (response, success, error) in
            self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                   // print("romio:\(response.statuscode)")
                    self.userData = response
                    print("getData : \(self.userData)")
                    self.notificationTableView.reloadData()
                }else if response.statuscode == 422 {
                    
                }else {
                    
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func stopLoading() {
        let load = Loader()
       load.hide(delegate: self)
    }
 }
extension NotificationView: bottomMenuDelegate{
    func selectedButton(selectedItem: NSInteger) {
        if selectedItem == 1 {
            //home
            let st = UIStoryboard.init(name: "HomeView", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"HomeView") as! HomeView
            self.navigationController?.pushViewController(controller, animated: true)
        }else if selectedItem == 2{
            //Feed
            let st = UIStoryboard.init(name: "Feed", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FeedView") as! FeedView
            self.navigationController?.pushViewController(controller, animated: true)
        }
       else if selectedItem == 3 {
            //Post
            let st = UIStoryboard.init(name: "Post", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"PostView") as! PostView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 4 {
            let st = UIStoryboard.init(name: "Favourite", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FavouriteView") as! FavouriteView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 5 {
            let st = UIStoryboard.init(name: "Profile", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"ProfileView") as! ProfileView
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
extension NotificationView : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userData?.responseData.userNot.count ?? 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if self.userData?.responseData.userNot[indexPath.row].notificationType == "REQUEST_RECEIVE" {
            return 150
        }
        else {
            return 120.0
        }
       
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.userData?.responseData.userNot[indexPath.row].notificationType == "REQUEST_RECEIVE" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FriendRequestCell") as! FriendRequestCell
           // cell.followUnfollowButton.setTitle("Unfollow", for: .normal)
            //pradipta@mailinator.com
            cell.labelName.text = self.userData?.responseData.userNot[indexPath.row].title
            cell.labelEmailID.text = self.userData?.responseData.userNot[indexPath.row].sendUserEmail
            cell.profileImage.af_setImage(
                withURL: URL(string:self.userData?.responseData.userNot[indexPath.row].profileImage ?? "")!,
                placeholderImage: UIImage(named: "userprofileDummy"),
                filter: nil,
                imageTransition: .crossDissolve(0.2)
            )
            cell.acceptButton.tag = indexPath.row
            cell.acceptButton.addTarget(self, action: #selector(acceptButtonAction(sender:)), for: .touchUpInside)
            cell.rejectButton.tag = indexPath.row
            cell.rejectButton.addTarget(self, action: #selector(rejectButtonAction(sender:)), for: .touchUpInside)
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            return cell
        }
        else {
             let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
                      // cell.followUnfollowButton.setTitle("Unfollow", for: .normal)
                       cell.contentView.backgroundColor = .clear
                       cell.backgroundColor = .clear
                       return cell
        }
        
    }
    //param["toCustomerId": "5f2d53624db503788439412b", "customerId": "5e6b2b2d6ecbce5af76b8576"]
    // pID : 5e6b2b2d6ecbce5af76b8576
     @objc func acceptButtonAction(sender: UIButton){
        
        requestResponse(tag: sender.tag, response: "YES")
    }
    @objc func rejectButtonAction(sender: UIButton){
        requestResponse(tag: sender.tag, response: "NO")
    }
    
    func requestResponse(tag : Int, response : String) {
        let fetchAllUserRepository = AcceptFriendRequestRepository()
         let userID = self.utility.Retrive(Constants.Strings.UserID)
        //5f2d53624db503788439412b
         let parameter = [ "customerId":userID as! String,
                           "toCustomerId" : self.userData?.responseData.userNot[tag].otherData as! String,
                           "isAccept":response]
         print("param\(parameter)")
         fetchAllUserRepository.acceptFriendRequest(vc:  self,parameter :parameter ) { (response, success, error) in
             //self.stopLoading()
             if (error != nil) {
                 Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
             }
             if (success) {
                 if response.statuscode == 200 {
                    // print("romio:\(response.statuscode)")
                    // self.userData = response
                    // print("getData : \(self.userData)")
                   //  self.tableViewFollowers.reloadData()
                  //  Utility.showAlert(withMessage: (response.message), onController: self)
                    self.fetchAllNotification()
                 }else if response.statuscode == 422 {
                     
                 }else {
                     
                 }
             }else{
                 Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
             }
         }
    }
    
}
