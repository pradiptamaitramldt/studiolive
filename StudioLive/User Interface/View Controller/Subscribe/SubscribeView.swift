//
//  SubscribeView.swift
//  StudioLive
//
//  Created by Pallab on 13/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

public enum SubscriptionPlanType: String {
    case silver = "silver", gold = "gold", platinum = "platinum", none = "none"
}

public struct SubscriptionPlan {
    
    public var subscriptionPlanType: SubscriptionPlanType
    public var subscriptionCurrency: String
    public var subscriptionPlanAmount: Float
    public var subscriptionPlanIsActivated: Bool
    
    init(type: SubscriptionPlanType = .none, currency: String = "$", amount: Float = 0.0, isActivated: Bool = false) {
        self.subscriptionPlanType = type
        self.subscriptionCurrency = currency
        self.subscriptionPlanAmount = amount
        self.subscriptionPlanIsActivated = isActivated
    }
    
}

class SubscribeView: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var controlPremiumPlanHeading: UIControl!
    @IBOutlet weak var collectionViewSubscriptionTypes: UICollectionView!
    @IBOutlet weak var buttonPayNow: UIButton!
    @IBOutlet weak var bottom_view: UIView!
    
    var bottom_menu = BottomMenu()
    
    //MARK: - Other Variables
    private var subscriptionPlans: [SubscriptionPlan] = [SubscriptionPlan(type: .silver, isActivated: true),
                                                         SubscriptionPlan(type: .gold, isActivated: false),
                                                         SubscriptionPlan(type: .platinum, isActivated: true)]
    
    private var selectedSubscriptionPlan: SubscriptionPlan? {
        didSet {
            DispatchQueue.main.async {
                self.collectionViewSubscriptionTypes.reloadData()
            }
        }
    }
    
    //MARK: - ViewCOntroller LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionViewSubscriptionTypes.register(UINib(nibName: "SubscriptionTypeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "SubscriptionTypeCollectionViewCell")
        self.collectionViewSubscriptionTypes.dataSource = self
        self.collectionViewSubscriptionTypes.delegate = self
        
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.bottom_view.bounds.size.width,height:self.bottom_view.bounds.size.height)
        self.bottom_view.addSubview(bottom_menu)
        bottom_menu.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.selectedSubscriptionPlan = SubscriptionPlan()
        self.selectedSubscriptionPlan?.subscriptionPlanType = .silver
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.buttonPayNow.cornerRadius = self.buttonPayNow.frame.size.height / 2
    }
    
    //MARK: - IBActions
    @IBAction func controlPremiumPlanHeadingClicked(_ sender: UIControl) {
        
    }
    
    @IBAction func buttonPayNowClicked(_ sender: UIButton, event: UIEvent) {
        
    }
    @IBAction func buttonMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
}
extension SubscribeView : bottomMenuDelegate{
    func selectedButton(selectedItem: NSInteger) {
        if selectedItem == 1 {
            //home
            let st = UIStoryboard.init(name: "HomeView", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"HomeView") as! HomeView
            self.navigationController?.pushViewController(controller, animated: true)
        }else if selectedItem == 2{
            //Feed
            let st = UIStoryboard.init(name: "Feed", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FeedView") as! FeedView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 3 {
            //Post
            let st = UIStoryboard.init(name: "Post", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"PostView") as! PostView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 4 {
            let st = UIStoryboard.init(name: "Favourite", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FavouriteView") as! FavouriteView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 5 {
            let st = UIStoryboard.init(name: "Profile", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"ProfileView") as! ProfileView
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
extension SubscribeView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.subscriptionPlans.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionTypeCollectionViewCell", for: indexPath) as! SubscriptionTypeCollectionViewCell
        if self.subscriptionPlans[indexPath.item].subscriptionPlanType == self.selectedSubscriptionPlan?.subscriptionPlanType {
            cell.viewContainer.backgroundColor = Constants.Color.themeBlueColor
            cell.labelSubscriptionAmount.textColor = Constants.Color.whiteColor
            cell.labelSubscriptionDescription.textColor = Constants.Color.whiteColor
            cell.labelSubscriptionPlanType.textColor = Constants.Color.whiteColor
        } else {
            cell.viewContainer.backgroundColor = Constants.Color.whiteColor
            cell.labelSubscriptionAmount.textColor = Constants.Color.themeBlueColor
            cell.labelSubscriptionDescription.textColor = Constants.Color.themeBlueColor
            cell.labelSubscriptionPlanType.textColor = Constants.Color.themeBlueColor
        }
        cell.labelSubscriptionAmount.text = "\(self.subscriptionPlans[indexPath.item].subscriptionCurrency)\(self.subscriptionPlans[indexPath.item].subscriptionPlanAmount)"
        cell.labelSubscriptionDescription.text = "Premium"
        cell.labelSubscriptionPlanType.text = self.subscriptionPlans[indexPath.item].subscriptionPlanType.rawValue.capitalizingFirstLetter()
        return cell
    }
}

extension SubscribeView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedSubscriptionPlan = self.subscriptionPlans[indexPath.item]
    }
    
}

extension SubscribeView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.size.width - (20.0 * 2))/3, height: collectionView.bounds.size.height)
    }
    
}
