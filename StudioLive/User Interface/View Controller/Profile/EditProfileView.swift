//
//  EditProfileView.swift
//  StudioLive
//
//  Created by Pallab on 13/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class EditProfileView: UITableViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imageViewProfilePicture: UIImageView!
    @IBOutlet weak var buttonChangeProfilePicture: UIButton!
    @IBOutlet weak var textFieldFirstName: UITextField!
    @IBOutlet weak var textFieldLastName: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPhoneNumber: UITextField!
    @IBOutlet weak var buttonSaveProfile: UIButton!
    
    var editProfileCommand: EditProfileCommand?
    var utility = Utility()
    let imagePicker = UIImagePickerController()
    
    //MARK: - ViewCOntroller LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textFieldFirstName.delegate=self
        self.textFieldLastName.delegate=self
        self.textFieldEmail.delegate=self
        self.textFieldPhoneNumber.delegate=self
        self.viewProfile()
        self.setup()
    }
    
    // MARK: - Private
    private func setup() {
        self.editProfileCommand = EditProfileCommand.init(firstName: nil, lastName: nil, email: nil, phone: nil)
    }
    
    private func updateCommand() {
        self.editProfileCommand?.email = self.textFieldEmail.text?.trimmingCharacters(in: .whitespaces)
        self.editProfileCommand?.firstName = self.textFieldFirstName.text
        self.editProfileCommand?.lastName = self.textFieldLastName.text
        self.editProfileCommand?.phone = self.textFieldPhoneNumber.text
    }
    
    func viewProfile() {
        let viewProfileRepository = ViewProfileRepository()
        viewProfileRepository.viewProfile(vc:  self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    if let firstName = response.responseData?.firstName {
                        if let lastName = response.responseData?.lastName {
                            if let email = response.responseData?.email {
                                if let phone = response.responseData?.phone{
                                    self.textFieldFirstName.text = firstName
                                    self.textFieldLastName.text = lastName
                                    self.textFieldEmail.text = email
                                    self.textFieldPhoneNumber.text = String(phone)
                                }
                            }
                        }
                    }
                    if let profileImage = response.responseData?.profileImage {
                        if profileImage != "" {
                            self.imageViewProfilePicture.af_setImage(
                                withURL: URL(string: profileImage)!,
                                placeholderImage: UIImage(named: "userprofileDummy"),
                                filter: nil,
                                imageTransition: .crossDissolve(0.2)
                            )
                        }else{
                            self.imageViewProfilePicture.image = UIImage(named: "userprofileDummy")
                        }
                    }
                }else if response.statuscode == 422 {
                    
                }else {
                    
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func performEditProfile(editProfileCommand: EditProfileCommand?) {
        
        let editProfileRepository = EditProfileRepository()
        editProfileRepository.editProfile(editProfileCommand: editProfileCommand!, vc: self) { (response, success, error) in
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                
                let fName = ( self.editProfileCommand?.firstName ?? "") + " " +   ( self.editProfileCommand?.lastName ?? " ")
                   self.utility.Save(fName, keyname: Constants.Strings.userFullName)
                  NotificationCenter.default.post(name: Notification.Name("UpdateSliderProfileValue"), object: nil)
                
                Utility.showAlert(withMessage: response.message ?? "", onController: self)
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    // MARK: - Method Alert Controller
    func AlertController()
    {
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        // 2
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Camera")
            self.imageCamera()
        })
        let galleryAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Gallery")
            self.imageGallery()
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        
        // 4
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    //MARK: - UITableViewDelegate & UITableViewDataSource Methods
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            //navigation bar
            return 94.0
        case 1:
            //profile picture view
            return tableView.bounds.size.width * (175.0 / 320.0)
        case 6:
            //save profile view
            return tableView.bounds.size.width * (60.0 / 320.0)
        default:
            //other text fields
            return tableView.bounds.size.width * (80.0 / 320.0)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
    
    //MARK: - IBActions
    @IBAction func buttonMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    @IBAction func buttonChangeProfilePictureClicked(_ sender: UIButton, event: UIEvent) {
        self.AlertController()
    }
    
    @IBAction func buttonSaveProfileClicked(_ sender: UIButton, event: UIEvent) {
        self.updateCommand()
        self.editProfileCommand?.validate(completion: { (isValid, message) in
            
            // Start activity
            //self.startLoading()
            if(isValid){
                guard let editProfileCommand = self.editProfileCommand else{
                    return
                }
                self.performEditProfile(editProfileCommand: editProfileCommand)
            }else{
                Utility.showAlert(withMessage: message!, onController: self)
            }
        })
    }
    @IBAction func buttonChangeEmailAction(_ sender: Any) {
        let st = UIStoryboard.init(name: "ChangeEmailAddress", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ChangeEmailAddressView") as! ChangeEmailAddressView
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func buttonChangePasswordAction(_ sender: Any) {
        let st = UIStoryboard.init(name: "ChangePassword", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ChangePasswordView") as! ChangePasswordView
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}
extension EditProfileView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension EditProfileView : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imageGallery() {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imageCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        let profileImage = self.fixImageOrientation(img!)
        self.imageViewProfilePicture.image=profileImage
        
        let profileUploadRepository = ProfileImageUploadRepository()
        profileUploadRepository.uploadProfileImage(profileImage: profileImage, vc: self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                let statusCode = response["STATUSCODE"] as! String
                if statusCode == "200" {
                    let message = response["message"]
                    Utility.showAlert(withMessage: message as! String, onController: self)
                }
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func fixImageOrientation(_ image: UIImage)->UIImage {
        UIGraphicsBeginImageContext(image.size)
        image.draw(at: .zero)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? image
    }
}
