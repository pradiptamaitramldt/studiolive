//
//  ProfileView.swift
//  StudioLive
//
//  Created by Pradipta on 15/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ProfileView: UIViewController {
    
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var botton_view: UIView!
    @IBOutlet weak var table_view: UITableView!
    
    var bottom_menu = BottomMenu()
    private var tableViewHeader: ProfileHeaderView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.botton_view.bounds.size.width,height:self.botton_view.bounds.size.height)
        self.botton_view.addSubview(bottom_menu)
        bottom_menu.delegate = self
        self.bottom_menu.profileSeleted()
        
        self.table_view.delegate = self
        self.table_view.dataSource = self
        self.table_view.register(UINib(nibName: "VideoTableViewCell", bundle: .main), forCellReuseIdentifier: "VideoTableViewCell")
        self.table_view.register(UINib(nibName: "SliderTableViewCell", bundle: .main), forCellReuseIdentifier: "SliderTableViewCell")
        
        self.textFieldSearch.attributedPlaceholder = NSAttributedString(string: "search here",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white ])
       self.viewProfile()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if self.tableViewHeader == nil {
               tableViewHeader = ProfileHeaderView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.width * (14/15)))
               self.table_view.tableHeaderView = tableViewHeader
               //self.tableViewHeader.setUpUI()
               self.tableViewHeader.controlFollowStatus.layer.cornerRadius=self.tableViewHeader.controlFollowStatus.bounds.height/2
                self.tableViewHeader.controlFollowersCount.layer.cornerRadius=self.tableViewHeader.controlFollowersCount.bounds.height/2
               self.tableViewHeader.delegate = self
        
           }
        if SingleToneClass.shared.didFollowersButtonClick {
            tableViewHeader.controlFollowersCount.backgroundColor = UIColor(red: 7/255, green: 19/255, blue: 61/255, alpha: 1.0)
            tableViewHeader.labelFollowersCount.textColor = .white
            tableViewHeader.controlFollowStatus.backgroundColor = .white
            tableViewHeader.labelFollowStatus.textColor = .black
        }
        else {
            tableViewHeader.controlFollowStatus.backgroundColor = UIColor(red: 7/255, green: 19/255, blue: 61/255, alpha: 1.0)
            tableViewHeader.labelFollowStatus.textColor = .white
            tableViewHeader.labelFollowersCount.textColor = .black
            tableViewHeader.controlFollowersCount.backgroundColor = .white
        }
    
    }
    
    func viewProfile() {
        let viewProfileRepository = ViewProfileRepository()
        viewProfileRepository.viewProfile(vc:  self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    if let name = response.responseData?.name {
                        if let email = response.responseData?.email {
                            if let profileImage = response.responseData?.profileImage {
                                self.tableViewHeader.setValues(name: name, email: email, profileImage: profileImage, connectedUser: String(response.responseData?.connectedUser ?? 0))
                                self.table_view.reloadData()
                            }else{
                                self.tableViewHeader.setValues(name: name, email: email, profileImage: "", connectedUser: String(response.responseData?.connectedUser ?? 0))
                                self.table_view.reloadData()
                            }
                        }
                    }
                }else if response.statuscode == 422 {
                    
                }else {
                    
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func buttonMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    @IBAction func buttonBellAction(_ sender: Any) {
        let st = UIStoryboard.init(name: "Notification", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"NotificationView") as! NotificationView
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
extension ProfileView: bottomMenuDelegate{
    func selectedButton(selectedItem: NSInteger) {
        if selectedItem == 1 {
            //home
            let st = UIStoryboard.init(name: "HomeView", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"HomeView") as! HomeView
            self.navigationController?.pushViewController(controller, animated: true)
        }else if selectedItem == 2{
            //Feed
            let st = UIStoryboard.init(name: "Feed", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FeedView") as! FeedView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 3 {
            //Post
            let st = UIStoryboard.init(name: "Post", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"PostView") as! PostView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 4 {
            let st = UIStoryboard.init(name: "Favourite", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FavouriteView") as! FavouriteView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 5 {
        }
    }
}
extension ProfileView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SliderTableViewCell") as! SliderTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SliderTableViewCell") as! SliderTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        } else if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SliderTableViewCell") as! SliderTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.size.width * (280 / 320)
    }
}
extension ProfileView : FollowerDelegate {
    func goToFollowerPage(type: String) {
        let st = UIStoryboard.init(name: "MyFollowers", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"MyFollowersView") as! MyFollowersView
        controller.userType = type
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func goToEditProfilePage() {
        let storyBoard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "EditProfileView") as! EditProfileView
        SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
    }
}
