//
//  ForgotPasswordView.swift
//  StudioLive
//
//  Created by Pradipta on 09/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ForgotPasswordView: UIViewController {

    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var textFieldEmail: UITextField!
    
    var forgotPasswordCommand: ForgotPasswordCommand?
    var utility = Utility()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setup()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Private
    private func setup() {
        self.forgotPasswordCommand = ForgotPasswordCommand.init(email: nil)
    }
    
    private func updateCommand() {
        self.forgotPasswordCommand?.email = self.textFieldEmail.text?.trimmingCharacters(in: .whitespaces)
    }
    
    func forgotPassword(forgotPasswordCommand: ForgotPasswordCommand?) {
        
        let forgotPasswordRepository = ForgotPasswordRepository()
        forgotPasswordRepository.forgotPassword(forgotPasswordCommand: forgotPasswordCommand!, vc: self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.responseCode == 2000 {
                    if (response.responseData?.forgotPassOtp) != nil {
                        print("email...\(response.responseData?.email ?? "NA")")
                        print("id...\(response.responseData?.forgotPassOtp ?? "NA")")
                        self.utility.Save(response.responseData?.email ?? "NA", keyname: Constants.Strings.UserEmail)
                        let alert = UIAlertController(title: Constants.Strings.AppName, message: response.responseMessage, preferredStyle: UIAlertController.Style.alert)
                        
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                            //Cancel Action
                            let st = UIStoryboard.init(name: "Main", bundle: nil)
                            let controller = st.instantiateViewController(withIdentifier:"OTPView") as! OTPView
                            controller.otp = response.responseData?.forgotPassOtp ?? "0"
                            controller.fromClass = "forgotPassword"
                            self.navigationController?.pushViewController(controller, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else if response.responseCode == 2008 {
                    
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    @IBAction func buttonSubmitAction(_ sender: Any) {
        self.updateCommand()
        self.forgotPasswordCommand?.validate(completion: { (isValid, message) in
            
            // Start activity
            //self.startLoading()
            if(isValid){
                guard let forgotPasswordCommand = self.forgotPasswordCommand else{
                    return
                }
                self.forgotPassword(forgotPasswordCommand: forgotPasswordCommand)
            }else{
                Utility.showAlert(withMessage: message!, onController: self)
            }
        })
    }
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
