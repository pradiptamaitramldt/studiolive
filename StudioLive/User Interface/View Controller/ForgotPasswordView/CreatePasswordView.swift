//
//  CreatePasswordView.swift
//  StudioLive
//
//  Created by Pradipta on 20/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class CreatePasswordView: UIViewController {

    @IBOutlet weak var textField_password: UITextField!
    @IBOutlet weak var textField_confirmPassword: UITextField!
    
    var createPasswordCommand: CreatePasswordCommand?
    var utility = Utility()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setup()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Private
    private func setup() {
        self.createPasswordCommand = CreatePasswordCommand.init(password: nil, confirmPassword: nil)
    }
    
    private func updateCommand() {
        self.createPasswordCommand?.password = self.textField_password.text
        self.createPasswordCommand?.confirmPassword = self.textField_confirmPassword.text
    }
    
    func createPassword(createPasswordCommand: CreatePasswordCommand?) {
        
        let resetPasswordRepository = ResetPasswordRepository()
        resetPasswordRepository.resetPassword(createPasswordCommand: createPasswordCommand!, vc: self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.responseCode == 2000 {
                    let alert = UIAlertController(title: Constants.Strings.AppName, message: response.responseMessage, preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                        //Cancel Action
                        self.navigationController?.popToRootViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else if response.responseCode == 2008 {
                    Utility.showAlert(withMessage: response.responseMessage!, onController: self)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSubmitAction(_ sender: Any) {
        self.updateCommand()
        self.createPasswordCommand?.validate(completion: { (isValid, message) in
            
            // Start activity
            //self.startLoading()
            if(isValid){
                guard let createPasswordCommand = self.createPasswordCommand else{
                    return
                }
                self.createPassword(createPasswordCommand: createPasswordCommand)
            }else{
                Utility.showAlert(withMessage: message!, onController: self)
            }
        })
    }
}
