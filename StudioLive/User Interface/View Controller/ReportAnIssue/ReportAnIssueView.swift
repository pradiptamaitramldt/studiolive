//
//  ReportAnIssueView.swift
//  StudioLive
//
//  Created by Subhajit on 10/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ReportAnIssueView: UIViewController {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var chooseIssueTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }
    
    
    private func setupView() {
        backView.layer.shadowOpacity = 0.3
        backView.layer.shadowOffset = CGSize(width: 0, height: 0)
        backView.layer.shadowRadius = 4.0
        backView.layer.cornerRadius = 6.0
        
        commentsTextView.delegate = self
        
        
    }

    @IBAction func backAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    
    @IBAction func submitAction(_ sender: Any) {
    }
    
}


private typealias TextViewDelegate = ReportAnIssueView
extension TextViewDelegate : UITextViewDelegate {
    
}
