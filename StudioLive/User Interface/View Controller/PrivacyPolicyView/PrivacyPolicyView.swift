//
//  PrivacyPolicyView.swift
//  StudioLive
//
//  Created by Pradipta on 27/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class PrivacyPolicyView: UIViewController {

    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.fetchCMSData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    func fetchCMSData() {
        let cmsRepository = CMSRepository()
        cmsRepository.fetchCMSData(slug: "privacy", vc: self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.responseCode == 2000 {
                    let description = response.responseData?.responseDataDescription
                    let htmlData = NSString(string: description ?? "Descriptions").data(using: String.Encoding.unicode.rawValue)
                    
                    let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
                    
                    let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
                    self.textView.attributedText = attributedString
                }else {
                    
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func buttonMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    
}
