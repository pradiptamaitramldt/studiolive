//
//  HomeView.swift
//  StudioLive
//
//  Created by Pradipta on 10/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class HomeView: UIViewController {
    
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var bottom_view: UIView!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet var labelShowHide: UILabel!
    
    var dashboardArray: [Post] = []
    var bottom_menu = BottomMenu()
    var urlPath: String = ""
    var updater: CADisplayLink! = nil
    var utility = Utility()
    var selectedIndexPath = Int(),audioUrl : URL?
    var didAudioCurrentlyPlay = false
    var didVideoCurrentlyPlay = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.bottom_view.bounds.size.width,height:self.bottom_view.bounds.size.height)
        self.bottom_view.addSubview(bottom_menu)
        bottom_menu.delegate = self
        bottom_menu.homeSelected()
        
        self.table_view.register(UINib(nibName: "VideoTableViewCell", bundle: .main), forCellReuseIdentifier: "VideoTableViewCell")
        self.table_view.register(UINib(nibName: "SliderTableViewCell", bundle: .main), forCellReuseIdentifier: "SliderTableViewCell")
        
        self.table_view.delegate = self
        self.table_view.dataSource = self
        self.table_view.decelerationRate = .init(rawValue: 1.00)
        self.textFieldSearch.attributedPlaceholder = NSAttributedString(string: "search here",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white ])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.fetchDashboardData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    func fetchDashboardData() {
        let homeRepository = HomeRepository()
        homeRepository.fetchHomeData(vc:  self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    self.urlPath = response.responseData?.path ?? ""
                    let arrayAll = response.responseData?.post ?? []
                    self.dashboardArray.removeAll()
                    for item in arrayAll {
                        self.dashboardArray.append(item)
                    }
                    if self.dashboardArray.count > 0 {
                        DispatchQueue.main.async {
                            self.table_view.reloadData()
                            self.table_view.isHidden = false
                            self.labelShowHide.isHidden = true
                        }
                        print("Post",self.dashboardArray)
                    }
                    else {
                        DispatchQueue.main.async {
                            // Utility.showAlert(withMessage: "You have no posts to show", onController: self)
                            self.table_view.isHidden = true
                            self.labelShowHide.isHidden = false
                        }
                    }
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func like(isLike: String, musicID: String, likeCustomerID: String) {
        let homeRepository = HomeRepository()
        homeRepository.likeUnlikePost(musicID: musicID, likeCustomerID: likeCustomerID, isLike: isLike, vc:  self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    self.fetchDashboardData()
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func favourite(isFav: String, musicID: String, favCustomerID: String) {
        let homeRepository = HomeRepository()
        homeRepository.favUnfavPost(musicID: musicID, favCustomerID: favCustomerID, isFav: isFav, vc:  self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    self.fetchDashboardData()
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    @IBAction func buttonBellAction(_ sender: Any) {
        
        let st = UIStoryboard.init(name: "Notification", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"NotificationView") as! NotificationView
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
extension HomeView: bottomMenuDelegate{
    func selectedButton(selectedItem: NSInteger) {
        if selectedItem == 1 {
            //home
        }else if selectedItem == 2{
            //Feed
            let st = UIStoryboard.init(name: "Feed", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FeedView") as! FeedView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 3 {
            //Post
            let st = UIStoryboard.init(name: "Post", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"PostView") as! PostView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 4 {
            let st = UIStoryboard.init(name: "Favourite", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FavouriteView") as! FavouriteView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 5 {
            let st = UIStoryboard.init(name: "Profile", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"ProfileView") as! ProfileView
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
extension HomeView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dashboardArray.count
    }
    /*
     (title: Optional("my song"), file: Optional("music-668-1601303115.m4a"), location: Optional("kolkata"), musicID: Optional("5f71f24bf9cb231a722c75d5"), fileType: Optional("AUDIO"), studioPrivacy: Optional("PRIVATE"), createdAt: Optional("2020-09-28T14:25:15.045Z"), updatedAt: Optional("2020-09-28T14:25:15.045Z"), customerID: Optional("5e6b2b2d6ecbce5af76b8576"), musicCategory: Optional("Dance"), likeNo: Optional(0), isLike: Optional(true), commentNo: Optional(0), favouriteNo: Optional(0), isFavourite: Optional(true), customerName: Optional(""), profileImage: Optional(""))
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let fileType = self.dashboardArray[indexPath.row].fileType
        if fileType == "VIDEO" {
            //   print("cellVideo")
            let videoCell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
            let url = "\(self.urlPath)\(self.dashboardArray[indexPath.row].file ?? "")"
            let videoURL = URL(string: url)!
            //      if didAudioCurrentlyPlay {
            //         didAudioCurrentlyPlay = false
            //
            //   }
            //  else {
            
            //                if didVideoCurrentlyPlay {
            //                   // NotificationCenter.default.post(name: Notification.Name("PauseVideo"), object: nil)
            //                     videoCell.playerController.player?.pause()
            //                  //  videoCell.playerController.player = nil
            //                   // videoCell.playerLayer.player = nil
            //                    didVideoCurrentlyPlay = false
            //                }
            //                else {
            //
            //                }
            
            //    didVideoCurrentlyPlay = true
            // }
            //  NotificationCenter.default.post(name: Notification.Name("PauseAudio"), object: audioUrl)
            videoCell.viewDeleteBG.isHidden = true
            videoCell.playerController.view.frame = videoCell.newVideoContainerView.bounds
            videoCell.newVideoContainerView.addSubview(videoCell.playerController.view)
            videoCell.playerController.player = AVPlayer(url: videoURL as URL )
            videoCell.playerLayer = AVPlayerLayer(layer: videoCell.playerController.player as Any)
            videoCell.playerLayer.videoGravity = .resize
            videoCell.playerLayer.frame = videoCell.newVideoContainerView.bounds
            videoCell.playerController.player?.isMuted = false
//            videoCell.playerController.player?.seek(to: CMTime.zero)
//            videoCell.playerController.player?.play()
            videoCell.labelUserName.text = self.dashboardArray[indexPath.row].customerName //self.utility.Retrive(Constants.Strings.userFullName) as? String
            videoCell.imageViewUserProfileImage.af_setImage(
                withURL: URL(string:self.dashboardArray[indexPath.row].profileImage ?? "")!,
                placeholderImage: UIImage(named: "userprofileDummy"),
                filter: nil,
                imageTransition: .crossDissolve(0.2))
            videoCell.labelPostedTime.text = self.dashboardArray[indexPath.row].updatedAt ?? ""
            videoCell.labelLikeCount.text = "\(self.dashboardArray[indexPath.row].likeNo ?? 0)"
            videoCell.labelCommentCount.text = "\(self.dashboardArray[indexPath.row].commentNo ?? 0)"
            videoCell.labelMusicGenre.text = self.dashboardArray[indexPath.row].musicCategory
            
            let isLike = self.dashboardArray[indexPath.row].isLike
            if isLike == false {
                videoCell.imageViewLike.image = UIImage(named: "Done_icon")
            }else{
                videoCell.imageViewLike.image = UIImage(named: "liked")
            }
            
            let isFav = self.dashboardArray[indexPath.row].isFavourite
            if isFav == false {
                videoCell.buttonFavourite.setImage(UIImage(named: "favourites_gray_icon"), for: .normal)
            }else{
                videoCell.buttonFavourite.setImage(UIImage(named: "favourites_icon"), for: .normal)
            }
            videoCell.buttonLike.tag = indexPath.row
            videoCell.buttonFavourite.tag = indexPath.row
            videoCell.delegate = self
            return videoCell
        }
        else {
            // print("cellAudio")
            let audioCell = tableView.dequeueReusableCell(withIdentifier: "SliderTableViewCell") as! SliderTableViewCell
            audioCell.viewDeleteBG.isHidden = true
            audioCell.delegate = self
            audioCell.buttonLike.tag = indexPath.row
            audioCell.buttonFav.tag = indexPath.row
            audioCell.audioSlider.tag = indexPath.row
            audioCell.labelCategory.text = self.dashboardArray[indexPath.row].musicCategory
            audioCell.labelLikeCount.text = "\(self.dashboardArray[indexPath.row].likeNo ?? 0)"
            audioCell.labelCommentCount.text = "\(self.dashboardArray[indexPath.row].commentNo ?? 0)"
            
            audioCell.labelHeader.text = self.utility.Retrive(Constants.Strings.userFullName) as? String
            audioCell.imageViewProfileImage.af_setImage(
                withURL: URL(string:self.utility.Retrive(Constants.Strings.profileImage) as! String)!,
                placeholderImage: UIImage(named: "userprofileDummy"),
                filter: nil,
                imageTransition: .crossDissolve(0.2))
            let isLike = self.dashboardArray[indexPath.row].isLike
            if isLike == false {
                audioCell.imageViewLike.image = UIImage(named: "Done_icon")
            }else{
                audioCell.imageViewLike.image = UIImage(named: "liked")
            }
            
            let isFav = self.dashboardArray[indexPath.row].isFavourite
            if isFav == false {
                audioCell.buttonFav.setImage(UIImage(named: "favourites_gray_icon"), for: .normal)
            }else{
                audioCell.buttonFav.setImage(UIImage(named: "favourites_icon"), for: .normal)
            }
            audioCell.labelTimeAgo.text = self.dashboardArray[indexPath.row].updatedAt ?? ""
            audioCell.labelCategory.text = self.dashboardArray[indexPath.row].musicCategory
            audioCell.labelLocation.text = self.dashboardArray[indexPath.row].location
            audioCell.labelTitle.text = self.dashboardArray[indexPath.row].title
            return audioCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let fileType = self.dashboardArray[indexPath.row].fileType
        if fileType == "VIDEO" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
            print("willCellVideo")
            
            
            // print("video...\(String(describing: videoURL))")
            
            
            // cell.playerController.player?.pause()
            self.addChild(cell.playerController)
            
        }  else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SliderTableViewCell") as! SliderTableViewCell
            print("willCellAudio")
            let url = "\(self.urlPath)\(self.dashboardArray[indexPath.row].file ?? "")"
            let audioUrl = URL(string: url)!
            
            
            
            
            //     print("Audio row",indexPath.row)
            let completelyVisible = tableView.isCellVisible(section: indexPath.section, row: indexPath.row)
            if completelyVisible {
                downloadFileFromURL(url: (audioUrl ))
            }
            else {
                cell.audioPlayer.pause()
            }
        }
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let fileType = self.dashboardArray[indexPath.row].fileType
        if fileType == "VIDEO" {
            //  print("endCellVideo")
            let videoCell: VideoTableViewCell = cell as! VideoTableViewCell
            videoCell.playerController.player?.pause()
            videoCell.playerController.player = nil
            videoCell.playerLayer.player = nil
            
        }else{
            //    print("endCellAudio")
            let audioCell: SliderTableViewCell = cell as! SliderTableViewCell
            audioCell.audioPlayer.pause()
            NotificationCenter.default.post(name: Notification.Name("PauseAudio"), object: audioUrl)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let fileType = self.dashboardArray[indexPath.row].fileType
        if fileType == "VIDEO" {
            return tableView.bounds.size.width * 1.2
        }
        else {
            return tableView.bounds.size.width * (250 / 320)
        }
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollViewDidScroll")
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let fileType = self.dashboardArray[indexPath.row].fileType
        //        if fileType == "VIDEO" {
        //            let videoCell: VideoTableViewCell = tableView.cellForRow(at: indexPath) as! VideoTableViewCell
        //            let playerVC = AVPlayerViewController()
        //            playerVC.player = videoCell.viewVideoPlayerContainer.player
        //            self.present(playerVC, animated: true){
        //                playerVC.player?.play()
        //            }
        //        }else{
        //            let audioCell: SliderTableViewCell = tableView.cellForRow(at: indexPath) as! SliderTableViewCell
        //            let playerVC = AVPlayerViewController()
        //            playerVC.player = audioCell.audioPlayerView.player
        //            self.present(playerVC, animated: true){
        //                playerVC.player?.play()
        //            }
        //        }
    }
}
extension HomeView {
    
    func downloadFileFromURL(url:URL){
        print("downloadFileFromURL")
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url as URL, completionHandler: { [weak self](url, response, error) -> Void in
            self?.play(url: ((url ?? URL(string: "url"))!)  )
        })
        downloadTask.resume()
    }
    
    func play(url:URL) {
        print("playing")
        audioUrl = url
        didAudioCurrentlyPlay = true
        NotificationCenter.default.post(name: Notification.Name("ReadyToPlayAudio"), object: audioUrl)
    }
}
extension HomeView: AVAudioPlayerDelegate {
    
}
extension HomeView: AudioCellDelegate {
    func deleteAudioAction(_ sender: UIButton) {
        
    }
    
    func likeAudioAction(_ sender: UIButton) {
        let fileType = self.dashboardArray[sender.tag].fileType
        if fileType == "AUDIO" {
            let isLike = self.dashboardArray[sender.tag].isLike ?? false
            let musicID = self.dashboardArray[sender.tag].musicID ?? ""
            let likeCustomerID = self.dashboardArray[sender.tag].customerID ?? ""
            if isLike == true {
                self.like(isLike: "NO", musicID: musicID, likeCustomerID: likeCustomerID)
            }else{
                self.like(isLike: "YES", musicID: musicID, likeCustomerID: likeCustomerID)
            }
        }
    }
    
    func commentAudioAction(_ sender: UIButton) {
        
    }
    
    func favAudioAction(_ sender: UIButton) {
        let fileType = self.dashboardArray[sender.tag].fileType
        if fileType == "AUDIO" {
            let isFav = self.dashboardArray[sender.tag].isFavourite ?? false
            let musicID = self.dashboardArray[sender.tag].musicID ?? ""
            let favCustomerID = self.dashboardArray[sender.tag].customerID ?? ""
            if isFav == true {
                self.favourite(isFav: "NO", musicID: musicID, favCustomerID: favCustomerID)
            }else{
                self.favourite(isFav: "YES", musicID: musicID, favCustomerID: favCustomerID)
            }
        }
    }
    
    func playPause(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.table_view)
        let indexPath = self.table_view.indexPathForRow(at:buttonPosition)
        let cell: SliderTableViewCell = self.table_view.cellForRow(at: indexPath!) as! SliderTableViewCell
        //        if cell.audioPlayer.duration == 0 {
        //
        //                } else {
        //                    cell.buttonPlayPause.setImage(UIImage(named: "play_icon"), for: .normal)
        //                    cell.audioPlayer.pause()
        //                }
        cell.buttonPlayPause.setImage(UIImage(named: "pause_icon"), for: .normal)
        cell.audioPlayer.play()
    }
}
extension Date {
    func timeAgoDisplay() -> String {
        
        let calendar = Calendar.current
        let minuteAgo = calendar.date(byAdding: .minute, value: -1, to: Date())!
        let hourAgo = calendar.date(byAdding: .hour, value: -1, to: Date())!
        let dayAgo = calendar.date(byAdding: .day, value: -1, to: Date())!
        let weekAgo = calendar.date(byAdding: .day, value: -7, to: Date())!
        
        if minuteAgo < self {
            let diff = Calendar.current.dateComponents([.second], from: self, to: Date()).second ?? 0
            return "\(diff) sec ago"
        } else if hourAgo < self {
            let diff = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute ?? 0
            return "\(diff) min ago"
        } else if dayAgo < self {
            let diff = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour ?? 0
            return "\(diff) hrs ago"
        } else if weekAgo < self {
            let diff = Calendar.current.dateComponents([.day], from: self, to: Date()).day ?? 0
            return "\(diff) days ago"
        }
        let diff = Calendar.current.dateComponents([.weekOfYear], from: self, to: Date()).weekOfYear ?? 0
        return "\(diff) weeks ago"
    }
}
extension HomeView: VideoCellDelegate {
    func deleteAction(_ sender: UIButton) {
        
    }
    
    func likeVideoAction(_ sender: UIButton) {
        let fileType = self.dashboardArray[sender.tag].fileType
        if fileType == "VIDEO" {
            let isLike = self.dashboardArray[sender.tag].isLike ?? false
            let musicID = self.dashboardArray[sender.tag].musicID ?? ""
            let likeCustomerID = self.dashboardArray[sender.tag].customerID ?? ""
            if isLike == true {
                self.like(isLike: "NO", musicID: musicID, likeCustomerID: likeCustomerID)
            }else{
                self.like(isLike: "YES", musicID: musicID, likeCustomerID: likeCustomerID)
            }
        }
    }
    
    func commentVideoAction(_ sender: UIButton) {
        
    }
    
    func favVideoAction(_ sender: UIButton) {
        let fileType = self.dashboardArray[sender.tag].fileType
        if fileType == "VIDEO" {
            let isFav = self.dashboardArray[sender.tag].isFavourite ?? false
            let musicID = self.dashboardArray[sender.tag].musicID ?? ""
            let favCustomerID = self.dashboardArray[sender.tag].customerID ?? ""
            if isFav == true {
                self.favourite(isFav: "NO", musicID: musicID, favCustomerID: favCustomerID)
            }else{
                self.favourite(isFav: "YES", musicID: musicID, favCustomerID: favCustomerID)
            }
        }
    }
}
