//
//  LoginView.swift
//  StudioLive
//
//  Created by Pradipta on 09/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class LoginView: UIViewController {

    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var buttonFB: UIButton!
    @IBOutlet weak var buttonGoogle: UIButton!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    var loginCommand: LoginCommand?
    var utility = Utility()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let userID: String = self.utility.Retrive(Constants.Strings.UserID) as! String
        
        if !userID.isEmpty {
            let storyboard = UIStoryboard(name: "HomeView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "HomeView") as! HomeView
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
            
        }
        
        self.setup()
    }
    
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textFieldEmail.text = ""
        self.textFieldPassword.text = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.buttonLogin.layer.cornerRadius = self.buttonLogin.frame.size.height/2
        self.buttonFB.layer.cornerRadius = self.buttonFB.frame.size.height/2
        self.buttonGoogle.layer.cornerRadius = self.buttonGoogle.frame.size.height/2
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Private
    private func setup() {
        self.loginCommand = LoginCommand.init(email: nil, password: nil)
    }
    
    private func updateCommand() {
        self.loginCommand?.email = self.textFieldEmail.text?.trimmingCharacters(in: .whitespaces)
        self.loginCommand?.password = self.textFieldPassword.text
    }
    
    func performLogin(loginCommand: LoginCommand?) {
        
        let loginRepository = LoginRepository()
        loginRepository.loginUser(loginCommand: loginCommand!, vc: self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.responseCode == 2000 {
                    if (response.responseData?.userDetails?.id) != nil {
                       // print("email...\(response.responseData?.userDetails?.email ?? "NA")")
                       // print("id...\(response.responseData?.userDetails?.id ?? "NA")")
                      //  print("token...\(response.responseData?.authToken ?? "NA")")
                   
                      //  print("name..\((response.responseData?.userDetails?.firstName ?? "NA"))")
                        self.utility.Save(response.responseData?.userDetails?.id ?? "NA", keyname: Constants.Strings.UserID)
                        self.utility.Save(response.responseData?.userDetails?.email ?? "NA", keyname: Constants.Strings.UserEmail)
                        self.utility.Save(response.responseData?.authToken ?? "NA", keyname: Constants.Strings.Token)
                        print("image : ",response.responseData?.imageUrl ?? "NA")
                        self.utility.Save(response.responseData?.imageUrl ?? "NA", keyname: Constants.Strings.profileImage)
                         self.utility.Save((response.responseData?.userDetails?.name ?? "NA"), keyname: Constants.Strings.userFullName)
                        NotificationCenter.default.post(name: Notification.Name("UpdateSliderProfileValue"), object: nil)
                        let storyboard = UIStoryboard(name: "HomeView", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "HomeView") as! HomeView
                        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
                    }
                }else if response.responseCode == 2008 {
                    let alert = UIAlertController(title: Constants.Strings.AppName, message: response.responseMessage, preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                        //Cancel Action
                        let st = UIStoryboard.init(name: "Main", bundle: nil)
                        let controller = st.instantiateViewController(withIdentifier:"OTPView") as! OTPView
                        controller.otp = response.responseData?.userDetails?.otp ?? "0"
                        controller.fromClass = "normal"
                        self.navigationController?.pushViewController(controller, animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
  
    @IBAction func buttonForgotPasswordAction(_ sender: Any) {
        self.view.endEditing(true)
        let controller = self.storyboard!.instantiateViewController(withIdentifier:"ForgotPasswordView") as! ForgotPasswordView
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func buttonLoginAction(_ sender: Any) {
        self.view.endEditing(true)
        self.updateCommand()
        self.loginCommand?.validate(completion: { (isValid, message) in
            
            // Start activity
            //self.startLoading()
            if(isValid){
                guard let loginCommand = self.loginCommand else{
                    return
                }
                self.performLogin(loginCommand: loginCommand)
            }else{
                Utility.showAlert(withMessage: message!, onController: self)
            }
        })
//        let storyboard = UIStoryboard(name: "HomeView", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "HomeView") as! HomeView
//        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
    }
    @IBAction func buttonFBAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    @IBAction func buttonGoogleAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    @IBAction func buttonSignUpAction(_ sender: Any) {
        self.view.endEditing(true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationView") as! RegistrationView
//        vc.modalPresentationStyle = .fullScreen
//        let navController = UINavigationController(rootViewController: vc)
//        navController.navigationBar.isHidden =  true
//        self.present(navController, animated: false, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
