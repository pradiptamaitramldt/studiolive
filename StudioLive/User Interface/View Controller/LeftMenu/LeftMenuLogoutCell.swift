//
//  LeftMenuLogoutCell.swift
//  StudioLive
//
//  Created by Pradipta on 13/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class LeftMenuLogoutCell: UITableViewCell {

    @IBOutlet weak var menuTitle: UILabel!
    @IBOutlet weak var menuIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
