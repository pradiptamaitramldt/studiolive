//
//  LeftMenu.swift
//  Nexopt
//
//  Created by Pradipta on 05/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class LeftMenu: UIViewController {

    @IBOutlet weak var profileIcon: UIImageView!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet var labelProfileName: UILabel!
    @IBOutlet var labelEmailID: UILabel!
    
    var menuNameArray : [String] = []
    var menuImageArray : [String] = []
    var utility = Utility()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        menuNameArray = ["Home", "Feeds",  "Post", "Favourites", "Profile", "Edit", "My Post", "Report An Issue", "Subscribe", "Terms & Conditions", "Privacy Policy", "About", "Tickets/Venue", "Log Out"]
        menuImageArray = ["Home_icon", "Feeds_icon", "Post_icon", "Favorites_icon", "Profile_icon", "Edit_icon", "my_post", "report", "Terms-and-Conditions_icon", "Terms-and-Conditions_icon", "Privacy_Policy_icon", "About_icon", "report", "Log-Out_icon"]
        self.table_view.delegate=self
        self.table_view.dataSource=self
          let userID: String = self.utility.Retrive(Constants.Strings.UserID) as! String
                if !userID.isEmpty {
                    self.labelProfileName.text = self.utility.Retrive(Constants.Strings.userFullName) as? String
                    self.labelEmailID.text = self.utility.Retrive(Constants.Strings.UserEmail) as? String
                    print("str:",self.utility.Retrive(Constants.Strings.profileImage) as! String)
                    self.profileIcon.af_setImage(
                        withURL: URL(string:self.utility.Retrive(Constants.Strings.profileImage) as! String)!,
                        placeholderImage: UIImage(named: "userprofileDummy"),
                        filter: nil,
                        imageTransition: .crossDissolve(0.2)
                    )
                    
                }
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.updateSliderProfileValue(notification:)), name: Notification.Name("UpdateSliderProfileValue"), object: nil)
    }
    @objc func updateSliderProfileValue(notification: Notification) {
        let userID: String = self.utility.Retrive(Constants.Strings.UserID) as! String
                       if !userID.isEmpty {
                           self.labelProfileName.text = self.utility.Retrive(Constants.Strings.userFullName) as? String
                           self.labelEmailID.text = self.utility.Retrive(Constants.Strings.UserEmail) as? String
                           self.profileIcon.af_setImage(
                            withURL: URL(string: (self.utility.Retrive(Constants.Strings.profileImage) as! String) )!,
                               placeholderImage: UIImage(named: "userprofileDummy"),
                               filter: nil,
                               imageTransition: .crossDissolve(0.2)
                           )
                       }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LeftMenu : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == menuNameArray.count - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "leftMenuLogoutCell", for: indexPath) as! LeftMenuLogoutCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.menuTitle.text = menuNameArray[indexPath.row]
            cell.menuIcon.image = UIImage(named: menuImageArray[indexPath.row])
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "leftMenuTableCell", for: indexPath) as! LeftMenuTableCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.menuTitle.text = menuNameArray[indexPath.row]
            cell.menuIcon.image = UIImage(named: menuImageArray[indexPath.row])
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 9 {
            return 80
        }else{
            return 60
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == menuNameArray.count - 1 {
            let selectedCell:LeftMenuLogoutCell = tableView.cellForRow(at: indexPath as IndexPath)! as! LeftMenuLogoutCell
            selectedCell.menuTitle.textColor=UIColor(red: 7.0/255, green: 19.0/255, blue: 61.0/255, alpha: 1.0)
            var selectedIcon = menuImageArray[indexPath.row]
            selectedIcon=selectedIcon+"_b"
            selectedCell.menuIcon.image=UIImage(named: selectedIcon)
        }else{
            let selectedCell:LeftMenuTableCell = tableView.cellForRow(at: indexPath as IndexPath)! as! LeftMenuTableCell
            selectedCell.menuTitle.textColor=UIColor(red: 7.0/255, green: 19.0/255, blue: 61.0/255, alpha: 1.0)
            var selectedIcon = menuImageArray[indexPath.row]
            selectedIcon=selectedIcon+"_b"
            selectedCell.menuIcon.image=UIImage(named: selectedIcon)
        }
        
        if indexPath.row==0 {
            let storyBoard = UIStoryboard(name: "HomeView", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomeView") as! HomeView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==1 {
            let storyBoard = UIStoryboard(name: "Feed", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "FeedView") as! FeedView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==2 {
            let storyBoard = UIStoryboard(name: "Post", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PostView") as! PostView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==3 {
            let storyBoard = UIStoryboard(name: "Favourite", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "FavouriteView") as! FavouriteView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==4 {
            let storyBoard = UIStoryboard(name: "Profile", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==5 {
            let storyBoard = UIStoryboard(name: "Profile", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "EditProfileView") as! EditProfileView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==6 {
            let storyBoard = UIStoryboard(name: "Post", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyPostView") as! MyPostView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==7 {
            let storyBoard = UIStoryboard(name: "ReportAnIssue", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ReportAnIssueView") as! ReportAnIssueView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==8 {
            let storyBoard = UIStoryboard(name: "Subscribe", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "SubscribeView") as! SubscribeView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)            
        } else if indexPath.row==9 {
            let storyBoard = UIStoryboard(name: "TermsCondition", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "TermsConditionView") as! TermsConditionView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==10 {
            let storyBoard = UIStoryboard(name: "PrivacyPolicy", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PrivacyPolicyView") as! PrivacyPolicyView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==11 {
            let storyBoard = UIStoryboard(name: "About", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "AboutView") as! AboutView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==12 {
            let storyBoard = UIStoryboard(name: "NeedHelp", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "NeedHelpViewController") as! NeedHelpViewController
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else {
            let defaults = UserDefaults.standard
            let dictionary = defaults.dictionaryRepresentation()
            dictionary.keys.forEach { key in
                defaults.removeObject(forKey: key)
            }
            
            let selectedCell:LeftMenuLogoutCell = tableView.cellForRow(at: indexPath as IndexPath)! as! LeftMenuLogoutCell
            selectedCell.menuTitle.textColor=UIColor(red: 110.0/255, green: 120.0/255, blue: 153.0/255, alpha: 1.0)
            let selectedIcon = menuImageArray[indexPath.row]
            selectedCell.menuIcon.image=UIImage(named: selectedIcon)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginView") as! LoginView
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.row == menuNameArray.count - 1 {
//            let selectedCell:LeftMenuLogoutCell = tableView.cellForRow(at: indexPath as IndexPath)! as! LeftMenuLogoutCell
//            selectedCell.menuTitle.textColor=UIColor(red: 110.0/255, green: 120.0/255, blue: 153.0/255, alpha: 1.0)
//            let selectedIcon = menuImageArray[indexPath.row]
//            selectedCell.menuIcon.image=UIImage(named: selectedIcon)
        }else{
            let selectedCell:LeftMenuTableCell = tableView.cellForRow(at: indexPath as IndexPath)! as! LeftMenuTableCell
            selectedCell.menuTitle.textColor=UIColor(red: 110.0/255, green: 120.0/255, blue: 153.0/255, alpha: 1.0)
            let selectedIcon = menuImageArray[indexPath.row]
            selectedCell.menuIcon.image=UIImage(named: selectedIcon)
        }
    }
}
