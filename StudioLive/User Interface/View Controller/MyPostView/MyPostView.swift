//
//  MyPostView.swift
//  StudioLive
//
//  Created by Pradipta on 15/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class MyPostView: UIViewController {
    
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var bottom_view: UIView!
    
    var bottom_menu = BottomMenu()
    var dashboardArray: [Post] = []
    var urlPath: String = ""
    var updater: CADisplayLink! = nil
    var player:AVPlayer?
    var audioUrl : URL?
    var utility = Utility()
    var isDeletePost: Bool = false
    var lastContentOffset: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.bottom_view.bounds.size.width,height:self.bottom_view.bounds.size.height)
        self.bottom_view.addSubview(bottom_menu)
        bottom_menu.delegate = self
        bottom_menu.postSeleted()
        
        self.table_view.register(UINib(nibName: "VideoTableViewCell", bundle: .main), forCellReuseIdentifier: "VideoTableViewCell")
        self.table_view.register(UINib(nibName: "SliderTableViewCell", bundle: .main), forCellReuseIdentifier: "SliderTableViewCell")
        
        self.table_view.delegate = self
        self.table_view.dataSource = self
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.fetchDashboardData()
    }
    
    func fetchDashboardData() {
        let homeRepository = HomeRepository()
        homeRepository.fetchMusicVideo(vc:  self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    self.urlPath = response.responseData?.path ?? ""
                    let arrayAll = response.responseData?.post ?? []
                    self.dashboardArray.removeAll()
                    for item in arrayAll {
                        self.dashboardArray.append(item)
                    }
                    if self.dashboardArray.count > 0 {
                        DispatchQueue.main.async {
                            self.table_view.reloadData()
                        }
                    }
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func delete(musicID: String) {
        let homeRepository = HomeRepository()
        homeRepository.deleteAudio(musicID: musicID, vc:  self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    self.fetchDashboardData()
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func like(isLike: String, musicID: String, likeCustomerID: String) {
        let homeRepository = HomeRepository()
        homeRepository.likeUnlikePost(musicID: musicID, likeCustomerID: likeCustomerID, isLike: isLike, vc:  self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    self.fetchDashboardData()
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func favourite(isFav: String, musicID: String, favCustomerID: String) {
        let homeRepository = HomeRepository()
        homeRepository.favUnfavPost(musicID: musicID, favCustomerID: favCustomerID, isFav: isFav, vc:  self) { (response, success, error) in
            //self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if response.statuscode == 200 {
                    self.fetchDashboardData()
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    
}
extension MyPostView: bottomMenuDelegate{
    func selectedButton(selectedItem: NSInteger) {
        if selectedItem == 1 {
            //home
            let st = UIStoryboard.init(name: "HomeView", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"HomeView") as! HomeView
            self.navigationController?.pushViewController(controller, animated: true)
        }else if selectedItem == 2{
            //Feed
            let st = UIStoryboard.init(name: "Feed", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FeedView") as! FeedView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 3 {
            //Post
            let st = UIStoryboard.init(name: "Post", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"PostView") as! PostView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 4 {
            let st = UIStoryboard.init(name: "Favourite", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FavouriteView") as! FavouriteView
            self.navigationController?.pushViewController(controller, animated: true)
        } else if selectedItem == 5 {
            let st = UIStoryboard.init(name: "Profile", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"ProfileView") as! ProfileView
            self.navigationController?.pushViewController(controller, animated: true)
        } 
    }
}
extension MyPostView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dashboardArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let fileType = self.dashboardArray[indexPath.row].fileType
        if fileType == "VIDEO" {
            let videoCell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
            let url = "\(self.urlPath)\(self.dashboardArray[indexPath.row].file ?? "")"
            let videoURL = URL(string: url)!
            
            videoCell.playerController.view.frame = videoCell.newVideoContainerView.bounds
            videoCell.newVideoContainerView.addSubview(videoCell.playerController.view)
            videoCell.playerController.player = AVPlayer(url: videoURL as URL )
            videoCell.playerLayer = AVPlayerLayer(layer: videoCell.playerController.player as Any)
            videoCell.playerLayer.videoGravity = .resize
            videoCell.playerLayer.frame = videoCell.newVideoContainerView.bounds
            videoCell.playerController.player?.isMuted = false
            
            videoCell.labelUserName.text = self.utility.Retrive(Constants.Strings.userFullName) as? String
            videoCell.imageViewUserProfileImage.af_setImage(
                withURL: URL(string:self.utility.Retrive(Constants.Strings.profileImage) as! String)!,
                placeholderImage: UIImage(named: "userprofileDummy"),
                filter: nil,
                imageTransition: .crossDissolve(0.2))
            videoCell.labelPostedTime.text = self.dashboardArray[indexPath.row].updatedAt ?? ""
            videoCell.labelLikeCount.text = "\(self.dashboardArray[indexPath.row].likeNo ?? 0)"
            videoCell.labelCommentCount.text = "\(self.dashboardArray[indexPath.row].commentNo ?? 0)"
            videoCell.labelMusicGenre.text = self.dashboardArray[indexPath.row].musicCategory
            
            let isLike = self.dashboardArray[indexPath.row].isLike
            if isLike == false {
                videoCell.imageViewLike.image = UIImage(named: "Done_icon")
            }else{
                videoCell.imageViewLike.image = UIImage(named: "liked")
            }
            
            let isFav = self.dashboardArray[indexPath.row].isFavourite
            if isFav == false {
                videoCell.buttonFavourite.setImage(UIImage(named: "favourites_gray_icon"), for: .normal)
                videoCell.buttonFavourite2.setImage(UIImage(named: "favourites_gray_icon"), for: .normal)
            }else{
                videoCell.buttonFavourite.setImage(UIImage(named: "favourites_icon"), for: .normal)
                videoCell.buttonFavourite2.setImage(UIImage(named: "favourites_icon"), for: .normal)
            }
            videoCell.buttonLike.tag = indexPath.row
            videoCell.buttonFavourite.tag = indexPath.row
            videoCell.buttonFavourite2.tag = indexPath.row
            videoCell.delegate = self
            return videoCell
        }else{
            let audioCell = tableView.dequeueReusableCell(withIdentifier: "SliderTableViewCell") as! SliderTableViewCell
            audioCell.delegate = self
            audioCell.buttonLike.tag = indexPath.row
            audioCell.buttonFav.tag = indexPath.row
            audioCell.buttonFav2.tag = indexPath.row
            audioCell.audioSlider.tag = indexPath.row
            audioCell.labelCategory.text = self.dashboardArray[indexPath.row].musicCategory
            audioCell.labelLikeCount.text = "\(self.dashboardArray[indexPath.row].likeNo ?? 0)"
            audioCell.labelCommentCount.text = "\(self.dashboardArray[indexPath.row].commentNo ?? 0)"
            
            audioCell.labelHeader.text = self.utility.Retrive(Constants.Strings.userFullName) as? String
            audioCell.imageViewProfileImage.af_setImage(
                withURL: URL(string:self.utility.Retrive(Constants.Strings.profileImage) as! String)!,
                placeholderImage: UIImage(named: "userprofileDummy"),
                filter: nil,
                imageTransition: .crossDissolve(0.2))
            let isLike = self.dashboardArray[indexPath.row].isLike
            if isLike == false {
                audioCell.imageViewLike.image = UIImage(named: "Done_icon")
            }else{
                audioCell.imageViewLike.image = UIImage(named: "liked")
            }
            
            let isFav = self.dashboardArray[indexPath.row].isFavourite
            if isFav == false {
                audioCell.buttonFav.setImage(UIImage(named: "favourites_gray_icon"), for: .normal)
                audioCell.buttonFav2.setImage(UIImage(named: "favourites_gray_icon"), for: .normal)
            }else{
                audioCell.buttonFav.setImage(UIImage(named: "favourites_icon"), for: .normal)
                audioCell.buttonFav2.setImage(UIImage(named: "favourites_icon"), for: .normal)
            }
            audioCell.labelTimeAgo.text = self.dashboardArray[indexPath.row].updatedAt ?? ""
            audioCell.labelCategory.text = self.dashboardArray[indexPath.row].musicCategory
            audioCell.labelLocation.text = self.dashboardArray[indexPath.row].location
            audioCell.labelTitle.text = self.dashboardArray[indexPath.row].title
            return audioCell
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let fileType = self.dashboardArray[indexPath.row].fileType
        if fileType == "VIDEO" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
            print("willCellVideo")
            self.addChild(cell.playerController)
        }  else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SliderTableViewCell") as! SliderTableViewCell
            print("willCellAudio")
            let url = "\(self.urlPath)\(self.dashboardArray[indexPath.row].file ?? "")"
            let audioUrl = URL(string: url)!
            let completelyVisible = tableView.isCellVisible(section: indexPath.section, row: indexPath.row)
            if completelyVisible {
                downloadFileFromURL(url: (audioUrl ))
            } else {
                cell.audioPlayer.pause()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.isDeletePost == false {
            let fileType = self.dashboardArray[indexPath.row].fileType
            if fileType == "VIDEO" {
                //  print("endCellVideo")
                let videoCell: VideoTableViewCell = cell as! VideoTableViewCell
                videoCell.playerController.player?.pause()
                videoCell.playerController.player = nil
                videoCell.playerLayer.player = nil
            }else{
                let audioCell: SliderTableViewCell = cell as! SliderTableViewCell
                audioCell.audioPlayer.pause()
                NotificationCenter.default.post(name: Notification.Name("PauseAudio"), object: audioUrl)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let fileType = self.dashboardArray[indexPath.row].fileType
        if fileType == "VIDEO" {
            return tableView.bounds.size.width * 1.2
        }
        else {
            return tableView.bounds.size.width * (250 / 320)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fileType = self.dashboardArray[indexPath.row].fileType
        if fileType == "VIDEO" {
            let videoCell: VideoTableViewCell = tableView.cellForRow(at: indexPath) as! VideoTableViewCell
            let playerVC = AVPlayerViewController()
            playerVC.player = videoCell.viewVideoPlayerContainer.player
            self.present(playerVC, animated: true){
                playerVC.player?.play()
            }
        }else{
            let audioCell: SliderTableViewCell = tableView.cellForRow(at: indexPath) as! SliderTableViewCell
            let playerVC = AVPlayerViewController()
            playerVC.player = audioCell.audioPlayerView.player
            self.present(playerVC, animated: true){
                playerVC.player?.play()
            }
        }
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }

    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.lastContentOffset < scrollView.contentOffset.y {
            // did move up
            self.isDeletePost = false
        } else if self.lastContentOffset > scrollView.contentOffset.y {
            // did move down
            self.isDeletePost = false
        } else {
            // didn't move
        }
    }
}
extension MyPostView {
    
    func downloadFileFromURL(url:URL){
        print("downloadFileFromURL")
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url as URL, completionHandler: { [weak self](url, response, error) -> Void in
            self?.play(url: ((url ?? URL(string: "url"))!)  )
        })
        downloadTask.resume()
    }
    
    func play(url:URL) {
        print("playing")
        audioUrl = url
        NotificationCenter.default.post(name: Notification.Name("ReadyToPlayAudio"), object: audioUrl)
    }
}

extension UITableView {
    func isCellVisible(section:Int, row: Int) -> Bool {
        guard let indexes = self.indexPathsForVisibleRows else {
            return false
        }
        return indexes.contains {$0.section == section && $0.row == row }
    }
    
}
extension MyPostView: AudioCellDelegate {
    func deleteAudioAction(_ sender: UIButton) {
        let musicID = self.dashboardArray[sender.tag].musicID ?? ""
        let optionMenu = UIAlertController(title: "", message: "Do you want to delete this post?", preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: "Delete", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("deleted")
            self.isDeletePost = true
            self.delete(musicID: musicID)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
            
        })
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    func likeAudioAction(_ sender: UIButton) {
        let fileType = self.dashboardArray[sender.tag].fileType
        if fileType == "AUDIO" {
            let isLike = self.dashboardArray[sender.tag].isLike ?? false
            let musicID = self.dashboardArray[sender.tag].musicID ?? ""
            let likeCustomerID = self.dashboardArray[sender.tag].customerID ?? ""
            if isLike == true {
                self.like(isLike: "NO", musicID: musicID, likeCustomerID: likeCustomerID)
            }else{
                self.like(isLike: "YES", musicID: musicID, likeCustomerID: likeCustomerID)
            }
        }
    }
    
    func commentAudioAction(_ sender: UIButton) {
        
    }
    
    func favAudioAction(_ sender: UIButton) {
        let fileType = self.dashboardArray[sender.tag].fileType
        if fileType == "AUDIO" {
            let isFav = self.dashboardArray[sender.tag].isFavourite ?? false
            let musicID = self.dashboardArray[sender.tag].musicID ?? ""
            let favCustomerID = self.dashboardArray[sender.tag].customerID ?? ""
            if isFav == true {
                self.favourite(isFav: "NO", musicID: musicID, favCustomerID: favCustomerID)
            }else{
                self.favourite(isFav: "YES", musicID: musicID, favCustomerID: favCustomerID)
            }
        }
    }
    
    func playPause(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.table_view)
        let indexPath = self.table_view.indexPathForRow(at:buttonPosition)
        let cell: SliderTableViewCell = self.table_view.cellForRow(at: indexPath!) as! SliderTableViewCell
        if player?.rate == 0 {
            cell.buttonPlayPause.setImage(UIImage(named: ""), for: .normal)
            player!.play()
        } else {
            cell.buttonPlayPause.setImage(UIImage(named: ""), for: .normal)
            player!.pause()
        }
    }
}
extension MyPostView: VideoCellDelegate {
    func deleteAction(_ sender: UIButton) {
        let musicID = self.dashboardArray[sender.tag].musicID ?? ""
        let optionMenu = UIAlertController(title: "", message: "Do you want to delete this post?", preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: "Delete", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("deleted")
            self.isDeletePost = true
            self.delete(musicID: musicID)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
            
        })
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    func likeVideoAction(_ sender: UIButton) {
        let fileType = self.dashboardArray[sender.tag].fileType
        if fileType == "VIDEO" {
            let isFav = self.dashboardArray[sender.tag].isLike ?? false
            let musicID = self.dashboardArray[sender.tag].musicID ?? ""
            let likeCustomerID = self.dashboardArray[sender.tag].customerID ?? ""
            if isFav == true {
                self.like(isLike: "NO", musicID: musicID, likeCustomerID: likeCustomerID)
            }else{
                self.like(isLike: "YES", musicID: musicID, likeCustomerID: likeCustomerID)
            }
        }
    }
    
    func commentVideoAction(_ sender: UIButton) {
        
    }
    
    func favVideoAction(_ sender: UIButton) {
        let fileType = self.dashboardArray[sender.tag].fileType
        if fileType == "VIDEO" {
            let isFav = self.dashboardArray[sender.tag].isFavourite ?? false
            let musicID = self.dashboardArray[sender.tag].musicID ?? ""
            let favCustomerID = self.dashboardArray[sender.tag].customerID ?? ""
            if isFav == true {
                self.favourite(isFav: "NO", musicID: musicID, favCustomerID: favCustomerID)
            }else{
                self.favourite(isFav: "YES", musicID: musicID, favCustomerID: favCustomerID)
            }
        }
    }
}
