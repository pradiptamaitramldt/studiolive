//
//  FriendRequestCell.swift
//  StudioLive
//
//  Created by BrainiumSSD on 04/11/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class FriendRequestCell: UITableViewCell {

    @IBOutlet var bgView: UIView!
    @IBOutlet var acceptButton: UIButton!
    @IBOutlet var rejectButton: UIButton!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelEmailID: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bgView.setCornerMolded(radiousVlaue : 10.0)
        bgView.setShadow(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
