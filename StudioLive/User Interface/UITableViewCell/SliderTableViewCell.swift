//
//  SliderTableViewCell.swift
//  StudioLive
//
//  Created by Pallab on 16/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

protocol AudioCellDelegate{
    func playPause(_ sender : UIButton)
    func likeAudioAction(_ sender: UIButton)
    func commentAudioAction(_ sender: UIButton)
    func favAudioAction(_ sender: UIButton)
    func deleteAudioAction(_ sender: UIButton)
}

class SliderTableViewCell: UITableViewCell {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imageViewProfileImage: UIImageView!
    @IBOutlet weak var imageViewOthersProfileImage: UIImageView!
    @IBOutlet weak var controlLike: UIControl!
    @IBOutlet weak var controlComment: UIControl!
    @IBOutlet weak var audioSlider: UISlider!
    @IBOutlet weak var labelStartTime: UILabel!
    @IBOutlet weak var labelendTime: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var buttonPlayPause: UIButton!
    @IBOutlet weak var audioPlayerView: PlayerView!
    @IBOutlet weak var labelTimeAgo: UILabel!
    @IBOutlet weak var labelCategory: UILabel!
    @IBOutlet weak var buttonLike: UIButton!
    @IBOutlet weak var buttonComment: UIButton!
    @IBOutlet weak var buttonFav: UIButton!
    @IBOutlet weak var buttonFav2: UIButton!
    @IBOutlet weak var labelLikeCount: UILabel!
    @IBOutlet weak var labelCommentCount: UILabel!
    @IBOutlet weak var imageViewLike: UIImageView!
    @IBOutlet var labelLocation: UILabel!
    @IBOutlet weak var viewDeleteBG: UIView!
    
    var delegate : AudioCellDelegate?
    var gameTimer: Timer?
    var audioPlayer = AVAudioPlayer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.readyToPlayAudio(notification:)), name: Notification.Name("ReadyToPlayAudio"), object: nil)
               NotificationCenter.default.addObserver(self, selector: #selector(self.needToUpdateSlider(notification:)), name: Notification.Name("NeedTOUpdateSliderValue"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.pauseAudio(notification:)), name: Notification.Name("PauseAudio"), object: nil)
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        self.imageViewProfileImage.layer.cornerRadius = self.imageViewProfileImage.bounds.height/2
        self.imageViewOthersProfileImage.layer.cornerRadius = self.imageViewOthersProfileImage.bounds.height/2
        self.controlLike.layer.cornerRadius = self.controlLike.bounds.height/2
        self.controlComment.layer.cornerRadius = self.controlComment.bounds.height/2
        self.viewContainer.dropShadowOnView()
    }
    @objc func needToUpdateSlider(notification: Notification) {
        // sliderDelegate?.needToUpdateSliderValue(self)
        
    }
    @objc func readyToPlayAudio(notification: Notification) {
           // sliderDelegate?.updateCellUI(self)
           let notification = (notification.object as! URL)
           do {
            
            NotificationCenter.default.post(name: Notification.Name("PauseVideo"), object: nil)
                self.audioPlayer.currentTime = 0.0
                self.audioPlayer = try AVAudioPlayer(contentsOf: notification )
                self.audioPlayer.prepareToPlay()
                self.audioPlayer.volume = 1.0
               
               // cell.audioPlayer.play()
               DispatchQueue.main.async {
                   //self.audioPlayer.pause()
                   self.audioSlider.value = 0
                   self.audioSlider.isContinuous = false
                   self.audioPlayer.play()
                   self.audioSlider.maximumValue = Float( self.audioPlayer.duration )
                   
                 //  print("duration",self.audioPlayer.duration)
                   let getTime = self.secondsToHoursMinutesSeconds(seconds: Int(Float( self.audioPlayer.duration )))
                  
                   self.labelendTime.text = String(format: "%02d",getTime.min) + " : " +  String(format: "%02d",getTime.sec)
                   self.gameTimer?.invalidate()
                   self.gameTimer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
               }
               
           } catch let error as NSError {
               //self.player = nil
               print(error.localizedDescription)
           } catch {
               print("AVAudioPlayer init failed")
           }
       }
     @objc func pauseAudio(notification: Notification) {
        self.audioPlayer.pause()
    }
       func secondsToHoursMinutesSeconds (seconds : Int) -> (Hour : Int, min : Int, sec : Int) {
           return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
       }
       @objc func runTimedCode() {
           audioSlider.value = Float( audioPlayer.currentTime )
           if ( audioPlayer.currentTime ) == 0.0 {
               labelStartTime.text = "00.00"
               gameTimer?.invalidate()
           }
           else {
               let getTime = self.secondsToHoursMinutesSeconds(seconds: Int(Float( self.audioSlider.value )))
               //String(format: "%.2f",Float( self.audioPlayer.duration ))
               self.labelStartTime.text = String(format: "%02d",getTime.min) + " : " +  String(format: "%02d",getTime.sec)
               
           }
       }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func buttonPlayPauseAction(_ sender: Any) {
        self.delegate?.playPause(sender as! UIButton)
    }
    
    @IBAction func sliderAction(_ sender: Any) {
        
        self.audioPlayer.stop()
        print("TimeInterval(cell.audioSlider.value)",TimeInterval(self.audioSlider.value))
        self.audioPlayer.currentTime = TimeInterval(self.audioSlider.value)
        self.audioPlayer.prepareToPlay()
        self.audioPlayer.play()
        gameTimer?.invalidate()
        self.gameTimer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
    }
    @IBAction func buttonAudioFavAction(_ sender: Any) {
        self.delegate?.favAudioAction(sender as! UIButton)
    }
    @IBAction func buttonAudioLikeAction(_ sender: Any) {
        self.delegate?.likeAudioAction(sender as! UIButton)
    }
    @IBAction func buttonAudioCommentAction(_ sender: Any) {
        self.delegate?.commentAudioAction(sender as! UIButton)
    }
    @IBAction func buttonDeleteAction(_ sender: Any) {
        self.delegate?.deleteAudioAction(sender as! UIButton)
    }
}
extension UIView {
    func dropShadowOnView(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOpacity = 0.25
        layer.shadowOffset = CGSize(width: 0, height: 3)
        layer.shadowRadius = 5
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
