//
//  VideoTableViewCell.swift
//  StudioLive
//
//  Created by Pallab on 16/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

protocol VideoCellDelegate{
    func likeVideoAction(_ sender: UIButton)
    func commentVideoAction(_ sender: UIButton)
    func favVideoAction(_ sender: UIButton)
    func deleteAction(_ sender: UIButton)
}

class VideoTableViewCell: UITableViewCell {

    //MARK: - IBOutlet
    @IBOutlet weak var viewContainer: UIView!
    //Top Part
    @IBOutlet weak var imageViewUserProfileImage: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelPostedTime: UILabel!
    @IBOutlet weak var labelMusicGenre: UILabel!
    @IBOutlet weak var buttonFavourite: UIButton!
    @IBOutlet weak var buttonFavourite2: UIButton!
    @IBOutlet weak var viewDeleteBG: UIView!
    
    //Middle Part
    @IBOutlet weak var viewVideoPlayerContainer: PlayerView!
    @IBOutlet weak var buttonVideoPlay: UIButton!
    
    //Bottom Part
    @IBOutlet weak var controlLike: UIControl!
    @IBOutlet weak var labelLikeCount: UILabel!
    @IBOutlet weak var imageViewLike: UIImageView!
    
    @IBOutlet weak var controlComment: UIControl!
    @IBOutlet weak var labelCommentCount: UILabel!
    @IBOutlet weak var imgeViewComment: UIImageView!
    @IBOutlet weak var shotImageView: UIImageView!
    @IBOutlet var newVideoContainerView: UIView!
    @IBOutlet weak var buttonLike: UIButton!
    @IBOutlet weak var buttonComment: UIButton!
    
    var playerLayer = AVPlayerLayer()
    let playerController = AVPlayerViewController()
    var delegate : VideoCellDelegate?
    
//    var playerController: ASVideoPlayerController?
//    var videoLayer: AVPlayerLayer = AVPlayerLayer()
//    var videoURL: String? {
//        didSet {
//            if let videoURL = videoURL {
//                ASVideoPlayerController.sharedVideoPlayer.setupVideoFor(url: videoURL)
//            }
//            videoLayer.isHidden = videoURL == nil
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        self.shotImageView.layer.cornerRadius = 5
//        self.shotImageView.backgroundColor = UIColor.gray.withAlphaComponent(0.7)
//        self.shotImageView.clipsToBounds = true
//        self.shotImageView.layer.borderColor = UIColor.gray.withAlphaComponent(0.3).cgColor
//        self.shotImageView.layer.borderWidth = 0.5
//        self.videoLayer.backgroundColor = UIColor.clear.cgColor
//        self.videoLayer.videoGravity = AVLayerVideoGravity.resize
//        self.shotImageView.layer.addSublayer(videoLayer)
         NotificationCenter.default.addObserver(self, selector: #selector(self.pauseVideo(notification:)), name: Notification.Name("PauseVideo"), object: nil)
        selectionStyle = .none
    }
    @objc func pauseVideo(notification: Notification) {
        DispatchQueue.main.async {
           // self.playerController.player = nil
                     //  self.playerLayer.player = nil
            self.playerController.player?.pause()
        }
          
       }
    
//    func configureCell(imageUrl: String?, description: String, videoUrl: String?) {
//        self.labelUserName.text = description
//        self.shotImageView.imageURL = imageUrl
//        self.videoURL = videoUrl
//    }
//
//    override func prepareForReuse() {
//        self.shotImageView.imageURL = nil
//        super.prepareForReuse()
//    }
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        let horizontalMargin: CGFloat = 20
//        let width: CGFloat = bounds.size.width - horizontalMargin * 2
//        let height: CGFloat = (width * 0.9).rounded(.up)
//        videoLayer.frame = CGRect(x: 0, y: 0, width: width, height: height)
//    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        self.imageViewUserProfileImage.layer.cornerRadius = self.imageViewUserProfileImage.bounds.height/2
        self.controlLike.layer.cornerRadius = self.controlLike.bounds.height/2
        self.controlComment.layer.cornerRadius = self.controlComment.bounds.height/2
        self.viewContainer.dropShadowOnView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
//    func visibleVideoHeight() -> CGFloat {
//        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(shotImageView.frame, from: shotImageView)
//        guard let videoFrame = videoFrameInParentSuperView,
//            let superViewFrame = superview?.frame else {
//             return 0
//        }
//        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
//        return visibleVideoFrame.size.height
//    }
    
    @IBAction func buttonVideoFavAction(_ sender: Any) {
        self.delegate?.favVideoAction(sender as! UIButton)
    }
    @IBAction func buttonVideoLikeAction(_ sender: Any) {
        self.delegate?.likeVideoAction(sender as! UIButton)
    }
    @IBAction func buttonVideoCommentAction(_ sender: Any) {
        self.delegate?.commentVideoAction(sender as! UIButton)
    }
    @IBAction func buttonDeleteAction(_ sender: Any) {
        self.delegate?.deleteAction(sender as! UIButton)
    }
}
