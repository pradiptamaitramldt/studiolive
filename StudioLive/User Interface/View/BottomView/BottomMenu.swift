//
//  BottomMenu.swift
//  Yacht Now 
//
//  Copyright © 2019 Yacht Now. All rights reserved.
//

import UIKit

protocol bottomMenuDelegate: class {
    func selectedButton(selectedItem:NSInteger)
}

class BottomMenu: UIView {
    
    @IBOutlet var button_home: UIButton!
    @IBOutlet var button_feed: UIButton!
    @IBOutlet var button_post: UIButton!
    @IBOutlet var button_favourite: UIButton!
    @IBOutlet var button_profile: UIButton!
    
    @IBOutlet weak var home_icon: UIImageView!
    @IBOutlet weak var feed_icon: UIImageView!
    @IBOutlet weak var post_icon: UIImageView!
    @IBOutlet weak var favourite_icon: UIImageView!
    @IBOutlet weak var profile_icon: UIImageView!
    
    var delegate :bottomMenuDelegate?
    
    
    
    class func instanceFromNib() -> BottomMenu {
        return UINib(nibName: "BottomMenu", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BottomMenu
    }
    
    //Home selected
    @IBAction func home_buttonClick(_ sender: UIButton) {
        self.delegate?.selectedButton(selectedItem: 1)
    }
    //Feed selected
    @IBAction func feed_buttonClick(_ sender: UIButton) {
        
        self.delegate?.selectedButton(selectedItem: 2)
    }
    
    //Post selected
    @IBAction func post_buttonClick(_ sender: UIButton) {
        
        self.delegate?.selectedButton(selectedItem: 3)
    }
    
    //Favourite selected
    @IBAction func favourite_buttonClick(_ sender: UIButton) {
       
        self.delegate?.selectedButton(selectedItem: 4)
    }
    
    //Profile selected
    @IBAction func profile_buttonClick(_ sender: UIButton) {
        
        self.delegate?.selectedButton(selectedItem: 5)
    }
    
    func selectedItem(index: NSInteger) {
        if index == 1 {
        
           self.homeSelected()

        }else if index == 2 {
            
           self.feedSeleted()
            
        }else if index == 3 {
            
           self.postSeleted()
            
        }else if index == 4 {
            
            self.favouriteSeleted()
            
        }else{
            
           self.profileSeleted()
        }
    }
    
    func homeSelected() {
        self.home_icon.image = UIImage(named: "Home_icon_b")
        self.feed_icon.image = UIImage(named: "Feeds_icon")
        self.post_icon.image = UIImage(named: "Post_icon")
        self.favourite_icon.image = UIImage(named: "Favorites_icon")
        self.profile_icon.image = UIImage(named: "Profile_icon")
    }
    func feedSeleted() {
        self.home_icon.image = UIImage(named: "Home_icon")
        self.feed_icon.image = UIImage(named: "Feeds_icon_b")
        self.post_icon.image = UIImage(named: "Post_icon")
        self.favourite_icon.image = UIImage(named: "Favorites_icon")
        self.profile_icon.image = UIImage(named: "Profile_icon")
    }
    func postSeleted() {
        self.home_icon.image = UIImage(named: "Home_icon")
        self.feed_icon.image = UIImage(named: "Feeds_icon")
        self.post_icon.image = UIImage(named: "Post_icon_b")
        self.favourite_icon.image = UIImage(named: "Favorites_icon")
        self.profile_icon.image = UIImage(named: "Profile_icon")
    }
    func favouriteSeleted() {
        self.home_icon.image = UIImage(named: "Home_icon")
        self.feed_icon.image = UIImage(named: "Feeds_icon")
        self.post_icon.image = UIImage(named: "Post_icon")
        self.favourite_icon.image = UIImage(named: "Favorites_icon_b")
        self.profile_icon.image = UIImage(named: "Profile_icon")
    }
    func profileSeleted() {
        self.home_icon.image = UIImage(named: "Home_icon")
        self.feed_icon.image = UIImage(named: "Feeds_icon")
        self.post_icon.image = UIImage(named: "Post_icon")
        self.favourite_icon.image = UIImage(named: "Favorites_icon")
        self.profile_icon.image = UIImage(named: "Profile_icon_b")
    }
}


extension UIColor{
    
}
