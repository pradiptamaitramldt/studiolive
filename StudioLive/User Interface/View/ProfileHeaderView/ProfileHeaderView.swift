//
//  ProfileHeaderView.swift
//  StudioLive
//
//  Created by Pallab on 16/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit
import AlamofireImage

protocol FollowerDelegate {
    func goToFollowerPage(type : String)
    func goToEditProfilePage()
}

class ProfileHeaderView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imageViewProfilePictureBorder: UIImageView!
    @IBOutlet weak var imageViewProfilePicture: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelUserCountry: UILabel!
    @IBOutlet weak var labelInterestedIn: UILabel!
    @IBOutlet weak var labelFollowersCount: UILabel!
    @IBOutlet weak var labelFollowStatus: UILabel!
    @IBOutlet weak var controlFollowStatus: UIControl!
    @IBOutlet weak var controlFollowersCount: UIControl!
    @IBOutlet weak var controlStackView: UIStackView!
    
    private var contentView: UIView!
    var delegate : FollowerDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadNib()
    }
    
    func loadNib() {
        self.contentView = Bundle.main.loadNibNamed("ProfileHeader_View", owner: self, options: nil)?[0] as? UIView
        self.contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.contentView.frame = bounds
        self.addSubview(self.contentView)
    }
    
    public func setUpUI() {
        self.imageViewProfilePicture.layer.cornerRadius = self.imageViewProfilePicture.bounds.size.height / 2
        self.imageViewProfilePicture.layer.masksToBounds = true
        self.controlFollowStatus.layer.cornerRadius = self.controlFollowStatus.bounds.height / 2
        self.controlFollowStatus.layer.masksToBounds = true
    }
    
    func setValues(name: String, email: String, profileImage: String,connectedUser : String) {
        self.labelUserName.text=name
        self.labelUserCountry.text=email
        self.labelFollowersCount.text = connectedUser + " Followers"
        if profileImage != "" {
            self.imageViewProfilePicture.af_setImage(
                withURL: URL(string: profileImage)!,
                placeholderImage: UIImage(named: "userprofileDummy"),
                filter: nil,
                imageTransition: .crossDissolve(0.2)
            )
        }
    }

    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        self.imageViewProfilePicture.layer.cornerRadius = self.imageViewProfilePicture.bounds.size.height / 2
        self.imageViewProfilePicture.layer.masksToBounds = true
        self.controlFollowStatus.layer.cornerRadius = self.controlFollowStatus.bounds.height / 2
        self.controlFollowStatus.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    @IBAction func editButtonAction(_ sender: Any) {
        delegate?.goToEditProfilePage()
    }
    
    @IBAction func buttonFollowAction(_ sender: Any) {
        self.controlFollowStatus.backgroundColor = UIColor(red: 7/255, green: 19/255, blue: 61/255, alpha: 1.0)
        self.labelFollowStatus.textColor = .white
        self.labelFollowersCount.textColor = .black
        self.controlFollowersCount.backgroundColor = .white
        
        self.delegate?.goToFollowerPage(type: "Connect with friends")
        SingleToneClass.shared.didFollowersButtonClick = false
    }
    @IBAction func buttonFollowerAction(_ sender: Any) {
        self.controlFollowersCount.backgroundColor = UIColor(red: 7/255, green: 19/255, blue: 61/255, alpha: 1.0)
        self.labelFollowersCount.textColor = .white
        self.controlFollowStatus.backgroundColor = .white
        self.labelFollowStatus.textColor = .black
        self.delegate?.goToFollowerPage(type: "My Followers")
        SingleToneClass.shared.didFollowersButtonClick = true
    }
}
