//
//  SubscriptionTypeCollectionViewCell.swift
//  StudioLive
//
//  Created by Pallab on 13/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class SubscriptionTypeCollectionViewCell: UICollectionViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var labelSubscriptionAmount: UILabel!
    @IBOutlet weak var labelSubscriptionDescription: UILabel!
    @IBOutlet weak var labelSubscriptionPlanType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
