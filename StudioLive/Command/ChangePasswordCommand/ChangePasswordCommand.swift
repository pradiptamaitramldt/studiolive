//
//  ChangePasswordCommand.swift
//  StudioLive
//
//  Created by Pradipta on 16/03/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ChangePasswordCommand: BaseCommand {
    var oldPassword : String?
    var newPassword : String?
    var confirmPassword : String?
    
    init(oldPassword: String?, newPassword: String?, confirmPassword: String?) {
        self.oldPassword = oldPassword
        self.newPassword = newPassword
        self.confirmPassword = confirmPassword
    }
    
    override func validate(completion: @escaping(Bool, String?) -> Void) {
        
        if(self.oldPassword == nil || self.oldPassword?.count == 0){
            completion(false, "Please enter password")
        }else if(self.newPassword == nil || self.newPassword?.count == 0){
            completion(false, "Please enter new password")
        }else if(self.confirmPassword == nil || self.confirmPassword?.count == 0){
            completion(false, "Please enter confirm password")
        }else if(self.newPassword != self.confirmPassword){
            completion(false, "Password and confirm password doesn't matched")
        }else{
            completion(true, "")
        }
    }
}
