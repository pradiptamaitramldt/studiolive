//
//  AccountVerifyCommand.swift
//  StudioLive
//
//  Created by Pradipta on 20/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class AccountVerifyCommand: BaseCommand {
    var otp : String?
    
    init(otp: String?) {
        self.otp = otp
    }
    
    override func validate(completion: @escaping(Bool, String?) -> Void) {
        
        if(self.otp == nil || self.otp?.count == 0){
            completion(false, "Please enter OTP")
        }else if((self.otp?.count)! < 4){
            completion(false, "Invalid OTP")
        }else{
            completion(true, "")
        }
    }
}
