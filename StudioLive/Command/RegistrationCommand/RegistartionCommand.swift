//
//  RegistartionCommand.swift
//  StudioLive
//
//  Created by Pradipta on 20/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class RegistartionCommand: BaseCommand {
    var name : String?
    var email : String?
    var password : String?
    var dob : String?
    var confirmPassword : String?
    
    init(name: String?, email: String?, password: String?, dob: String?, confirmPassword: String?) {
        self.name = name
        self.email = email
        self.password = password
        self.dob = dob
        self.confirmPassword = confirmPassword
    }
    
    override func validate(completion: @escaping(Bool, String?) -> Void) {
        if self.name == nil || self.name?.count == 0 {
            completion(false, "Please enter name")
        }else if(self.email == nil || self.email?.count == 0){
            completion(false, "Please enter email")
        }else if(self.isValidEmail(emailStr: self.email)==false){
            completion(false, "Please enter valid email")
        }else if(self.dob == nil || self.dob?.count == 0){
            completion(false, "Please enter your date of birth")
        }else if((self.dob?.count)! < 10 || (self.dob?.count)! > 10){
            completion(false, "Please enter your date of birth")
        }else if(self.password == nil || self.password?.count == 0){
            completion(false, "Please enter password")
        }else if(self.confirmPassword == nil || self.confirmPassword?.count == 0){
            completion(false, "Please enter confirm password")
        }else if(self.confirmPassword != self.password){
            completion(false, "Password and confirm password does not matched")
        }else{
            completion(true, "")
        }
    }
    
    func isValidEmail(emailStr:String?) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
}
