//
//  PostCommand.swift
//  StudioLive
//
//  Created by Pradipta Maitra on 26/09/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class PostCommand: BaseCommand {
    var title : String?
    var categoryID : String?
    var privacyType : String?
    var location: String?
    var fileType: String?
    
    init(title: String?, categoryID: String?, privacyType: String?, location: String?, fileType: String?) {
        self.title = title
        self.categoryID = categoryID
        self.privacyType = privacyType
        self.location = location
        self.fileType = fileType
    }
    
    override func validate(completion: @escaping(Bool, String?) -> Void) {
        
        if(self.title == nil || self.title?.count == 0){
            completion(false, "Please enter title")
        }else if(self.categoryID == nil || self.categoryID?.count == 0){
            completion(false, "Please enter new category")
        }else if(self.privacyType == nil || self.privacyType?.count == 0){
            completion(false, "Please select privacy")
        }else if(self.location == nil || self.location?.count == 0){
            completion(false, "Please enter new location")
        }else{
            completion(true, "")
        }
    }
}
