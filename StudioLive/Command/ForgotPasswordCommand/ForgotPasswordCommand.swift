//
//  ForgotPasswordCommand.swift
//  StudioLive
//
//  Created by Pradipta on 20/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ForgotPasswordCommand: BaseCommand {
    var email : String?
    
    init(email: String?) {
        self.email = email
    }
    
    override func validate(completion: @escaping(Bool, String?) -> Void) {
        
        if(self.email == nil || self.email?.count == 0){
            completion(false, "Please enter email")
        }else if(self.isValidEmail(emailStr: self.email)==false){
            completion(false, "Please enter valid email")
        }else{
            completion(true, "")
        }
    }
    
    func isValidEmail(emailStr:String?) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
}
