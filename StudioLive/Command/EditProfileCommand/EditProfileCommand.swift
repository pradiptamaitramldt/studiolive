//
//  EditProfileCommand.swift
//  StudioLive
//
//  Created by Pradipta on 13/03/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class EditProfileCommand: BaseCommand {
    var firstName : String?
    var lastName : String?
    var email : String?
    var phone : String?
    
    init(firstName: String?, lastName: String?, email: String?, phone: String?) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phone = phone
    }
    
    override func validate(completion: @escaping(Bool, String?) -> Void) {
        
        if(self.firstName == nil || self.firstName?.count == 0){
            completion(false, "Please enter first name")
        }else if(self.lastName == nil || self.lastName?.count == 0){
            completion(false, "Please enter last name")
        }else if(self.email == nil || self.email?.count == 0){
            completion(false, "Please enter email")
        }else if(self.isValidEmail(emailStr: self.email)==false){
            completion(false, "Please enter valid email")
        }else if(self.phone == nil || self.phone?.count == 0){
            completion(false, "Please enter phone")
        }else if(self.phone!.count < 10){
            completion(false, "Please enter valid phone")
        }else{
            completion(true, "")
        }
    }
    
    func isValidPhoneNumber(phoneStr:String?) -> Bool {
        let phoneRegEx = "^[0-9]{3}-[0-9]{3}-[0-9]{4}$"
        let phonePred = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phonePred.evaluate(with: phoneStr)
    }
    
    func isValidEmail(emailStr:String?) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
}
