//
//  CreatePasswordCommand.swift
//  StudioLive
//
//  Created by Pradipta on 21/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class CreatePasswordCommand: BaseCommand {
    var password : String?
    var confirmPassword : String?
    
    init(password: String?, confirmPassword: String?) {
        self.password = password
        self.confirmPassword = confirmPassword
    }
    
    override func validate(completion: @escaping(Bool, String?) -> Void) {
        
        if(self.password == nil || self.password?.count == 0){
            completion(false, "Please enter password")
        }else if(self.confirmPassword == nil || self.confirmPassword?.count == 0){
            completion(false, "Please enter confirm password")
        }else if(self.confirmPassword != self.password){
            completion(false, "Password and confirm password does not matched")
        }else{
            completion(true, "")
        }
    }
    
    func isValidEmail(emailStr:String?) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
}
