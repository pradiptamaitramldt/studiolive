//
//  ChangeEmailCommand.swift
//  StudioLive
//
//  Created by Pradipta on 16/03/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ChangeEmailCommand: BaseCommand {
    var email : String?
    var confirmEmail : String?
    
    init(email: String?, confirmEmail: String?) {
        self.email = email
        self.confirmEmail = confirmEmail
    }
    
    override func validate(completion: @escaping(Bool, String?) -> Void) {
        
        if(self.email == nil || self.email?.count == 0){
            completion(false, "Please enter email")
        }else if(self.isValidEmail(emailStr: self.email)==false){
            completion(false, "Please enter valid email")
        }else if(self.confirmEmail == nil || self.confirmEmail?.count == 0){
            completion(false, "Please enter confirm email")
        }else if(self.isValidEmail(emailStr: self.confirmEmail)==false){
            completion(false, "Please enter valid confirm email")
        }else if(self.email != self.confirmEmail){
            completion(false, "Email and confirm email doesn't matched")
        }else{
            completion(true, "")
        }
    }
    
    func isValidEmail(emailStr:String?) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
}
