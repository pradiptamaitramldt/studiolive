//  ZPLStringConverter.m
//  Nexopt

#import "ZPLStringConverter.h"
#import <UIKit/UIKit.h>
#import "StudioLive-Bridging-Header.h"
#import <StudioLive-Swift.h>

@interface ZPLStringConverter()
@end

@implementation ZPLStringConverter {
    double blackLimit;
    NSInteger total;
    NSInteger widthBytes;
    NSDictionary* mapCode;
    BOOL compressHex;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        blackLimit = 380;
        
        mapCode = @{@1: @"G",
                    @2: @"H",
                    @3: @"I",
                    @4: @"J",
                    @5: @"K",
                    @6: @"L",
                    @7: @"M",
                    @8: @"N",
                    @9: @"O",
                    @10: @"P",
                    @11: @"Q",
                    @12: @"R",
                    @13: @"S",
                    @14: @"T",
                    @15: @"U",
                    @16: @"V",
                    @17: @"W",
                    @18: @"X",
                    @19: @"Y",
                    @20: @"g",
                    @40: @"h",
                    @60: @"i",
                    @80: @"j",
                    @100: @"k",
                    @120: @"l",
                    @140: @"m",
                    @160: @"n",
                    @180: @"o",
                    @200: @"p",
                    @220: @"q",
                    @240: @"r",
                    @260: @"s",
                    @280: @"t",
                    @300: @"u",
                    @320: @"v",
                    @340: @"w",
                    @360: @"x",
                    @380: @"y",
                    @400: @"z"
                    };
    }
    return self;
}

-(NSString*)getZPLDataFrom:(UIImage*)bitmap with:(BOOL)addHeaderFooter {
    
    if (bitmap == nil) {
        return nil;
    }
    
//    ZPLStringConverter *zplStringConverter = [ZPLStringConverter init];
    [self setCompressHex: YES];
    [self setBlacknessLimitPercentage: 50];
    // TODO: get grayscale image
    return [self convertFromImage:bitmap addHeaderFooter: addHeaderFooter];
}

-(void)setCompressHex:(BOOL)cmprsHex {
    compressHex = cmprsHex; // this.compressHex
}

-(void)setBlacknessLimitPercentage:(NSInteger)percentage {
    blackLimit = (percentage * 768 / 100);
}

- (UIImage *) getResizedImageFrom:(UIImage *) sourceImage ofSize:(CGSize) newSize
{
    if(!sourceImage)
        return nil;
    
    float hfactor = sourceImage.size.width / newSize.width;
    float vfactor = sourceImage.size.height / newSize.height;
    
    float factor = fmax(hfactor, vfactor);
    
    // Divide the size by the greater of the vertical or horizontal shrinkage factor
    float newWidth = ceilf(sourceImage.size.width / factor);
    float newHeight = ceilf(sourceImage.size.height / factor);
    
    newSize = CGSizeMake(newWidth, newHeight);
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(newSize.width, newSize.height), NO, 0.0);
    
    [sourceImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(NSString*)convertFromImage:(UIImage*) bitmap addHeaderFooter:(BOOL)addHF {
    
    bitmap = [self getResizedImageFrom:bitmap ofSize:CGSizeMake(bitmap.size.width / [UIScreen mainScreen].scale, bitmap.size.height / [UIScreen mainScreen].scale)];
    
    NSString* hexAscii = [self createBody:bitmap];
    
    if (compressHex) {
        hexAscii = [self encodeHexAscii:hexAscii];
    }
    
    NSString* zplCode = [[NSString alloc] init];
    zplCode = [zplCode stringByAppendingString:@"^GFA,"];
    zplCode = [zplCode stringByAppendingString:[NSString stringWithFormat:@"%ld",(long)total]];
    zplCode = [zplCode stringByAppendingString:@","];
    zplCode = [zplCode stringByAppendingString:[NSString stringWithFormat:@"%ld",(long)total]];
    zplCode = [zplCode stringByAppendingString:@","];
    zplCode = [zplCode stringByAppendingString:[NSString stringWithFormat:@"%ld",(long)widthBytes]];
    zplCode = [zplCode stringByAppendingString:@", "];
    zplCode = [zplCode stringByAppendingString:hexAscii];
    
    if (addHF) {
        NSString* header = [NSString stringWithFormat:@"^XA "];
        header = [header stringByAppendingString:@"^FO20,20"];
        NSString* footer = [NSString stringWithFormat:@"^FS"];
        footer = [footer stringByAppendingString:@"^XZ"];
        
        zplCode = [NSString stringWithFormat:@"%@%@%@",header,zplCode,footer];
    }
    
    return zplCode;
}

-(NSString*)createBody:(UIImage*)currentImage {
    
    // Creating Bitmap Image
    if (currentImage) {
        CGImageRef originalImage = [currentImage CGImage];
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGContextRef bitmapContext = CGBitmapContextCreate(NULL, CGImageGetWidth(originalImage), CGImageGetHeight(originalImage), 8, CGImageGetWidth(originalImage)*4, colorSpace, kCGImageAlphaPremultipliedLast);
        CGColorSpaceRelease(colorSpace);
        CGContextDrawImage(bitmapContext, CGRectMake(0, 0, CGBitmapContextGetWidth(bitmapContext), CGBitmapContextGetHeight(bitmapContext)), originalImage);

        UInt8 *data = CGBitmapContextGetData(bitmapContext);

        int numComponents = 4;

        double redIn, greenIn, blueIn;

        CGImageRef outImage = CGBitmapContextCreateImage(bitmapContext);

        currentImage = [UIImage imageWithCGImage:outImage];

        // Create Body Operaion
        NSString* sb = [[NSString alloc] init];
        int height = currentImage.size.height;
        int width = currentImage.size.width;

        int index = 0;

        char auxBinaryChar[] = {'0', '0', '0', '0', '0', '0', '0', '0'};

        widthBytes = width / 8;

        if (width % 8 > 0) {
            widthBytes = (((int) (width / 8)) + 1);
        } else {
            widthBytes = width / 8;
        }

        total = widthBytes * height;
        
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                
                NSUInteger byteIndex = ((4 * width) * h) + w * numComponents;

                redIn   = (double)data[byteIndex];
                greenIn = (double)data[byteIndex+1];
                blueIn  = (double)data[byteIndex+2];

                char auxChar = '1';

                double totalColor = redIn + greenIn + blueIn;
                
                if (totalColor > blackLimit) {
                    auxChar = '0';
                }

                auxBinaryChar[index] = auxChar;
                index++;

                if (index == 8 || w == (width - 1)) {
                    sb = [sb stringByAppendingString:[self fourByteBinary:[NSString stringWithFormat:@"%s", auxBinaryChar]]];
                    
                    for (int i = 0; i < 8; i++) {
                        auxBinaryChar[i] = '0';
                    }

                    index = 0;
                }
            }
            sb = [sb stringByAppendingString:@"\n"];
        }

        CGImageRelease(outImage);
        CGContextRelease(bitmapContext);
        return sb;
    }
    
    return @"";
}


-(NSString*)fourByteBinary:(NSString*)binaryString {
    
    if([binaryString length] >= 8)
    {
        NSString *secondTwoByte = [binaryString substringWithRange:NSMakeRange(0, 4)];
        NSString *firstTwoByte = [binaryString substringWithRange:NSMakeRange(4, 4)];
        
        NSInteger firstTwoByteDecimal = [Utility parseIntegerWithString:firstTwoByte radix:2];
        NSString *firstTwoByteBinary = [Utility integerToStringWithCurrentInt:firstTwoByteDecimal radix:16];
        
        NSInteger secondTwoByteDecimal = [Utility parseIntegerWithString:secondTwoByte radix:2];
        NSString *secondTwoByteBinary = [Utility integerToStringWithCurrentInt:secondTwoByteDecimal radix:16];
        
        binaryString = [NSString stringWithFormat:@"%@%@", secondTwoByteBinary, firstTwoByteBinary];
    }
    
    return binaryString;
}

- (NSString *) encodeHexAscii:(NSString*)code {
    
    long maxLinea = widthBytes * 2;
    
    NSString* sbCode = [[NSString alloc] init];
    NSString* sbLinea = [[NSString alloc] init];

    NSString* previousLine = nil;
    
    int counter = 1;
    
    char aux = [code characterAtIndex:0];
    BOOL firstChar = NO;
    
    for (int i = 1; i < code.length; i++) {
        if (firstChar) {
            aux = [code characterAtIndex:i];
            firstChar = NO;
            continue;
        }
        
        if ([code characterAtIndex:i] == '\n') {
            if (counter >= maxLinea && aux == '0') {
                sbLinea = [sbLinea stringByAppendingString:@","];
            } else if (counter >= maxLinea && aux == 'F') {
                sbLinea = [sbLinea stringByAppendingString:@"!"];
            } else if (counter > 20) {
                int multi20 = (counter / 20) * 20;
                int resto20 = (counter % 20);
                
                sbLinea = [sbLinea stringByAppendingString:[NSString stringWithFormat:@"%@", [mapCode objectForKey:[NSNumber numberWithInt:multi20]]]];
                
                if (resto20 != 0) {
                    sbLinea = [sbLinea stringByAppendingString:[NSString stringWithFormat:@"%@", [mapCode objectForKey:[NSNumber numberWithInt:resto20]]]];
                    sbLinea = [sbLinea stringByAppendingString:[NSString stringWithFormat:@"%c",aux]];
                } else {
                    sbLinea = [sbLinea stringByAppendingString:[NSString stringWithFormat:@"%c",aux]];
                }
            } else {
                sbLinea = [sbLinea stringByAppendingString:[NSString stringWithFormat:@"%@", [mapCode objectForKey:[NSNumber numberWithInt:counter]]]];
                sbLinea = [sbLinea stringByAppendingString:[NSString stringWithFormat:@"%c",aux]];
            }
            
            counter = 1;
            firstChar = YES;
            
            if (sbLinea == previousLine) {
                sbCode = [sbCode stringByAppendingString:@":"];
            } else {
                sbCode = [sbCode stringByAppendingString:[NSString stringWithFormat:@"%@",sbLinea]];
            }
            
            previousLine = sbLinea;
            // TODO: sbLinea.setLength(0); To Reset or Create new
            sbLinea = @"";
            continue;
        }
        
        if (aux == [code characterAtIndex:i]) {
            counter++;
        } else {
            if (counter > 20) {
                int multi20 = (counter / 20) * 20;
                int resto20 = (counter % 20);
                
                sbLinea = [sbLinea stringByAppendingString:[NSString stringWithFormat:@"%@", [mapCode objectForKey:[NSNumber numberWithInt:multi20]]]];
                
                if (resto20 != 0) {
                    sbLinea = [sbLinea stringByAppendingString:[NSString stringWithFormat:@"%@", [mapCode objectForKey:[NSNumber numberWithInt:resto20]]]];
                    sbLinea = [sbLinea stringByAppendingString:[NSString stringWithFormat:@"%c", aux]];
                } else {
                    sbLinea = [sbLinea stringByAppendingString:[NSString stringWithFormat:@"%c", aux]];
                }
            } else {
                sbLinea = [sbLinea stringByAppendingString:[NSString stringWithFormat:@"%@", [mapCode objectForKey:[NSNumber numberWithInt:counter]]]];
                sbLinea = [sbLinea stringByAppendingString:[NSString stringWithFormat:@"%c", aux]];
            }
            
            counter = 1;
            aux = [code characterAtIndex:i];
        }
    }
    
    return sbCode;
}

@end
