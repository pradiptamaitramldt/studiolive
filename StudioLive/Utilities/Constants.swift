//
//  Constants.swift
//  Nexopt
//

import Foundation
import UIKit
/* Nexopt Constants */
struct Constants {
    
    struct Strings {
        // Alert Title
        static let AppName = "StudioLive"
        static let userFullName = "userFullName"
        static let UserID = "user_id"
        static let UserEmail = "email_id"
        static let ChangeEmailOTP = "otp"
        static let OldEmail = "old_email"
        static let NewEmail = "new_email"
        static let profileImage = "profileImage"
        static let Token = "authToken"
        
        // Alert Action Title
        static let yesAction        = "Yes"
        static let noAction         = "No"
        static let okAction         = "Ok"
        static let cancelAction     = "Cancel"
        
        // Alert Messages
        static let logoutMessage = "Are you sure you want to logout?"
        
        static let cameraPermissionRequired        = "It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. Please go to settings and enable camera in \(AppName) app"
        static let cameraNotAvailable              = "Camera is not available on this Device"
        
        static let noConnectionMessage          = "Could not connect to the server. Please check your internet connection"
        static let enableLocationMessage        = "Allow Location Access from Location Services in Settings."
    }
    
    struct AppLaunched {
        static let AppLaunchedKey = "AppLaunched"
    }
    
    struct StoryboardIdentifiers {
        static let main             = "Main"
        static let myProfile        = "MyProfile"
        static let home             = "Home"
        static let messages         = "Messages"
        static let qrCode           = "QRCode"
        static let reports          = "Reports"
    }
    
    struct ViewControllerIdentifiers {
        static let nexoptLoginView            = "LoginView"
        // Master Data Setup
        static let nexoptTermsConditionView   = "TermsConditionsView"
        // Cases
        static let nexoptLeftMenu             = "LeftMenu"
        static let nexoptSideMenuRootView     = "SideMenuRootView"
        static let filterViewController       = "FilterViewController"
        static let nexoptMyProfileView        = "MyProfileView"
        static let nexoptHomeView             = "HomeView"
        
        // Master
        static let nexoptMyMessageView        = "MyMessageView"
        static let nexoptMessageDetailView    = "MessageDetailView"
        static let nexoptQRScannerDriveView   = "QRScannerDriveView"
        
        // Case Info
        static let nexoptQRAuthenticationView = "QRAuthenticationView"
        static let nexoptCompletedReportsView = "CompletedReportsView"
        static let nexoptReportsView          = "ReportsView"
    }
    
    struct Color {
        //static let themeRedColor               =  UIColor(red: 194.0/255.0, green: 49.0/255.0, blue: 52.0/255.0, alpha: 1.0)
        
        // TODO: Read Below <<< DISCUSS >>>
        /* FIRST VERIFY COLOR ON DEVICE, then -
         1. Constants.Color.themeBlueColor is rgb(30,128,239)
         2. navigationBar.barTintColor appears darker on simulator while setting color programatically, which is rgb(27,104,235)
         3. As of now navigationBar.barTintColor is rgb(33,150,243) which matches the requirement.
         4. This is happening bacause of 'sRGB' color profile and 'Generic RGB' colors, both has different rgb values for colors.
         5. DISCUSS and VERIFY this usage.
         */
        
        static let themeBlueColor               = UIColor(red: 7.0/255.0, green: 19.0/255.0, blue: 61.0/255.0, alpha: 1.0)
        static let whiteColor = UIColor.white
        
        static let sRGBThemeBlueColor           = UIColor(red: 33.0/255.0, green: 150.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        static let sRGBisTranslucentThemeBlueColor = UIColor(red: 0.0/255.0, green: 133.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        static let swipeOptionRed   = UIColor(red: 239.0/255.0, green: 83.0/255.0, blue: 80.0/255.0, alpha: 1.0)
        static let swipeOptionGreen = UIColor(red: 32.0/255.0, green: 121.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        static let swipeOptionGray  = UIColor(red: 173.0/255.0, green: 173.0/255.0, blue: 173.0/255.0, alpha: 1.0)
        static let swipeOptionBlue  = UIColor(red: 56.0/255.0, green: 91.0/255.0, blue: 191.0/255.0, alpha: 1.0)
    }
    
    struct APIPath {
//        static let webServer              = "https://nodeserver.brainiuminfotech.com:3047/";
        static let webServer              = "https://nodeserver.mydevfactory.com:3047/";
        
        static let baseURL                = webServer
        
        static let userLogin              = baseURL + "api/login"
        static let userRegistration       = baseURL + "api/register"
        static let accountVerification    = baseURL + "api/verifyAccount"
        static let forgotPassword         = baseURL + "api/forgotPassword"
        static let resetPassword          = baseURL + "api/resetPassword"
        static let resendOTP              = baseURL + "api/resendOtp"
        static let cmsRequest             = baseURL + "api/getCmsPages"
        static let viewProfile            = baseURL + "api/viewProfile"
        static let editProfile            = baseURL + "api/editProfile"
        static let profileImageUpload     = baseURL + "api/profileImageUpload"
        static let changeEmail            = baseURL + "api/changeEmail"
        static let changePassword         = baseURL + "api/changePassword"
        static let verifyEmail            = baseURL + "api/verifyEmail"
        static let audioCategory          = baseURL + "api/getAllCategories"
        static let postAudio              = baseURL + "api/userPost"
        static let fetchDashboard         = baseURL + "api/myPost"
        static let likeUnlikePost         = baseURL + "api/musicLikeUnlike"
        static let favUnFavPost           = baseURL + "api/musicFavouriteUnfavourite"
        static let fetchAllUser           = baseURL + "api/fetchAllUser"
        static let fetchAllConnectedUser  = baseURL + "api/fetchAllConnectedUser"
        static let sendConnectionRequest  = baseURL + "api/sendRequest"
        static let acceptConnectionRequest = baseURL + "api/acceptRequest"
        static let fetchNotification      = baseURL + "api/fetchNotification"
        static let fetchHome              = baseURL + "api/home"
        static let deletePost             = baseURL + "api/deletePost"
        
    }
    
    struct Keychain {
        static let kUid = "uid"
        static let kDsn = "email"
        static let kPassword = "password"
        static let kAuthToken = "token"
    }
    
    struct AlertTypeID {
        static let futureAlertTypeID = "FutureAlertTypeID"
        static let transferInitiatedAlertTypeID = "TransferInitiatedAlertTypeID"
    }
    
    struct UserType {
        static let GUEST = 0
        static let REGULAR = 1
        static let ADMIN = 2
    }
    
    struct DateFormat {
        public static let FILE_NAME_FORMAT = "yyyy_MM_dd_HH_mm_ss"
        public static let FILE_NAME_FORMAT_ONLY_DIGITS = "yyyyMMddHHmmss";
        public static let FILE_NAME_DATE = "yyyy_MM_dd";
        public static let FILE_NAME_TIME = "HH_mm_ss";
        
        public static let DD_MM_YYYY = "dd:MM:yyyy";
        public static let DD_MM_YYYY_SLASH = "dd/MM/yyyy";
        public static let DD_MM_YYYY_HH_MM_SS = "dd:MM:yyyy HH:mm:ss";
        public static let DD_MM_YYYY_HH_MM_SS_A = "dd:MM:yyyy hh:mm:ss a";
        public static let DD_MMM_YYYY = "dd MMM yyyy";
        public static let DD_MMM_YYYY_WITH_COMMA = "dd MMM, yyyy";
        public static let DD_MMMM = "dd MMMM";
        public static let DD_MMMM_YYYY = "dd MMMM yyyy";
        public static let EEEE_D_MMM_YYYY = "EEEE, d MMM yyyy";
        public static let EEE = "EEE";
        public static let EEEE = "EEEE";
        public static let EEE_MMM_DD = "EEE, MMM dd";
        public static let EEE_MMM_DD_YYYY = "EEE, MMM dd, yyyy";
        public static let EEE_MMM_DD_YYYY_HH_MM_SS_A = "EEE, MMM dd, yyyy hh:mm:ss a";
        
        public static let H_MM_A = "h:mm a";
        public static let HH_MM_A = "hh:mm a";
        public static let HH_MM_A_Z = "hh:mm a z";
        public static let HH_MM_SS = "HH:mm:ss";
        public static let HH_MM_SS_A = "hh:mm:ss a";
        
        public static let MM_DD_YYYY = "MM-dd-yyyy";
        public static let MM_DD_YYYY_HH_MM_A = "MM-dd-yyyy hh:mm a";
        public static let MM_DD_YYYY_HH_MM = "MM-dd-yyyy HH:mm";
        public static let MM_DD_YYYY_HH_MM_SLASH = "MM/dd/yyyy HH:mm";
        public static let MM_DD_YYYY_SLASH = "MM/dd/yyyy";
        public static let MM_DD_YYYY_HH_MM_A_SLASH = "MM/dd/yyyy hh:mm a";
        public static let MM_DD_YYYY_HH_MM_A_Z_SLASH = "MM/dd/yyyy hh:mm a z";
        public static let MMM_D_YYYY = "MMM d, yyyy";
        public static let MMM_D_HH_MM_A = "MMM d, hh:mm a";
        public static let MMM_DD = "MMM dd";
        public static let MMM_DD_HH_MM_A = "MMM dd, hh:mm a";
        public static let MMM_DD_HH_MM_A_DELIMITED = "MMM dd, - hh:mm a";
        public static let MMM_DD_YYYY = "MMM dd, yyyy";
        public static let MMM_DD_YYYY_HH_MM = "MMM dd, yyyy HH:mm";
        public static let MMM_DD_YYYY_HH_MM_A = "MMM dd, yyyy hh:mm a";
        public static let MMM_DD_YYYY_HH_MM_A_DELIMITED = "MMM dd, yyyy - hh:mm a";
        public static let MMM_DD_YYYY_HH_MM_SS_A = "MMM dd, yyyy - hh:mm:ss a";
        public static let MMM_DD_YYYY_HH_MM_A_DELIMITED_ZZZ = "MMM dd, yyyy - HH:mm: a zzz";
        public static let MMMM_YYYY = "MMMM yyyy";
        public static let MMMM_DD = "MMMM dd";
        public static let MMMM_DD_YYYY = "MMMM dd, yyyy";
        public static let YYYY = "yyyy";
        public static let YYYY_MM_DD = "yyyy-MM-dd";
        public static let YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
        public static let YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss"; 
        public static let YYYY_MM_DD_T_HH_MM_SS = "yyyy-MM-dd'T'HH:mm:ss";
        public static let YYYY_MM_DD_T_HH_MM_SS_Z = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        public static let YYYY_MM_DD_T_HH_MM_SS_SSS_Z = "yyyy-MM-ddTHH:mm:ss.SSSZ";
        public static let C_SHARP_TIME_FORMAT = "'PT'HH'H'mm'M'ss.S'S'";
        public static let C_SHARP_TIME_FORMAT_HOUR = "'PT'HH'H'";
        public static let C_SHARP_TIME_FORMAT_HOUR_MINUTE = "'PT'HH'H'mm'M'";
    }
    
    struct CaseListFilter {
        static let cfCase        = "caseFilterCase"
        static let cfFromDate    = "caseFilterFromDate"
        static let cfToDate      = "caseFilterToDate"
        static let cfDSN         = "caseFilterDSN"
        static let cfType        = "caseFilterType"
        static let cfZip         = "caseFilterZip"
        static let cfSortBy      = "caseFilterSortBy"
        static let isCaseFilterApplied      = "isCaseFilterApplied"
        
        static let kSortCaseNumber   = "caseNumber"
        static let kSortIncidentDate = "incidentDate"
    }
    
    struct NotificationKeys {
        static let fileDownloadStatusKey        = "dStatus-%@"
        static let fileUploadStatusKey          = "uStatus-%@"
    }
    
    struct AlertsFilter {
        static let afFromDate   = "alertFilterFromDate"
        static let afToDate     = "alertFilterToDate"
        static let afAlertType  = "alertFilterAlertType"
        static let isAlertsFilterApplied = "isAlertsFilterApplied"
    }
    
    struct LoginStatus {
        static let keyLogin             = "ETWLogin"
        static let valueUserLoggedIn    = "ETWUserLoggedIn"
        static let valueUserLoggedOut   = "ETWUserLoggedOut"
    }
    
    struct EvidenceTransferStatus {
        static let initiated = "I"
        static let cancel = "C"
        static let reject = "R"
        static let accept = "A"
        static let finalStatus = "F"
    }
    
    struct UsePrevious {
        static let usePreviousStreet = "usePreviousStreet"
        static let usePreviousApt = "usePreviousApt"
        static let usePreviousCity = "usePreviousCity"
        static let usePreviousZip = "usePreviousZip"
        static let usePreviousPhoneNumber = "usePreviousPhoneNumber"
    }
    
    struct MasterSync {
        static let masterSyncLastSyncDate   = "masterSyncLastSyncDate"
        static let masterSyncStatus         = "masterSyncStatus"
        static let masterSyncCompleted      = "masterSyncCompleted"
        static let masterSyncNotSynced      = "masterSyncNotSynced"
    }
    
    struct IDScanSDK {
        static let iOSCameraScanningSDKKey = "iOSCameraScanningSDKKey"
        static let DriverLicenseParserCurrentSerial = "DriverLicenseParserCurrentSerial"
    }
    
    struct FirebaseCloudMessaging {
        static let fcmRegistrationToken = "fcmRegistrationToken"
        static let isFCMTokenRegisteredOnServer = "isFCMTokenRegisteredOnServer"
        static let openAlertsScreenOnPushNotificationSelection = "OpenAlertsScreenOnPushNotificationSelection"
    }
}
extension Dictionary {
    func toJson() {
        let jsonData = try! JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        print("JSON string = \(jsonString)")
    }
}
extension UIView {
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
}
