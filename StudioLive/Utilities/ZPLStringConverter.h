//
//  ZPLStringConverter.h
//  Nexopt
//
//  Created by admin on 18/01/19.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ZPLStringConverter : NSObject

-(NSString*)getZPLDataFrom:(UIImage*)bitmap with:(BOOL)addHeaderFooter;

-(NSString*)encodeHexAscii:(NSString*)code;

@end
