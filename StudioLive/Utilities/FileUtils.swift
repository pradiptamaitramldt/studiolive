import UIKit

class FileUtils: NSObject {
    static let shared : FileUtils = FileUtils()
    
    private var mediaFolderPath : String?
    
    func mediaFilesDirectory() throws -> String {
        let fileManager = FileManager.default
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as String
        let path = documentDirectory.appending("/MediaFiles")
        if !fileManager.fileExists(atPath: path) {
            try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
        return path
    }
    
    func getMediaFolderPath() throws -> String? {
        if self.mediaFolderPath == nil {
            self.mediaFolderPath = try self.mediaFilesDirectory()
        }
        return self.mediaFolderPath
    }
    
    // Considering filename is with extension
    func saveImageFile(data: Data, fileName:String) throws -> URL{
        if let mediaDirectory = try getMediaFolderPath() {
            let path = mediaDirectory + "/" + fileName
            let filePath = URL(fileURLWithPath: path)
            try data.write(to: filePath)
            print(filePath)
            return filePath
        }
        throw NSError(domain: "Nexopt", code: 1, userInfo: [NSLocalizedDescriptionKey:"Failed to save file - \(fileName)"])
    }
    
    func isExistAtPath(path:String) -> Bool {
        return FileManager.default.fileExists(atPath: path)
    }
    
//    func getImageForFileName(fileName:String) throws -> UIImage?{
//        if fileName.count == 0 {
//            throw NSError(domain: "FileDetails", code: 100, userInfo: [NSLocalizedDescriptionKey:"File Name should not be empty"])
//        }
//        let mediaDirectory = try self.mediaFilesDirectory()
//        let filePath = URL(fileURLWithPath: mediaDirectory.appending("/").appending(fileName))// Change
//        if self.isExistAtPath(path: filePath.path) {
//            let image = UIImage(contentsOfFile: filePath.path)
//            return image
//        }
//        else {
//            // TODO: Return thumbnail Image
//            throw NSError(domain: "FileDetails", code: 100, userInfo: [NSLocalizedDescriptionKey: "Image not found file name \(fileName)"])
//        }
//    }
    
    func getImageForFileName(fileName: String) -> UIImage? {
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as String
        let path = documentDirectory.appending("/MediaFiles")
        
        var filePath = URL(fileURLWithPath: path.appending("/").appending(fileName))// Change
        // When there is no path extension for filename, it will search file by appending jpg extension to it
        if filePath.pathExtension.count == 0 {
            filePath = filePath.appendingPathExtension("jpg")
        }
        
        if self.isExistAtPath(path: filePath.path) {
            let image = UIImage(contentsOfFile: filePath.path)
            return image
        }
        return nil
    }
    
    func getFileURLFor(fileName: String) -> URL {
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as String
        let path = documentDirectory.appending("/MediaFiles")
        return URL(fileURLWithPath: path.appending("/").appending(fileName))// Change
    }
    
    func getImageFrom(path:String) -> UIImage?{
        let filePath = URL(fileURLWithPath: path)
        
        if self.isExistAtPath(path: filePath.path) {
            let image = UIImage(contentsOfFile: filePath.path)
            return image
        }
        return nil
    }
    
    func removeFile(filePath: URL) throws{
        let fileManager = FileManager.default
        if self.isExistAtPath(path: filePath.path) {
            try fileManager.removeItem(at: filePath)
        }
    }
    
    func removeFile(fileName: String) throws {
        if let mediaDirectory = try self.getMediaFolderPath() {
            let filePath = URL(fileURLWithPath: mediaDirectory.appending("/").appending(fileName))
            try self.removeFile(filePath: filePath)
        }
        else {
            throw NSError(domain: "RemoveThumbnail", code: 100, userInfo: [NSLocalizedDescriptionKey:"MediaFolderPath is null"])
        }
    }
    
    func renameFile(oldFileName: String, newFileName: String) throws -> String {
        if let documentDirectory = try self.getMediaFolderPath() {
            
            let currentPath = documentDirectory.appending("/").appending(oldFileName)
            let destinationPath = documentDirectory.appending("/").appending(newFileName)
            
            let fileManager = FileManager.default
            try fileManager.moveItem(atPath: currentPath, toPath: destinationPath)
            
            return destinationPath
        }
        else {
            // TODO: Log Error
            throw NSError(domain: "Nexopt", code: 1, userInfo: [NSLocalizedDescriptionKey: "Failed to rename file"])
        }
    }
}
