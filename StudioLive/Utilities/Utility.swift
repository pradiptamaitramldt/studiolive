//
//  Utility.swift
//  Nexopt
//

import Foundation
import UIKit

class Utility : NSObject {
    let prefs = UserDefaults.standard
    
    @objc static func showAlert(withMessage : String, onController : UIViewController) {
        let alertController = UIAlertController(title: Constants.Strings.AppName, message: withMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: Constants.Strings.okAction, style: .default, handler: nil)
        alertController.addAction(okAction)
        onController.present(alertController, animated: true, completion: nil)
    }
    
    static func showAlert(withTitle: String, andMessage: String, onController: UIViewController, actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: withTitle, message: andMessage, preferredStyle: .alert)
        for action in actions {
            alertController.addAction(action)
        }
        onController.present(alertController, animated: true, completion: nil)
    }
    
    public static func log(_ items: Any?) {
        #if DEBUG//log will work only for debug mode
            print(items ?? "Nil value")
        #endif
    }
    
    static func isNetworkReachable() -> Bool {
        return (Reachability()?.connection != Reachability.Connection.none)
    }
    
    func Save(_ str:String,keyname:String) {
        prefs.setValue(str, forKey: keyname)
        prefs.synchronize()
    }
    
    func Retrive(_ str:String)-> AnyObject {
        let retrivevalue =  prefs.value(forKey: str)
        if retrivevalue != nil {
            return retrivevalue! as AnyObject
        }
        return "" as AnyObject
    }
    
    static func handleViewForKeyboard(notification: NSNotification, activeField: UIView, scrollView: UIScrollView, selfViewFrame: CGRect) {
        if let keyboardSize = (notification.userInfo? [UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            scrollView.contentInset = contentInsets
            scrollView.scrollIndicatorInsets = contentInsets
            
            var aRect = selfViewFrame
            aRect.size.height -= keyboardSize.size.height
            
            if (!aRect.contains(activeField.frame.origin)) {
                scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    static func convertTimeStampToDate(timeStamp: String) -> String {
        var date = Date()
        var localDate = String()
        if let timeResult = Double(timeStamp) {
            date = Date(timeIntervalSince1970: timeResult)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
            //dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
            //dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
            localDate = dateFormatter.string(from: date)
        }
        return localDate
    }
    
    static func getFormmatedStringFrom(date:Date, format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale     = Locale.current
        return dateFormatter.string(from:date)
    }
    
    static func getFormmatedDateFrom(date:String!, format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale     = Locale.current
        return dateFormatter.date(from:date)!
    }
    
    static func getlocalToUTCFormattedDate(date: Date, format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone   = TimeZone(identifier: "UTC")
        return dateFormatter.string(from:date)
    }
    
    static func dateFormatterUTC(format: String)->DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter
    }
    
    static func dateFormatterCurrent(format: String)->DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateFormat.YYYY_MM_DD_HH_MM_SS
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter
    }
    
    static func localToUTC(_ string: String) -> String? {
        let dateFormatter = self.dateFormatterCurrent(format: Constants.DateFormat.YYYY_MM_DD_HH_MM_SS)
        if let date = dateFormatter.date(from: string) {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    static func localToUTCDate(_ string: String) -> Date? {
        let dateFormatter = self.dateFormatterCurrent(format: Constants.DateFormat.YYYY_MM_DD_T_HH_MM_SS_SSS_Z)
        if let date = dateFormatter.date(from: string) {
            return date
        }
        return nil
    }
    
    static func utcToLocal(_ string: String, format: String) -> String? {
        let dateFormatter = self.dateFormatterCurrent(format: format)
        if let date = dateFormatter.date(from: string) {
            dateFormatter.dateFormat = format
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    //    static func convertToUTCFromLocalDate(dateStr : String, format: String) -> String? {
    //        let formatter = DateFormatter()
    //        formatter.dateFormat = format
    //        let utc = NSTimeZone(abbreviation: "UTC")
    //        formatter.timeZone = utc as TimeZone?
    //        formatter.dateFormat = format
    //        guard let localDate = formatter.date(from: dateStr) else{
    //            return nil
    //        }
    //        let timeZoneOffset: TimeInterval = TimeInterval(NSTimeZone.default.secondsFromGMT())
    //        let utcTimeInterval: TimeInterval? = localDate.timeIntervalSinceReferenceDate - timeZoneOffset
    //        let utcCurrentDate = Date(timeIntervalSinceReferenceDate: utcTimeInterval!)
    //        print("UTC time %@",utcCurrentDate)
    //        return formatter.string(from: utcCurrentDate)
    //    }
    //
    //    static func convertToUTCFromLocalDate(localDate : Date, format: String) -> String? {
    //
    //        let formatter = DateFormatter()
    //        formatter.timeZone = TimeZone(abbreviation: "UTC")
    //        formatter.dateFormat = format
    //
    //        let timeZoneOffset: TimeInterval = TimeInterval(NSTimeZone.default.secondsFromGMT())
    //        let utcTimeInterval  = localDate.timeIntervalSinceReferenceDate - timeZoneOffset
    //        let utcCurrentDate = Date(timeIntervalSinceReferenceDate: utcTimeInterval)
    //        return formatter.string(from: utcCurrentDate)
    //
    //    }
    
    static func convertToLocalFromUTC(_ dateStr : String, format: String) -> String? {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = Constants.DateFormat.YYYY_MM_DD_HH_MM_SS
        guard let serverDate = formatter.date(from: dateStr) else {
            return nil
        }
        let timeZoneOffset: TimeInterval = TimeInterval(NSTimeZone.default.secondsFromGMT())
        formatter.dateFormat = format
        let utcTimeInterval: TimeInterval? = serverDate.timeIntervalSinceReferenceDate + timeZoneOffset
        let currentDate = Date(timeIntervalSinceReferenceDate: utcTimeInterval!)
        
        return formatter.string(from: currentDate)
    }
    
    static func convertToGMTFromUTC(_ dateStr : String, format: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateFormat.YYYY_MM_DD_HH_MM_SS
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let dt = dateFormatter.date(from: dateStr) else {
            return nil
        }
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT")
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: dt)
    }
    
    static func convertDateToString(_alertDate : Date, format : String) -> String?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateFormat.YYYY_MM_DD_HH_MM_SS
        return dateFormatter.string(from: _alertDate)
    }
    
    static func convertStringToDate(_alertDateStr : String, format : String) -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateFormat.YYYY_MM_DD_HH_MM_SS
        return dateFormatter.date(from: _alertDateStr)
    }
    
    static func showBirthdateInUTC(_ dateStr: String, format: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        dateFormatter.dateFormat = Constants.DateFormat.YYYY_MM_DD_HH_MM_SS
        guard let dt = dateFormatter.date(from: dateStr) else {
            return nil
        }
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: dt)
    }
    
    static func getPartyBirthdateForIDScanInUTC(_ dateStr: String, format: String) -> Date? {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(abbreviation: "UTC")
        formatter.dateFormat = Constants.DateFormat.MM_DD_YYYY_SLASH
        guard let serverDate = formatter.date(from: dateStr) else {
            return nil
        }
        formatter.dateFormat = format
        return serverDate
    }
    
    static func showPartyBirthdateStringInUTCForIDScan(_ dateStr: String, format: String) -> String? {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(abbreviation: "UTC")
        formatter.dateFormat = Constants.DateFormat.MM_DD_YYYY_SLASH
        guard let serverDate = formatter.date(from: dateStr) else {
            return nil
        }
        formatter.dateFormat = format
        return formatter.string(from: serverDate)
    }
    
    /*static func previewImage(fileName:String?, onController: UIViewController, title:String) {
        let storyboard = UIStoryboard(name: Constants.StoryboardIdentifiers.common, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Constants.ViewControllerIdentifiers.previewImageViewController) as! PreviewImageViewController
        if let fileName = fileName {
            controller.image = FileUtils.shared.getImageForFileName(fileName: fileName)
            controller.imagePath  = FileUtils.shared.getFileURLFor(fileName: fileName).path
        }
        controller.title        = title
        let navController = CommonNavigationController(rootViewController: controller)
        onController.present(navController, animated: true, completion: nil)
    }*/
    
    static func isToday(date : Date) -> Bool {
        let today : Bool = NSCalendar.current.isDateInToday(date)
        if (today) {
            return true
        } else {
            return false
        }
    }
    
    static func isYesterday(date: Date) -> Bool {
        let yesterday : Bool = NSCalendar.current.isDateInYesterday(date)
        if (yesterday) {
            return true
        } else {
            return false
        }
    }
    
    static func isFirstLogin() -> Bool {
        let firstLogin : String? = UserDefaults.standard.string(forKey: Constants.LoginStatus.keyLogin)
        if firstLogin == Constants.LoginStatus.valueUserLoggedOut || firstLogin == nil {
            return true
        } else {
            return false
        }
    }
    
    static func getSyncMessage() -> String? {
        
        if let lastSyncDate = UserDefaults.standard.value(forKey: Constants.MasterSync.masterSyncLastSyncDate) as? Date {
            let elapsedSeconds = Date.init().timeIntervalSince(lastSyncDate) //lastSyncDate.timeIntervalSince(Date.init())
            
            let minutes = Int(elapsedSeconds / 60)
            let hours = Int(minutes / 60)
            let days = Int(hours / 24)
            
            if (days == 1) {
                
                return " \(days)" + " Day";
            }
            
            if (days > 1) {
                return " \(days)" + " Days";
            }
            
            if (hours == 1) {
                return " \(hours)" + " Hour";
            }
            
            if (hours > 1) {
                return " \(hours)" + " Hours";
            }
            
            if (minutes == 1) { //if (minutes == 0 || minutes == 1) {
                return " \(minutes)" + " minute";
            }
            
            if (minutes > 1) {
                return " \(minutes)" + " minutes";
            }
            
            return " few seconds";
        }
        
        return nil
    }
    
    /*static func isUserType(userType: Int16) -> Bool {
        
        let sessionRep = ETWSessionRepository.shared
        sessionRep.load()
        
        if let uid = sessionRep.uid, let user = ETWUserRepository().getUser(uid: uid) {
            
            return user.userType == userType
        }
        
        return false
    }*/
    
    static func formattedPhoneNumber(_ phoneNumber : String?) -> String? {
        
        if let phoneNumber = phoneNumber{
            
            let formattedPhoneNumber = String(phoneNumber.filter { "01234567890.".contains($0) })
            
            if(formattedPhoneNumber.count > 10 || formattedPhoneNumber.count <= 3){
                return formattedPhoneNumber
            } else if(formattedPhoneNumber.count > 6){
                
                let firstRange = formattedPhoneNumber.index(formattedPhoneNumber.startIndex, offsetBy: 0)..<formattedPhoneNumber.index(formattedPhoneNumber.startIndex, offsetBy: 3)
                let secondRange = formattedPhoneNumber.index(formattedPhoneNumber.startIndex, offsetBy: 3)..<formattedPhoneNumber.index(formattedPhoneNumber.startIndex, offsetBy: 6)
                let thirdRange = formattedPhoneNumber.index(formattedPhoneNumber.startIndex, offsetBy: 6)..<formattedPhoneNumber.index(formattedPhoneNumber.endIndex, offsetBy: 0)
                
                return "\(String(formattedPhoneNumber[firstRange]))-\(String(formattedPhoneNumber[secondRange]))-\(String(formattedPhoneNumber[thirdRange]))"
            } else if(formattedPhoneNumber.count > 3){
                
                let firstRange = formattedPhoneNumber.index(formattedPhoneNumber.startIndex, offsetBy: 0)..<formattedPhoneNumber.index(formattedPhoneNumber.startIndex, offsetBy: 3)
                let secondRange = formattedPhoneNumber.index(formattedPhoneNumber.startIndex, offsetBy: 3)..<formattedPhoneNumber.index(formattedPhoneNumber.endIndex, offsetBy: 0)
                
                return "\(String(formattedPhoneNumber[firstRange]))-\(String(formattedPhoneNumber[secondRange]))"
            }
        }
        
        return phoneNumber
    }
    
    // For ZPLStringConverter
    @objc public static func parseInteger(string: String, radix: Int) -> Int {
        
        if let convertedData = Int(string, radix: radix) {
            
            return convertedData
        }
        
        return 0
    }
    
    // For ZPLStringConverter
    @objc public static func integerToString(currentInt: Int, radix: Int) -> String {
        return String(currentInt, radix: radix).uppercased()
    }
    
    public static func parseZipCode(zipCode: String) -> [String] {
        do {
            // Regular Expression: Get first five digits
            let regex = try NSRegularExpression(pattern: "\\d{5}(?:[\\s]\\d{4})?")
            let results = regex.matches(in: zipCode, range: NSRange(zipCode.startIndex..., in: zipCode))
            
            let currentResults = results.map { (result) -> String in
                
                if let range = Range(result.range, in:zipCode) {

                    return String(zipCode[range])
                }

                return ""
            }
            
            return currentResults
        } catch {
            // TODO: Log Error
            return []
        }
    }
    
    @objc public static func getAllPageImagesFromPDFURL(url: URL) -> [UIImage] {
        
        var images: [UIImage] = []
        
        guard let document = CGPDFDocument(url as CFURL) else { return images }
        
        for i in 0..<document.numberOfPages{
            
            if let page = document.page(at: i + 1) {
                
                let pageRect = page.getBoxRect(.mediaBox)
                let renderer = UIGraphicsImageRenderer(size: pageRect.size)
                
                let currentImage = renderer.image { (imageRendererContext) in
                    UIColor.white.set()
                    imageRendererContext.fill(pageRect)
                    
                    imageRendererContext.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
                    imageRendererContext.cgContext.scaleBy(x: 1.0, y: -1.0)
                    
                    imageRendererContext.cgContext.drawPDFPage(page)
                }
                
                images.append(currentImage)
            }
        }
        
        return images
    }
}
