import Foundation

extension String {
    
    func containsOnlyCharactersIn(matchCharacters: String, replacementString string: String) -> Bool {
        let aSet = NSCharacterSet(charactersIn: matchCharacters).inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
}
