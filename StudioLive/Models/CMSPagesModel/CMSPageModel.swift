//
//  CMSPageModel.swift
//  StudioLive
//
//  Created by Pradipta on 27/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation

// MARK: - CMSPagesModel
class CMSPagesModel: Codable {
    let responseCode: Int?
    let responseMessage: String?
    let responseData: CMSResponseData?
    
    enum CodingKeys: String, CodingKey {
        case responseCode = "response_code"
        case responseMessage = "response_message"
        case responseData = "response_data"
    }
    
    init(responseCode: Int?, responseMessage: String?, responseData: CMSResponseData?) {
        self.responseCode = responseCode
        self.responseMessage = responseMessage
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class CMSResponseData: Codable {
    let title, slug, responseDataDescription, id: String?
    let createdAt, updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case title, slug
        case responseDataDescription = "description"
        case id = "_id"
        case createdAt, updatedAt
    }
    
    init(title: String?, slug: String?, responseDataDescription: String?, id: String?, createdAt: String?, updatedAt: String?) {
        self.title = title
        self.slug = slug
        self.responseDataDescription = responseDataDescription
        self.id = id
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}
