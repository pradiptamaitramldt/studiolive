//
//  FetchNotificationModel.swift
//  StudioLive
//
//  Created by BrainiumSSD on 06/11/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation
// MARK: - FetchNotificationRequestModel
struct FetchNotificationRequestModel: Codable {
    let success: Bool
    let statuscode: Int
    let message: String
    let responseData: NotificationData

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct NotificationData: Codable {
    let userNot: [UserNot]
}

// MARK: - UserNot
struct UserNot: Codable {
    let customerID, notificationType, title, message: String
    let otherData, sendUserEmail: String
    let profileImage: String

    enum CodingKeys: String, CodingKey {
        case customerID = "customerId"
        case notificationType, title, message, otherData, sendUserEmail, profileImage
    }
}
