//
//  AccountVerificationModel.swift
//  StudioLive
//
//  Created by Pradipta on 20/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation

// MARK: - AccountVerificationModel
class AccountVerificationModel: Codable {
    let responseCode: Int?
    let responseMessage: String?
    let responseData: AccountVerificationResponseData?
    
    enum CodingKeys: String, CodingKey {
        case responseCode = "response_code"
        case responseMessage = "response_message"
        case responseData = "response_data"
    }
    
    init(responseCode: Int?, responseMessage: String?, responseData: AccountVerificationResponseData?) {
        self.responseCode = responseCode
        self.responseMessage = responseMessage
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class AccountVerificationResponseData: Codable {
    let userDetails: UserDetails?
    let authToken: String?
    let imageUrl : String?
    init(userDetails: UserDetails?, authToken: String?,imageUrl : String?) {
        self.userDetails = userDetails
        self.authToken = authToken
        self.imageUrl = imageUrl
    }
}

// MARK: - UserDetails
/*class UserDetails: Codable {
    let name, email, password, profileImage: String?
    let otp, otpVerify, deviceToken, id: String?
    let dateOfBirth, createdAt, updatedAt: String?
    let v: Int?
    
    enum CodingKeys: String, CodingKey {
        case name, email, password, profileImage, otp, otpVerify, deviceToken
        case id = "_id"
        case dateOfBirth, createdAt, updatedAt
        case v = "__v"
    }
    
    init(name: String?, email: String?, password: String?, profileImage: String?, otp: String?, otpVerify: String?, deviceToken: String?, id: String?, dateOfBirth: String?, createdAt: String?, updatedAt: String?, v: Int?) {
        self.name = name
        self.email = email
        self.password = password
        self.profileImage = profileImage
        self.otp = otp
        self.otpVerify = otpVerify
        self.deviceToken = deviceToken
        self.id = id
        self.dateOfBirth = dateOfBirth
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.v = v
    }
}*/
