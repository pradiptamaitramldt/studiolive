//
//  PostModel.swift
//  StudioLive
//
//  Created by Pradipta Maitra on 26/09/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation

// MARK: - PostResponseModel
struct PostResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: PostResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct PostResponseData: Codable {
}

// MARK: - AudioCategoryResponseModel
struct AudioCategoryResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: [AudioCategoryResponseDatum]?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseDatum
struct AudioCategoryResponseDatum: Codable {
    let id, categoryName: String?
    let isActive: Bool?
    let createdAt, updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case categoryName, isActive, createdAt, updatedAt
        case v = "__v"
    }
}
