//
//  HomeModel.swift
//  StudioLive
//
//  Created by Pradipta Maitra on 29/09/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation

// MARK: - HomeResponseModel
struct HomeResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: HomeResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct HomeResponseData: Codable {
    let path: String?
    let post: [Post]?
}

// MARK: - Post
struct Post: Codable {
    let title, file, location, musicID: String?
    let fileType, studioPrivacy, createdAt, updatedAt: String?
    let customerID, musicCategory: String?
    let likeNo: Int?
    let isLike: Bool?
    let commentNo, favouriteNo: Int?
    let isFavourite: Bool?
    let customerName, profileImage: String?

    enum CodingKeys: String, CodingKey {
        case title, file, location
        case musicID = "musicId"
        case fileType, studioPrivacy, createdAt, updatedAt
        case customerID = "customerId"
        case musicCategory, likeNo, isLike, commentNo, favouriteNo, isFavourite, customerName, profileImage
    }
}

// MARK: - LikeResponseModel
struct LikeResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: LikeResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct LikeResponseData: Codable {
}
