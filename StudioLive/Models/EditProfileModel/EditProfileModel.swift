//
//  EditProfileModel.swift
//  StudioLive
//
//  Created by Pradipta on 16/03/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation

// MARK: - EditProfile
class EditProfile: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: EditProfileResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: EditProfileResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class EditProfileResponseData: Codable {

    init() {
    }
}
