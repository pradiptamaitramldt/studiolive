//
//  RegistrationModel.swift
//  StudioLive
//
//  Created by Pradipta on 20/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation

// MARK: - RegistrationModel
class RegistrationModel: Codable {
    let responseCode: Int?
    let responseMessage: String?
    let responseData: RegistrationResponseData?
    
    enum CodingKeys: String, CodingKey {
        case responseCode = "response_code"
        case responseMessage = "response_message"
        case responseData = "response_data"
    }
    
    init(responseCode: Int?, responseMessage: String?, responseData: RegistrationResponseData?) {
        self.responseCode = responseCode
        self.responseMessage = responseMessage
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class RegistrationResponseData: Codable {
    let id, email, otp: String?
    
    init(id: String?, email: String?, otp: String?) {
        self.id = id
        self.email = email
        self.otp = otp
    }
}
