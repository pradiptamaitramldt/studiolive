//
//  ForgotPasswordModel.swift
//  StudioLive
//
//  Created by Pradipta on 20/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation

// MARK: - ForgotPasswordModel
class ForgotPasswordModel: Codable {
    let responseCode: Int?
    let responseMessage: String?
    let responseData: ForgotPasswordResponseData?
    
    enum CodingKeys: String, CodingKey {
        case responseCode = "response_code"
        case responseMessage = "response_message"
        case responseData = "response_data"
    }
    
    init(responseCode: Int?, responseMessage: String?, responseData: ForgotPasswordResponseData?) {
        self.responseCode = responseCode
        self.responseMessage = responseMessage
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class ForgotPasswordResponseData: Codable {
    let email, forgotPassOtp: String?
    
    init(email: String?, forgotPassOtp: String?) {
        self.email = email
        self.forgotPassOtp = forgotPassOtp
    }
}
