//
//  FetchAllUserModel.swift
//  StudioLive
//
//  Created by BrainiumSSD on 05/11/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation

// MARK: - FetchAllUserModel
struct FetchAllUserModel: Codable {
    let success: Bool
    let statuscode: Int
    let message: String
    let responseData: UserData

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct UserData: Codable {
    let users: [User]
}

// MARK: - User
struct User: Codable {
    let name, email: String
    let profileImage: String
    let customerID: String
    let sendRequest : String?
    enum CodingKeys: String, CodingKey {
        case name, email, profileImage
        case customerID = "customerId"
        case sendRequest = "sendReq"
    }
}
