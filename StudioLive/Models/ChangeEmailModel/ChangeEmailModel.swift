//
//  ChangeEmailModel.swift
//  StudioLive
//
//  Created by Pradipta on 16/03/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation

// MARK: - ChangeEmail
class ChangeEmail: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: ChangeEmailResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: ChangeEmailResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class ChangeEmailResponseData: Codable {
    let id, email, otp, newEmail: String?

    init(id: String?, email: String?, otp: String?, newEmail: String?) {
        self.id = id
        self.email = email
        self.otp = otp
        self.newEmail = newEmail
    }
}
