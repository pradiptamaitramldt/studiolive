//
//  SendFriendRequestModel.swift
//  StudioLive
//
//  Created by BrainiumSSD on 05/11/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation
// MARK: - SendFriendRequestModel
struct SendFriendRequestModel: Codable {
    let success: Bool
    let statuscode: Int
    let message: String
    let responseData: SendFriendRequestData

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct SendFriendRequestData: Codable {
}
