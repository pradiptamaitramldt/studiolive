//
//  ResetPasswordModel.swift
//  StudioLive
//
//  Created by Pradipta on 21/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation

// MARK: - ResetPasswordModel
class ResetPasswordModel: Codable {
    let responseCode: Int?
    let responseMessage: String?
    let responseData: ResetPasswordResponseData?
    
    enum CodingKeys: String, CodingKey {
        case responseCode = "response_code"
        case responseMessage = "response_message"
        case responseData = "response_data"
    }
    
    init(responseCode: Int?, responseMessage: String?, responseData: ResetPasswordResponseData?) {
        self.responseCode = responseCode
        self.responseMessage = responseMessage
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class ResetPasswordResponseData: Codable {
    
    init() {
    }
}
