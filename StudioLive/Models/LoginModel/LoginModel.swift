//
//  LoginModel.swift
//  StudioLive
//
//  Created by Pradipta on 20/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation

// MARK: - LoginModel
class LoginModel: Codable {
    let responseCode: Int?
    let responseMessage: String?
    let responseData: LoginResponseData?

    enum CodingKeys: String, CodingKey {
        case responseCode = "response_code"
        case responseMessage = "response_message"
        case responseData = "response_data"
    }

    init(responseCode: Int?, responseMessage: String?, responseData: LoginResponseData?) {
        self.responseCode = responseCode
        self.responseMessage = responseMessage
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class LoginResponseData: Codable {
    let userDetails: UserDetails?
    let authToken: String?
    let imageUrl : String?
    init(userDetails: UserDetails?, authToken: String?, imageUrl: String?) {
        self.userDetails = userDetails
        self.authToken = authToken
        self.imageUrl = imageUrl
    }
}

// MARK: - UserDetails
class UserDetails: Codable {
    let name, firstName, lastName, email: String?
    let phone: NameUnion?
    let password, profileImage, otp, otpVerify: String?
    let deviceToken, id, dateOfBirth, createdAt: String?
    let updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case name, firstName, lastName, email, phone, password, otp, otpVerify, deviceToken,profileImage
        case id = "_id"
        case dateOfBirth, createdAt, updatedAt
        case v = "__v"
       
    }

    init(name: String?, firstName: String?, lastName: String?, email: String?, phone: NameUnion?, password: String?, profileImage: String?, otp: String?, otpVerify: String?, deviceToken: String?, id: String?, dateOfBirth: String?, createdAt: String?, updatedAt: String?, v: Int?) {
        self.name = name
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phone = phone
        self.password = password
        self.profileImage = profileImage
        self.otp = otp
        self.otpVerify = otpVerify
        self.deviceToken = deviceToken
        self.id = id
        self.dateOfBirth = dateOfBirth
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.v = v
    }
}

enum NameUnion: Codable {
    case integer(Int)
    case string(String)
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(NameUnion.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for NameUnion"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
