//
//  ViewProfileModel.swift
//  StudioLive
//
//  Created by Pradipta on 13/03/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import Foundation

// MARK: - ViewProfile
class ViewProfile: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: ViewProfileResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: ViewProfileResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class ViewProfileResponseData: Codable {
    let name, firstName, lastName, email: String?
    let phone: Int?
    let profileImage: String?
    let connectedUser : Int?
    init(name: String?, firstName: String?, lastName: String?, email: String?, phone: Int?, profileImage: String?,connectedUser : Int?) {
        self.name = name
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phone = phone
        self.profileImage = profileImage
        self.connectedUser = connectedUser
    }
}
