//
//  HTTPRequestCall.swift

import UIKit

class HTTPRequestCall: NSObject {

    var HTTPRequest: HTTPRequest
    var HTTPResponseHandler: HTTPResponseHandler
    
    init(HTTPRequest: HTTPRequest, HTTPResponseHandler: HTTPResponseHandler) {
        self.HTTPRequest = HTTPRequest
        self.HTTPResponseHandler = HTTPResponseHandler
    }
    
}
