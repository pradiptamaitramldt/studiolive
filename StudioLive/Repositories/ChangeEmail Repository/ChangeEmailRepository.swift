//
//  ChangeEmailRepository.swift
//  StudioLive
//
//  Created by Pradipta on 16/03/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ChangeEmailRepository: NSObject {
    var utility = Utility()
    
    func changeEmail(changeEmailCommand: ChangeEmailCommand, vc: UIViewController, completion: @escaping (ChangeEmail, Bool, NSError?) -> Void) {
        let userID = self.utility.Retrive(Constants.Strings.UserID)
        let paramDict = ["customerId":userID, "newEmail": changeEmailCommand.email!, "confirmEmail": changeEmailCommand.confirmEmail!] as [String : Any]
        let request = ChangeEmailRequest()
        request.changeEmail(parameter: paramDict as! [String : String], vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(ChangeEmail.self, from: response! as! Data)
                    let message = objResponse.message
                    print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
}
