//
//  RegistrationRepository.swift
//  StudioLive
//
//  Created by Pradipta on 20/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class RegistrationRepository: NSObject {
    func registrationUser(registrationCommand: RegistartionCommand, vc: UIViewController, completion: @escaping (RegistrationModel, Bool, NSError?) -> Void) {
        let name = registrationCommand.name
        let email = registrationCommand.email
        let dob = registrationCommand.dob
        let password = registrationCommand.password
        let confirmPassword = registrationCommand.confirmPassword
        let paramDict = ["name":name, "email":email, "dob":dob, "password":password, "confirmPassword":confirmPassword]
        let request = RegistrationRequest(parameter: paramDict as! [String : String])
        request.registerUser(parameter: paramDict as! [String : String], vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(RegistrationModel.self, from: response! as! Data)
                    let userid = objResponse.responseData?.id
                    print("id...\(userid ?? "NA")")
                    completion(objResponse, success, nil)
                    
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
}
