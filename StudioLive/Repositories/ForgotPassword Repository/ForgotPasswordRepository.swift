//
//  ForgotPasswordRepository.swift
//  StudioLive
//
//  Created by Pradipta on 20/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ForgotPasswordRepository: NSObject {
    func forgotPassword(forgotPasswordCommand: ForgotPasswordCommand, vc: UIViewController, completion: @escaping (ForgotPasswordModel, Bool, NSError?) -> Void) {
        let email = forgotPasswordCommand.email
        let paramDict = ["email":email]
        let request = ForgotPasswordRequest(parameter: paramDict as! [String : String])
        request.forgotPassword(parameter: paramDict as! [String : String], vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(ForgotPasswordModel.self, from: response! as! Data)
                    let otp = objResponse.responseData?.forgotPassOtp
                    print("otp...\(otp ?? "NA")")
                    completion(objResponse, success, nil)
                    
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
}
