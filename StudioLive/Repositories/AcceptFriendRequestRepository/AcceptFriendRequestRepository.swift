//
//  AcceptFriendRequestRepository.swift
//  StudioLive
//
//  Created by BrainiumSSD on 05/11/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class AcceptFriendRequestRepository: NSObject {
    
    func acceptFriendRequest(vc: UIViewController,parameter: [String:String] ,completion: @escaping (SendFriendRequestModel, Bool, NSError?) -> Void) {
        let request = AcceptFriendRequest()
        
        request.acceptFriendRequest(vc: vc, parameter: parameter,hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(SendFriendRequestModel.self, from: response! as! Data)
                    let message = objResponse.message
                    print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
                print("objResponse\(response)")
            }
        }
    }
}
