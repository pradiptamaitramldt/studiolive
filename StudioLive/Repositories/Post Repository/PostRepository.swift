//
//  PostRepository.swift
//  StudioLive
//
//  Created by Pradipta Maitra on 26/09/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class PostRepository: NSObject {
    var utility = Utility()
    
    func uploadAudio(myAudio:Data, postCommand: PostCommand, vc: UIViewController, completion: @escaping (NSDictionary, Bool, NSError?) -> Void) {
        let userID = self.utility.Retrive(Constants.Strings.UserID)
        let paramDict = ["customerId":userID, "title": postCommand.title ?? "", "categoryId": postCommand.categoryID ?? "", "studioPrivacy": postCommand.privacyType ?? "", "location": postCommand.location ?? "", "fileType": postCommand.fileType ?? ""] as [String : Any]
        
        
        let request = PostRequest()
        request.audioUploadWithMultipart(parameter: paramDict as NSDictionary, coverdata: myAudio, imageKey: "file", mimetypeExtention: ".m4a", vc: vc, hud: true) { (response, success) in
            if success{
                completion(response, true, nil)
            }
        }
    }
    
    func fetchAudioCategory(vc: UIViewController, completion: @escaping (AudioCategoryResponseModel, Bool, NSError?) -> Void) {
        let userID = self.utility.Retrive(Constants.Strings.UserID)
        let paramDict = ["customerId":userID]
        
        let request = PostRequest()
        request.fetchAudioCategory(parameter: paramDict as! [String : String], vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success {
                guard let successDashboardResponse = response, let data = successDashboardResponse.responseData else {
                    return
                }
                                
                completion(response!, success, nil)
                
                Utility.log("Finally it's done")
            } else {
                print("JSON Parse Error:")
                completion(response!, false, nil)
            }
        }
    }
}
