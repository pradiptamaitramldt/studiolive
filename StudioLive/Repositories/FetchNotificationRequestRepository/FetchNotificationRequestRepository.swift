//
//  FetchNotificationRequestRepository.swift
//  StudioLive
//
//  Created by BrainiumSSD on 06/11/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class FetchNotificationRequestRepository: NSObject {
    func fetchNotificationRequest(vc: UIViewController, completion: @escaping (FetchNotificationRequestModel, Bool, NSError?) -> Void) {
        let request = FetchNotificationRequest()
        request.fetchNotificationRequest(vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(FetchNotificationRequestModel.self, from: response! as! Data)
                    let message = objResponse.message
                    print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
                print("objResponse\(response)")
            }
        }
    }
   
}
