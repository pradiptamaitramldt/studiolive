//
//  ChangePasswordRepository.swift
//  StudioLive
//
//  Created by Pradipta on 16/03/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ChangePasswordRepository: NSObject {
    var utility = Utility()
    
    func changePassword(changePasswordCommand: ChangePasswordCommand, vc: UIViewController, completion: @escaping (EditProfile, Bool, NSError?) -> Void) {
        let userID = self.utility.Retrive(Constants.Strings.UserID)
        let paramDict = ["customerId":userID, "oldPassword": changePasswordCommand.oldPassword!, "newPassword": changePasswordCommand.newPassword!, "confirmPassword": changePasswordCommand.confirmPassword!] as [String : Any]
        let request = ChangePasswordRequest()
        request.changePassword(parameter: paramDict as! [String : String], vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(EditProfile.self, from: response! as! Data)
                    let message = objResponse.message
                    print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
}
