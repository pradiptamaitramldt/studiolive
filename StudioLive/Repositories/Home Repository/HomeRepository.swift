//
//  HomeRepository.swift
//  Nexopt
//
//  Created by Pradipta on 26/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class HomeRepository: NSObject {
    var utility = Utility()
    
    func fetchMusicVideo(vc: UIViewController, completion: @escaping (HomeResponseModel, Bool, NSError?) -> Void) {
        let userID = self.utility.Retrive(Constants.Strings.UserID)
        let paramDict = ["customerId":userID]
        
        let request = HomeRequest()
        request.fetchMusicVideo(parameter: paramDict as! [String : String], vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success {
                guard let successDashboardResponse = response, let data = successDashboardResponse.responseData else {
                    return
                }
                //  print("HomeResponseModelResponse",response?.responseData as Any)
                completion(response!, success, nil)
                
                Utility.log("Finally it's done")
            } else {
                print("JSON Parse Error:")
                completion(response!, false, nil)
            }
        }
    }
    
    func deleteAudio(musicID: String, vc: UIViewController, completion: @escaping (LikeResponseModel, Bool, NSError?) -> Void) {
        let userID = self.utility.Retrive(Constants.Strings.UserID)
        let paramDict = ["customerId":userID, "musicId": musicID] as [String : Any]
        
        let request = HomeRequest()
        request.deleteAudio(parameter: paramDict as! [String : String], vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success {
                guard let successDashboardResponse = response, let data = successDashboardResponse.responseData else {
                    return
                }
                //  print("HomeResponseModelResponse",response?.responseData as Any)
                completion(response!, success, nil)
                
                Utility.log("Finally it's done")
            } else {
                print("JSON Parse Error:")
                completion(response!, false, nil)
            }
        }
    }
    
    func fetchHomeData(vc: UIViewController, completion: @escaping (HomeResponseModel, Bool, NSError?) -> Void) {
        let userID = self.utility.Retrive(Constants.Strings.UserID)
        let paramDict = ["customerId":userID]
        
        let request = HomeRequest()
        request.fetchHomeData(parameter: paramDict as! [String : String], vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success {
                guard let successDashboardResponse = response, let data = successDashboardResponse.responseData else {
                    return
                }
                //  print("HomeResponseModelResponse",response?.responseData as Any)
                completion(response!, success, nil)
                
                Utility.log("Finally it's done")
            } else {
                print("JSON Parse Error:")
                completion(response!, false, nil)
            }
        }
    }
    
    func likeUnlikePost(musicID: String, likeCustomerID: String, isLike: String, vc: UIViewController, completion: @escaping (LikeResponseModel, Bool, NSError?) -> Void) {
        let userID = self.utility.Retrive(Constants.Strings.UserID)
        let paramDict = ["customerId": userID, "musicId": musicID, "likeCustomerId": likeCustomerID, "like": isLike] as [String : Any]
        
        let request = HomeRequest()
        request.likeUnlikePost(parameter: paramDict as! [String : String], vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success {
                guard let successDashboardResponse = response, let data = successDashboardResponse.responseData else {
                    return
                }
                                
                completion(response!, success, nil)
                
                Utility.log("Finally it's done")
            } else {
                print("JSON Parse Error:")
                completion(response!, false, nil)
            }
        }
    }
    
    func favUnfavPost(musicID: String, favCustomerID: String, isFav: String, vc: UIViewController, completion: @escaping (LikeResponseModel, Bool, NSError?) -> Void) {
        let userID = self.utility.Retrive(Constants.Strings.UserID)
        let paramDict = ["customerId": userID, "musicId": musicID, "favouriteCustomerId": favCustomerID, "favourite": isFav] as [String : Any]
        
        let request = HomeRequest()
        request.favUnfavPost(parameter: paramDict as! [String : String], vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success {
                guard let successDashboardResponse = response, let data = successDashboardResponse.responseData else {
                    return
                }
                                
                completion(response!, success, nil)
                
                Utility.log("Finally it's done")
            } else {
                print("JSON Parse Error:")
                completion(response!, false, nil)
            }
        }
    }
}
