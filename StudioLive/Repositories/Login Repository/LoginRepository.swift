//
//  LoginRepository.swift
//  Nexopt
//
//  Created by Pradipta on 25/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class LoginRepository: NSObject {
    func loginUser(loginCommand: LoginCommand, vc: UIViewController, completion: @escaping (LoginModel, Bool, NSError?) -> Void) {
        let email = loginCommand.email
        let password = loginCommand.password
        let paramDict = ["email":email,
                         "password":password]
        let request = LoginRequest(parameter: paramDict as! [String : String])
        request.loginUser(parameter: paramDict as! [String : String], vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(LoginModel.self, from: response! as! Data)
                    let userid = objResponse.responseData?.userDetails?.id
                    print("id...\(userid ?? "NA")")
                    completion(objResponse, success, nil)
                    
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
}
