//
//  AccountVerifyRepository.swift
//  StudioLive
//
//  Created by Pradipta on 20/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class AccountVerifyRepository: NSObject {
    var utility = Utility()
    func verifyAccount(accountVerifyCommand: AccountVerifyCommand, vc: UIViewController, completion: @escaping (AccountVerificationModel, Bool, NSError?) -> Void) {
        let email = utility.Retrive(Constants.Strings.UserEmail) as! String
        let otp = accountVerifyCommand.otp
        let paramDict = ["email":email,
                         "otp":otp]
        let request = AccountVerifyRequest(parameter: paramDict as! [String : String])
        request.verifyUserAccount(parameter: paramDict as! [String : String], vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(AccountVerificationModel.self, from: response! as! Data)
                    let userid = objResponse.responseData?.userDetails?.id
                    print("id...\(userid ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
    func resendOTP(vc: UIViewController, completion: @escaping (ResendOTPModel, Bool, NSError?) -> Void) {
        let email = utility.Retrive(Constants.Strings.UserEmail) as! String
        let paramDict = ["email":email]
        let request = AccountVerifyRequest(parameter: paramDict)
        request.resendOTP(parameter: paramDict, vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(ResendOTPModel.self, from: response! as! Data)
                    let email = objResponse.responseData?.email
                    print("email...\(email ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
}
