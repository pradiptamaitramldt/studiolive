//
//  FetchAllUserRepository.swift
//  StudioLive
//
//  Created by BrainiumSSD on 05/11/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class FetchAllUserRepository: NSObject {
    func fetchAllUser(vc: UIViewController, completion: @escaping (FetchAllUserModel, Bool, NSError?) -> Void) {
        let request = FetchAllUserRequest()
        request.fetchAllUser(vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(FetchAllUserModel.self, from: response! as! Data)
                    let message = objResponse.message
                    print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
                print("objResponse\(response)")
            }
        }
    }
    func fetchAllConnectedUser(vc: UIViewController, completion: @escaping (FetchAllUserModel, Bool, NSError?) -> Void) {
        let request = FetchAllUserRequest()
        request.fetchAllConectedUser(vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(FetchAllUserModel.self, from: response! as! Data)
                    let message = objResponse.message
                    print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
                print("objResponse\(response)")
            }
        }
    }
}
