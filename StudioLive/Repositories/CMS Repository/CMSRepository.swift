//
//  CMSRepository.swift
//  StudioLive
//
//  Created by Pradipta on 27/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class CMSRepository: NSObject {
    func fetchCMSData(slug: String, vc: UIViewController, completion: @escaping (CMSPagesModel, Bool, NSError?) -> Void) {
        let paramDict = ["slug":slug]
        let request = CMSRequest(parameter: paramDict)
        request.fetchCMSData(parameter: paramDict, vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(CMSPagesModel.self, from: response! as! Data)
                    let message = objResponse.responseMessage
                    print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
}
