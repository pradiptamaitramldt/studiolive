//
//  ProfileImageUploadRepository.swift
//  StudioLive
//
//  Created by Pradipta on 16/03/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ProfileImageUploadRepository: NSObject {
    var utility = Utility()
    
    func uploadProfileImage(profileImage:UIImage,vc: UIViewController, completion: @escaping (NSDictionary, Bool, NSError?) -> Void) {
        let userID = self.utility.Retrive(Constants.Strings.UserID)
        let paramDict = ["customerId":userID] as [String : Any]
        let request = ProfileImageUploadRequest()
        request.imageUploadWithMultipart(parameter: paramDict as NSDictionary, coverdata: profileImage, imageKey: "image", mimetypeExtention: "png", vc: vc, hud: true) { (response, success) in
            if success{
                completion(response, true, nil)
            }
        }
        
        
//        request.imageUploadWithMultipart(parameter: paramDict as! [String : String], coverdata:profileImage, imageKey:"image", mimetypeExtention:"png", vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
//            if success{
//                do {
//                    let objResponse = try JSONDecoder().decode(EditProfile.self, from: response! as! Data)
//                    let message = objResponse.message
//                    print("message...\(message ?? "NA")")
//                    completion(objResponse, success, nil)
//                } catch let error {
//                    print("JSON Parse Error: \(error.localizedDescription)")
//                }
//            }
//        }
    }
}
