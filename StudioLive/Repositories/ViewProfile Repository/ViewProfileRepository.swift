//
//  ViewProfileRepository.swift
//  StudioLive
//
//  Created by Pradipta on 13/03/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ViewProfileRepository: NSObject {
    func viewProfile(vc: UIViewController, completion: @escaping (ViewProfile, Bool, NSError?) -> Void) {
        let request = ViewProfileRequest()
        request.viewProfile(vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(ViewProfile.self, from: response! as! Data)
                    let message = objResponse.message
                    print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
}
