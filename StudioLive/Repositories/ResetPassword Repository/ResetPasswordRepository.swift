//
//  ResetPasswordRepository.swift
//  StudioLive
//
//  Created by Pradipta on 21/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit

class ResetPasswordRepository: NSObject {
    var utility = Utility()
    func resetPassword(createPasswordCommand: CreatePasswordCommand, vc: UIViewController, completion: @escaping (ResetPasswordModel, Bool, NSError?) -> Void) {
        let email = utility.Retrive(Constants.Strings.UserEmail) as! String
        let password = createPasswordCommand.password
        let confirmPassword = createPasswordCommand.confirmPassword
        let paramDict = ["email":email,
                         "password":password,
                         "confirmPassword":confirmPassword]
        let request = ResetPasswordRequest(parameter: paramDict as! [String : String])
        request.resetPassword(parameter: paramDict as! [String : String], vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(ResetPasswordModel.self, from: response! as! Data)
                    let message = objResponse.responseMessage
                    print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
}
