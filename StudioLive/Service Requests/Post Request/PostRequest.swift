//
//  PostRequest.swift
//  StudioLive
//
//  Created by Pradipta Maitra on 26/09/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit
import Alamofire

class PostRequest: NSObject {
    let load = Loader()
    var utility = Utility()
    
    private func printJSON(apiName: String, data: Data) {
        print("API name: ", apiName)
        print(String(data: data, encoding: String.Encoding.utf8) ?? "")
    }
    func presentAlertWithTitle(title: String, message : String, vc: UIViewController)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in print("Youve pressed OK Button")
        }
        alertController.addAction(OKAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: 6. Audio Upload Webserice Method Brainium
    
   
    func audioUploadWithMultipart(parameter: NSDictionary, coverdata: Data,imageKey : String, mimetypeExtention :String , vc: UIViewController, hud: Bool, completion: @escaping (_ responce : NSDictionary, _ status : Bool) -> ()) {
        if Utility.isNetworkReachable() {
            if hud{
                load.show(views: vc.view)
            }
            let editProfileUrl = Constants.APIPath.postAudio
            let token = self.utility.Retrive(Constants.Strings.Token) as! String
            let jsonData  = try! JSONSerialization.data(withJSONObject: parameter, options: .prettyPrinted)
            
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print("JSON Params->\(jsonString)")
            
            let headers: HTTPHeaders = [
                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "Bearer \(token)"
            ]
            
            let url = try! URLRequest(url: URL(string:editProfileUrl)!, method: .post, headers: headers)
            
//            let coverimageData = coverdata.jpegData(compressionQuality: 0.8)
           
//            Alamofire.upload(
//                multipartFormData: { multipartFormData in
//                        for (key, value) in parameter {
//                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as! String)
//                        }
//
//                        multipartFormData.append(coverdata, withName: imageKey, fileName: /*self.coverImage(ext: mimetypeExtention)*/"myaudio.m4a", mimeType: self.mimeTypeofAudio(ext: mimetypeExtention))
//                },
//                to: url as! URLConvertible,
//                encodingCompletion: { encodingResult in
//                    switch encodingResult {
//                    case .success(let upload, _, _):
//                        upload.responseJSON { response in
//                            debugPrint(response)
//                        }
//                        upload.uploadProgress { progress in
//
//                            print("procress",progress.fractionCompleted)
//
//                        }
//                        upload.responseJSON
//                          { response in
//
//                            if hud{
//                                self.load.hide(delegate: vc)
//                            }
//                            if response.result.value != nil
//                            {
//                                let responce = response.result.value! as AnyObject
//
//                                let sucess = responce.value(forKey: "STATUSCODE") as! Int
//                                let success_message = responce.value(forKey: "message") as! String
//
//                                print("JSON Responce->\(responce)")
//
//                                if sucess == 200
//                                {
//                                    // let data = responce.value(forKey: "data") as! NSDictionary
//                                    //completion(responce.replacingNullsWithBlanks() as NSDictionary, true)
//                                    self.presentAlertWithTitle(title: Constants.Strings.AppName, message: success_message, vc: vc)
//
//                                }else
//                                {
//                                    print("Fails")
//                                    completion([:], false)
//                                    self.presentAlertWithTitle(title: "Error!", message: (response.result.error.debugDescription), vc: vc)
//                                }
//                        }
//                    }
//
//                    case .failure(let encodingError):
//                        print(encodingError)
//                    }
//                }
//            )
            
            
            
            
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    for (key, value) in parameter {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as! String)
                    }
                    
                    multipartFormData.append(coverdata, withName: imageKey, fileName: /*self.coverImage(ext: mimetypeExtention)*/"myaudio.m4a", mimeType: self.mimeTypeofAudio(ext: mimetypeExtention))
            },
                with: url,
                encodingCompletion: { encodingResult in
                    print(encodingResult)
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.uploadProgress { progress in
                                                      //   print("Float(progress.fractionCompleted)",Float(progress.fractionCompleted))
                            NotificationCenter.default.post(name: Notification.Name("ReadyToPlayAudio2"), object: Float((progress.fractionCompleted)))
                                                     }
                        upload.responseJSON
                            { response in
                                
                                print("response",response)
                                
                             
                                if hud{
                                    self.load.hide(delegate: vc)
                                }
                                if response.result.value != nil
                                {
                                    let responce = response.result.value! as AnyObject
                                    
                                    let sucess = responce.value(forKey: "STATUSCODE") as! Int
                                    let success_message = responce.value(forKey: "message") as! String
                                    
                                    print("JSON Responce->\(responce)")
                                    
                                    if sucess == 200
                                    {
                                        // let data = responce.value(forKey: "data") as! NSDictionary
                                        //completion(responce.replacingNullsWithBlanks() as NSDictionary, true)
                                        self.presentAlertWithTitle(title: Constants.Strings.AppName, message: success_message, vc: vc)
                                        
                                    }else
                                    {
                                        print("Fails")
                                        completion([:], false)
                                        self.presentAlertWithTitle(title: "Error!", message: (response.result.error.debugDescription), vc: vc)
                                    }
                                }
                        }
                        break
                    case .failure( _):
                        self.presentAlertWithTitle(title: "Error!", message: "", vc: vc)
                        break
                    }
                })
        }else {
            self.presentAlertWithTitle(title: "Error!", message: "Please check network connectivity.", vc: vc)
        }
    }
 
    
    func mimeTypeofAudio(ext: String) -> String {
        
        if ext == "m4a"{
            return "audio.m4a"
        }else
        {
            return "audio.mp3"
        }
        
    }
    
//    func fetchAudioCategory<T : APIResponseParentModel>(parameter: [String:String], vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?,_ message : NSString, _ status : Bool) -> ()) {
//
//        if Utility.isNetworkReachable() {
//            if hud{
//                load.show(views: vc.view)
//            }
//            let forgotPasswordUrl = Constants.APIPath.audioCategory
//            let token = self.utility.Retrive(Constants.Strings.Token) as! String
//
//            let headers: HTTPHeaders = [
//                "Accept": "application/json",
//                "Content-Type": "application/x-www-form-urlencoded",
//                "Authorization": "Bearer \(token)"
//            ]
//
//            Alamofire.request(forgotPasswordUrl, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: headers).responseData { response in
//                if hud{
//                    self.load.hide(delegate: vc)
//                }
//                switch(response.result){
//                case .success(_):
//                    self.printJSON(apiName: forgotPasswordUrl, data: response.result.value!)
//                    print(response.result.description)
//                    if response.result.value != nil {
//                        completion(response.result.value, "", true)
//                    }
//                    break
//                case .failure(_):
//                    print(response.result.error ?? "Fail")
//                    self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
//                    break
//                }
//            }
//        }
//    }
    
    func fetchAudioCategory<T : APIResponseParentModel>(parameter: [String:String], vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : AudioCategoryResponseModel?,_ message : String, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            
            let token = self.utility.Retrive(Constants.Strings.Token) as! String
            let headers: HTTPHeaders = [
                            "Accept": "application/json",
                            "Content-Type": "application/x-www-form-urlencoded",
                            "Authorization": "Bearer \(token)"
                        ]
            
            self.callPost(url: URL(string: Constants.APIPath.audioCategory)!, params: parameter, httpHeader: headers) { (message, data) in
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: AudioCategoryResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(successResponse, successMessage, true)
                    } else {
                        completion(nil, parsingError?.localizedDescription ?? "", false)
                    }
                })
            }
        } else {
            completion(nil, "", false)
        }
    }
    
    func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }

    func callPost(url:URL, params:[String:Any], httpHeader: [String: String], finish: @escaping ((message:String, data:Data?)) -> Void)
    {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = httpHeader
        
        Utility.log("URL: \(url)")
        Utility.log("Header: \(httpHeader)")
        Utility.log("Parameter: ")
        Utility.log(params)
        
        let postString = self.getPostString(params: params)
        request.httpBody = postString.data(using: .utf8)

        var result:(message:String, data:Data?) = (message: "Fail", data: nil)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in

            if(error != nil)
            {
                result.message = "Fail Error not null : \(error.debugDescription)"
            }
            else
            {
                result.message = "Success"
                result.data = data
            }

            finish(result)
        }
        task.resume()
    }
}
