import UIKit
import MobileCoreServices


class UploadFileRequest: HTTPRequest {
    
    var notificationKey : String? {
        get{
            return nil
        }
    }
    
    var localPath : URL? {
        get{
            return nil
        }
    }
    
    override var url : URL? {
        get{
            return nil
        }
    }
    
    override var httpMethod : String? {
        get{
            return "POST"
        }
    }
    
    override var headers : NSDictionary? {
        get {
            let headers : [String : String] = [
                "cookies"       : ETWSessionRepository.shared.token ?? "",
                "Content-Type" : "multipart/form-data; boundary=\(self.generateBoundaryString())"
            ]
            return headers as NSDictionary
        }
    }
    
    
    func createBody(with parameters: [String: String]?, filePathKey: String, fileName: String, fileData: Data)  -> Data {
        let boundary = self.generateBoundaryString()
        var body = Data()
        let linebreak = "\r\n"
        if parameters != nil {
            for (key, value) in parameters! {
                body.append("--\(boundary + linebreak)")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\(linebreak + linebreak)")
                body.append("\(value + linebreak)")
            }
        }
        
        let data = fileData
        if let path = self.localPath?.absoluteString {
            let mimetype = mimeType(for: path)
            
            body.append("--\(boundary + linebreak)")
            body.append("Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(fileName)\"\(linebreak)")
            body.append("Content-Type: \(mimetype + linebreak + linebreak)")
            body.append(data)
            body.append("\(linebreak)")
            
            body.append("--\(boundary + linebreak)")
        }
        
        return body
    }
    
    private func generateBoundaryString() -> String {
        return "----WebKitFormBoundaryT1BYAZIit8D93BT0"
    }
    
    private func mimeType(for path: String) -> String {
        let url = URL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
}

extension Data {
    /// Append string to Data
    ///
    /// Rather than littering my code with calls to `data(using: .utf8)` to convert `String` values to `Data`, this wraps it in a nice convenient little extension to Data. This defaults to converting using UTF-8.
    ///
    /// - parameter string:       The string to be added to the `Data`.
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}
