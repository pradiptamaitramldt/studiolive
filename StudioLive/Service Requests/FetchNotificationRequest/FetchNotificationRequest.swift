//
//  FetchNotificationRequest.swift
//  StudioLive
//
//  Created by BrainiumSSD on 06/11/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit
import Alamofire

class FetchNotificationRequest: NSObject {

    let load = Loader()
    var utility = Utility()
    private func printJSON(apiName: String, data: Data) {
        print("API name: ", apiName)
        print(String(data: data, encoding: String.Encoding.utf8) ?? "")
    }
    func presentAlertWithTitle(title: String, message : String, vc: UIViewController)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in print("Youve pressed OK Button")
        }
        alertController.addAction(OKAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    func fetchNotificationRequest<T : APIResponseParentModel>(vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?,_ message : NSString, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            if hud{
                load.show(views: vc.view)
            }
            let fetchAllUserUrl = Constants.APIPath.fetchNotification
            let token = self.utility.Retrive(Constants.Strings.Token) as! String
            let headers: HTTPHeaders = [
                            "Content-Type": "application/x-www-form-urlencoded",
                            "Authorization": "Bearer \(token)"
                        ]
            let userID = self.utility.Retrive(Constants.Strings.UserID)
            let paramDict = ["customerId":userID]
            
            Alamofire.request(fetchAllUserUrl, method: .post, parameters: paramDict, encoding: URLEncoding.default, headers: headers).responseData { response in
                if hud{
                    self.load.hide(delegate: vc)
                }
                switch(response.result){
                case .success(_):
                    self.printJSON(apiName: fetchAllUserUrl, data: response.result.value!)
                    print(response.result.description)
                    if response.result.value != nil {
                        completion(response.result.value, "", true)
                    }
                    //self.presentAlertWithTitle(title: "Alert", message: (response.result.message)!, vc: vc)
                    break
                case .failure(_):
                    print(response.result.error ?? "Fail")
                    self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                    break
                }
            }
        }
    }
    func fetchAllConectedUser<T : APIResponseParentModel>(vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?,_ message : NSString, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            if hud{
                load.show(views: vc.view)
            }
            let fetchAllUserUrl = Constants.APIPath.fetchAllConnectedUser
            let token = self.utility.Retrive(Constants.Strings.Token) as! String
            let headers: HTTPHeaders = [
                            "Content-Type": "application/x-www-form-urlencoded",
                            "Authorization": "Bearer \(token)"
                        ]
            let userID = self.utility.Retrive(Constants.Strings.UserID)
            let paramDict = ["customerId":userID]
            
            Alamofire.request(fetchAllUserUrl, method: .post, parameters: paramDict, encoding: URLEncoding.default, headers: headers).responseData { response in
                if hud{
                    self.load.hide(delegate: vc)
                }
                switch(response.result){
                case .success(_):
                    self.printJSON(apiName: fetchAllUserUrl, data: response.result.value!)
                    print(response.result.description)
                    if response.result.value != nil {
                        completion(response.result.value, "", true)
                    }
                    break
                case .failure(_):
                    print(response.result.error ?? "Fail")
                    self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                    break
                }
            }
        }
    }
}
