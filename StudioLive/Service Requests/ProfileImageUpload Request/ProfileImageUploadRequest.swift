//
//  ProfileImageUploadRequest.swift
//  StudioLive
//
//  Created by Pradipta on 16/03/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit
import Alamofire

class ProfileImageUploadRequest: NSObject {
    let load = Loader()
    var utility = Utility()
    
    private func printJSON(apiName: String, data: Data) {
        print("API name: ", apiName)
        print(String(data: data, encoding: String.Encoding.utf8) ?? "")
    }
    func presentAlertWithTitle(title: String, message : String, vc: UIViewController)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in print("Youve pressed OK Button")
        }
        alertController.addAction(OKAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: 6. Image Upload Webserice Method Brainium
    func imageUploadWithMultipart(parameter: NSDictionary,coverdata: UIImage,imageKey : String, mimetypeExtention :String , vc: UIViewController, hud: Bool, completion: @escaping (_ responce : NSDictionary, _ status : Bool) -> ()) {
        if Utility.isNetworkReachable() {
            if hud{
                load.show(views: vc.view)
            }
            let editProfileUrl = Constants.APIPath.profileImageUpload
            let token = self.utility.Retrive(Constants.Strings.Token) as! String
            let jsonData  = try! JSONSerialization.data(withJSONObject: parameter, options: .prettyPrinted)
            
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print("JSON Params->\(jsonString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "Bearer \(token)"
            ]
            
            let url = try! URLRequest(url: URL(string:editProfileUrl)!, method: .post, headers: headers)
            
            let coverimageData = coverdata.jpegData(compressionQuality: 0.8)
            
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    for (key, value) in parameter {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as! String)
                    }
                    
                    multipartFormData.append(coverimageData!, withName: imageKey, fileName: /*self.coverImage(ext: mimetypeExtention)*/"image.png", mimeType: self.mimeTypeofImage(ext: mimetypeExtention))
            },
                with: url,
                encodingCompletion: { encodingResult in
                    print(encodingResult)
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON
                            { response in
                                
                                print("response",response)
                                
                                if hud{
                                    self.load.hide(delegate: vc)
                                }
                                if response.result.value != nil
                                {
                                    let responce = response.result.value! as AnyObject
                                    
                                    let sucess = responce.value(forKey: "STATUSCODE") as! Int
                                    let success_message = responce.value(forKey: "message") as! String
                                    
                                    print("JSON Responce->\(responce)")
                                    
                                    if sucess == 200
                                    {
                                        // let data = responce.value(forKey: "data") as! NSDictionary
                                        //completion(responce.replacingNullsWithBlanks() as NSDictionary, true)
                                        let profileImage  =    ( (responce.value(forKey: "response_data") as! NSDictionary).object(forKey: "profileImage") as! String)
                                      //  print("profileImage : \(profileImage)")
                                        self.utility.Save(profileImage, keyname: Constants.Strings.profileImage)
                                        NotificationCenter.default.post(name: Notification.Name("UpdateSliderProfileValue"), object: nil)
                                        self.presentAlertWithTitle(title: Constants.Strings.AppName, message: success_message, vc: vc)
                                        
                                    }else
                                    {
                                        print("Fails")
                                        completion([:], false)
                                        self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                                    }
                                }
                        }
                        break
                    case .failure( _):
                        self.presentAlertWithTitle(title: "Error!", message: "", vc: vc)
                        break
                    }
            })
        }else {
            self.presentAlertWithTitle(title: "Error!", message: "Please check network connectivity.", vc: vc)
        }
    }
    func mimeTypeofImage(ext: String) -> String {
        
        if ext == "jpg"{
            return "image/jpeg"
        }else
        {
            return "image/png"
        }
        
    }
}
