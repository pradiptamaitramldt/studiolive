//
//  AccountVerifyRequest.swift
//  StudioLive
//
//  Created by Pradipta on 20/01/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit
import Alamofire

class AccountVerifyRequest: NSObject {
    let load = Loader()
    init(parameter: [String:String]) {
        
    }
    private func printJSON(apiName: String, data: Data) {
        print("API name: ", apiName)
        print(String(data: data, encoding: String.Encoding.utf8) ?? "")
    }
    func presentAlertWithTitle(title: String, message : String, vc: UIViewController)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in print("Youve pressed OK Button")
        }
        alertController.addAction(OKAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    func verifyUserAccount<T : APIResponseParentModel>(parameter: [String:String], vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?,_ message : NSString, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            if hud{
                load.show(views: vc.view)
            }
            let accountVerifyUrl = Constants.APIPath.accountVerification
            
            Alamofire.request(accountVerifyUrl, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: nil).responseData { response in
                if hud{
                    self.load.hide(delegate: vc)
                }
                switch(response.result){
                case .success(_):
                    self.printJSON(apiName: accountVerifyUrl, data: response.result.value!)
                    print(response.result.description)
                    if response.result.value != nil {
                        completion(response.result.value, "", true)
                    }
                    break
                case .failure(_):
                    print(response.result.error ?? "Fail")
                    self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                    break
                }
            }
        }
    }
    
    func resendOTP<T : APIResponseParentModel>(parameter: [String:String], vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?,_ message : NSString, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            if hud{
                load.show(views: vc.view)
            }
            let accountVerifyUrl = Constants.APIPath.resendOTP
            
            Alamofire.request(accountVerifyUrl, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: nil).responseData { response in
                if hud{
                    self.load.hide(delegate: vc)
                }
                switch(response.result){
                case .success(_):
                    self.printJSON(apiName: accountVerifyUrl, data: response.result.value!)
                    print(response.result.description)
                    if response.result.value != nil {
                        completion(response.result.value, "", true)
                    }
                    break
                case .failure(_):
                    print(response.result.error ?? "Fail")
                    self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                    break
                }
            }
        }
    }
}
