//
//  HomeRequest.swift
//  StudioLive
//
//  Created by Pradipta Maitra on 29/09/20.
//  Copyright © 2020 Brainium. All rights reserved.
//

import UIKit
import Alamofire

class HomeRequest: NSObject {

    let load = Loader()
    var utility = Utility()
    
    private func printJSON(apiName: String, data: Data) {
        print("API name: ", apiName)
        print(String(data: data, encoding: String.Encoding.utf8) ?? "")
    }
    func presentAlertWithTitle(title: String, message : String, vc: UIViewController)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in print("Youve pressed OK Button")
        }
        alertController.addAction(OKAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    func fetchMusicVideo<T : APIResponseParentModel>(parameter: [String:String], vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : HomeResponseModel?,_ message : String, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            if hud{
                DispatchQueue.main.async {
                    self.load.show(views: vc.view)
                }
                
            }
            let token = self.utility.Retrive(Constants.Strings.Token) as! String
            let headers: HTTPHeaders = [
                            "Accept": "application/json",
                            "Content-Type": "application/x-www-form-urlencoded",
                            "Authorization": "Bearer \(token)"
                        ]
            
            self.callPost(url: URL(string: Constants.APIPath.fetchDashboard)!, params: parameter, httpHeader: headers) { (message, data) in
                if hud{
                    self.load.hide(delegate: vc)
                }
              //  let str = String(decoding: data!, as: UTF8.self)
             //   print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: HomeResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(successResponse, successMessage, true)
                    } else {
                        completion(nil, parsingError?.localizedDescription ?? "", false)
                    }
                })
            }
        } else {
            completion(nil, "", false)
        }
    }
    
    func deleteAudio<T : APIResponseParentModel>(parameter: [String:String], vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : LikeResponseModel?,_ message : String, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            if hud{
                DispatchQueue.main.async {
                    self.load.show(views: vc.view)
                }
                
            }
            let token = self.utility.Retrive(Constants.Strings.Token) as! String
            let headers: HTTPHeaders = [
                            "Accept": "application/json",
                            "Content-Type": "application/x-www-form-urlencoded",
                            "Authorization": "Bearer \(token)"
                        ]
            
            self.callPost(url: URL(string: Constants.APIPath.deletePost)!, params: parameter, httpHeader: headers) { (message, data) in
                if hud{
                    self.load.hide(delegate: vc)
                }
              //  let str = String(decoding: data!, as: UTF8.self)
             //   print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: LikeResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(successResponse, successMessage, true)
                    } else {
                        completion(nil, parsingError?.localizedDescription ?? "", false)
                    }
                })
            }
        } else {
            completion(nil, "", false)
        }
    }
    
    func fetchHomeData<T : APIResponseParentModel>(parameter: [String:String], vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : HomeResponseModel?,_ message : String, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            if hud{
                DispatchQueue.main.async {
                    self.load.show(views: vc.view)
                }
                
            }
            let token = self.utility.Retrive(Constants.Strings.Token) as! String
            let headers: HTTPHeaders = [
                            "Accept": "application/json",
                            "Content-Type": "application/x-www-form-urlencoded",
                            "Authorization": "Bearer \(token)"
                        ]
            
            self.callPost(url: URL(string: Constants.APIPath.fetchHome)!, params: parameter, httpHeader: headers) { (message, data) in
                if hud{
                    self.load.hide(delegate: vc)
                }
              //  let str = String(decoding: data!, as: UTF8.self)
             //   print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: HomeResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(successResponse, successMessage, true)
                    } else {
                        completion(nil, parsingError?.localizedDescription ?? "", false)
                    }
                })
            }
        } else {
            completion(nil, "", false)
        }
    }
    
    func likeUnlikePost<T : APIResponseParentModel>(parameter: [String:String], vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : LikeResponseModel?,_ message : String, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            if hud{
                load.show(views: vc.view)
            }
            let token = self.utility.Retrive(Constants.Strings.Token) as! String
            let headers: HTTPHeaders = [
                            "Accept": "application/json",
                            "Content-Type": "application/x-www-form-urlencoded",
                            "Authorization": "Bearer \(token)"
                        ]
            
            self.callPost(url: URL(string: Constants.APIPath.likeUnlikePost)!, params: parameter, httpHeader: headers) { (message, data) in
                if hud{
                    self.load.hide(delegate: vc)
                }
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: LikeResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(successResponse, successMessage, true)
                    } else {
                        completion(nil, parsingError?.localizedDescription ?? "", false)
                    }
                })
            }
        } else {
            completion(nil, "", false)
        }
    }
    
    func favUnfavPost<T : APIResponseParentModel>(parameter: [String:String], vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : LikeResponseModel?,_ message : String, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            if hud{
                load.show(views: vc.view)
            }
            let token = self.utility.Retrive(Constants.Strings.Token) as! String
            let headers: HTTPHeaders = [
                            "Accept": "application/json",
                            "Content-Type": "application/x-www-form-urlencoded",
                            "Authorization": "Bearer \(token)"
                        ]
            
            self.callPost(url: URL(string: Constants.APIPath.favUnFavPost)!, params: parameter, httpHeader: headers) { (message, data) in
                if hud{
                    self.load.hide(delegate: vc)
                }
                let str = String(decoding: data!, as: UTF8.self)
                print("response:\(str)")
                JSONResponseDecoder.decodeFrom(data!, returningModelType: LikeResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                    if parsingError == nil {
                        guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                        completion(successResponse, successMessage, true)
                    } else {
                        completion(nil, parsingError?.localizedDescription ?? "", false)
                    }
                })
            }
        } else {
            completion(nil, "", false)
        }
    }
    
    func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }

    func callPost(url:URL, params:[String:Any], httpHeader: [String: String], finish: @escaping ((message:String, data:Data?)) -> Void)
    {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = httpHeader
        
        Utility.log("URL: \(url)")
        Utility.log("Header: \(httpHeader)")
        Utility.log("Parameter: ")
        Utility.log(params)
        
        let postString = self.getPostString(params: params)
        request.httpBody = postString.data(using: .utf8)

        var result:(message:String, data:Data?) = (message: "Fail", data: nil)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in

            if(error != nil)
            {
                result.message = "Fail Error not null : \(error.debugDescription)"
            }
            else
            {
                print("data ",data)
                result.message = "Success"
                result.data = data
            }

            finish(result)
        }
        task.resume()
    }
}
